:lang: en
:toc: left
:toclevels: 1
:icons: font
:sectanchors:
:sectlinks:
:hardbreaks:
:doctype: book
:backend: html5
:docinfo: shared
:nofooter:
:source-highlighter: prettify

= Music Organizer

== Components

The code basically works like this:

.Build the docker image
[source,bash]
----
include::Dockerfiles/build_scripts.sh[]
----


.Run the shell script which starts the docker image as a container
[source,bash]
----
include::src/shell/database.sh[]
----


.The database script checks what playlists are available, checks if they are on the blacklist or already scraped and spits them out as a json. Afterwards it checks when the last time a playlist has been scraped for info. If it's been X days, it saves the list of track ids to a database. 
[source,python]
----
include::src/python/database.py[tag=main]
----


The data from this database plus other external data sources are then used to generate the comment field I posted in the thread and save it ino the comment field of the mp3.


.The genre script checks the external data in the comment field plus the internal data in the genre field and decides which tags should be added/removed from the genre field.
[source,python]
----
include::src/python/genre.py[tag=powerful_ambient_electronic]
----


The playlist script checks all songs, looks for certain tags and then makes sure that the correct songs are in the correct Spotify playlists