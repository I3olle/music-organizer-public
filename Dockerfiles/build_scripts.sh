#!/usr/bin/env bash

docker build -t music-organizer-genre ./genre
docker build -t music-organizer-local-database ./local-database
