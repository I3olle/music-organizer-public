#!/usr/bin/env bash

dunstify --icon music-download "Updating playlist-database"

notify_of_errors() {
    dunstify --icon music-download -u critical "Updating playlist-database went wrong"
}

trap "notify_of_errors" ERR

SPOTIPY_CLIENT_ID=$(gopass show spotify/client_id |awk '{print $1}')
SPOTIPY_CLIENT_SECRET=$(gopass show spotify/client_secret |awk '{print $1}')
SPOTIPY_CLIENT_USERNAME=$(gopass show spotify/username |awk '{print $1}')
SPOTIPY_CLIENT_CACHE_PATH="$HOME/tmp/.cache-$SPOTIPY_CLIENT_USERNAME"
docker run \
-t \
--name "music-database-updater" \
--rm \
-e SPOTIPY_CLIENT_ID="$SPOTIPY_CLIENT_ID" \
-e SPOTIPY_CLIENT_SECRET="$SPOTIPY_CLIENT_SECRET" \
-e SPOTIPY_CLIENT_USERNAME="$SPOTIPY_CLIENT_USERNAME" \
-e SPOTIPY_REDIRECT_URI="http://localhost:8888/callback" \
-p 8888:8888 \
-v "$SPOTIPY_CLIENT_CACHE_PATH:/app/.cached-spotipy-credentials" \
-v "$HOME/music-organizer/src/python/database.py:/app/database.py" \
-v "$HOME/music-organizer/sourced_playlists:/app/sourced_playlists" \
-v "$HOME/music-organizer/databases:/app/music_db/" \
-v "$MUSIC_PATH:/app/music" \
music-organizer-local-database:latest python "/app/database.py"