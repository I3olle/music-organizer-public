#!/usr/bin/env bash

dunstify --icon music-distribution "Updating song genres"

notify_of_errors() {
    dunstify --icon music-distribution -u critical "Updating song-genres went wrong"
}

trap "notify_of_errors" ERR

docker run --rm \
-t \
-v "$HOME/music-organizer/src/python/genre.py:/app/genre.py" \
-v "$MUSIC_PATH:/app/music" \
music-organizer-genre:latest python "/app/genre.py"
