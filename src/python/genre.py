from __future__ import print_function    # (at top of module)
import json
import datetime
import math
import mutagen
from mutagen.easyid3 import EasyID3
from eyed3 import id3
import eyed3
from pathlib import Path
import os
import collections
from alive_progress import alive_bar
import logging
import sys
from multiprocessing import Pool


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'



tag = id3.Tag()

MUSIC_PATH = "/app/music"

id3.log.setLevel("DEBUG")
eyed3.log.setLevel("DEBUG")


Log_Format = "%(levelname)s - %(message)s"

LOG_LEVEL = logging.INFO

logging.basicConfig(stream = sys.stdout, 
                    format = Log_Format, 
                    level = LOG_LEVEL)

logger = logging.getLogger()



def genre_set(path, genres_local):
    genres_local = list(filter(None, genres_local))
    if len(genres_local) > 1:
        try:
            genres_local.remove('Unbekannt')
        except:
            genres_local = list(filter(None, genres_local))
    joined_genres = ";".join(genres_local)
    meta = EasyID3(path)
    try:
        genre =  meta["Genre"][0]
    except KeyError:
        EasyID3.RegisterTextKey('Genre', 'TCON')
        meta.save(path, v1=2)
        changed = meta
        changed
    meta['Genre'] = joined_genres.strip(';')
    meta.save(path, v1=2)
    changed = meta
    changed

def append_to_genre_list_v2(
        genres_local,
        comment_list,
        genre_to_append,
        artist='',
        mood_list=[],
        instrument_list=[],
        gender_list=[],
        playlists_with_track=[],
        playlists_with_artist=[],

        include_genres=[],
        exclude_genres=[],

        include_genres_A=[],
        include_genres_B=[],
        exclude_genres_A=[],
        exclude_genres_B=[],

        include_playlists_with_track=[],
        exclude_playlists_with_track=[],

        include_playlists_with_track_A=[],
        include_playlists_with_track_B=[],
        exclude_playlists_with_track_A=[],
        exclude_playlists_with_track_B=[],

        include_playlists_with_artist=[],
        exclude_playlists_with_artist=[],

        include_playlists_with_artist_A=[],
        include_playlists_with_artist_B=[],
        exclude_playlists_with_artist_A=[],
        exclude_playlists_with_artist_B=[],

        include_instruments=[],
        exclude_instruments=[],

        include_instruments_A=[],
        include_instruments_B=[],
        exclude_instruments_A=[],
        exclude_instruments_B=[],

        include_genders=[],
        exclude_genders=[],
        
        include_moods=[],
        exclude_moods=[],

        include_moods_A=[],
        include_moods_B=[],
        exclude_moods_A=[],
        exclude_moods_B=[],

        include_artists=[],
        exclude_artists=[],
        
        exclude_only=False,
        artists_only=False,
        playlists_only=False,
        mood_only=False,
        substring=False,

        debug=False
    ) -> list:

    match_artist=False
    match_exclude_artists=False

    match_mood=False
    match_moods_combined=False
    match_exclude_moods_combined=False
    match_exclude_moods=False

    match_genre=False
    match_genre_combined=False
    match_genre_substring=False
    match_exclude_genres=False
    match_exclude_genres_combined=False

    match_playlists_with_track=False
    match_playlist_with_track_substring=False
    match_playlist_with_track_combined=False
    match_exclude_playlists_with_track=False
    match_exclude_playlists_with_track_combined=False

    match_playlists_with_artist=False
    match_playlist_with_artist_substring=False
    match_playlist_with_artist_combined=False
    match_exclude_playlists_with_artist=False
    match_exclude_playlists_with_artist_combined=False

    match_instruments=False
    match_instruments_combined=False
    match_exclude_instruments=False
    match_exclude_instruments_combined=False

    match_genders=False
    match_exclude_genders=False
    
    match_with_playlist=False
    match_with_mood=False
    match_with_genre=False
    match_with_instrument=False
    match_with_artist_only=False
    match_with_playlists_only=False
    match_with_mood_only=False
    match_with_genre_and_artist=False
    match_with_genre_and_artist_only=False
    match_with_everything_available=False

    if (genre_to_append in exclude_genres):
        exclude_genres.remove(genre_to_append)

    try:
        if (
                (not set(include_artists).isdisjoint([artist])) and
                include_artists != [] and
                artist != ''
            ):
            match_artist=True
    except:
        logger.debug(f"match_artist remains false for: {genre_to_append}")


    if (
            (
                (not set(include_moods).isdisjoint(genres_local) and genres_local != []) or
                (not set(include_moods).isdisjoint(mood_list) and mood_list != [])
            )
            and  (include_moods != [])
        ):
        match_mood=True

    if (
            (
                (not set(include_genres).isdisjoint(comment_list) and comment_list != [] and include_genres != [])  or
                (not set(include_genres).isdisjoint(genres_local) and genres_local != [] and include_genres != []) 
            )
            and  (include_genres != [])
        ):
        match_genre=True

    if (
            (
                (
                    (
                        (not set(include_genres_A).isdisjoint(comment_list) and comment_list != [])
                    )  
                    or (
                        (not set(include_genres_A).isdisjoint(genres_local) and genres_local != [])
                    )
                )
                and (
                    (include_genres_A != [])
                )
            )
            and (
                (
                    (
                        (
                            (not set(include_genres_B).isdisjoint(comment_list) and comment_list != [])
                        )
                        or (
                            (not set(include_genres_B).isdisjoint(genres_local) and genres_local != [])
                        )
                    )
                    and (
                        (include_genres_B != [])
                    )
                )
            )
        ):
        match_genre_combined=True

    if (
            (
                ([s for s in comment_list if any(xs in s for xs in include_genres)] and comment_list != [] and include_genres != [])  or
                ([s for s in genres_local if any(xs in s for xs in include_genres)] and genres_local != [] and include_genres != []) 
            )
            and (
                include_genres != [] and
                substring == True
            )
        ):
        match_genre_substring=True

    if (
            (
                not set(include_playlists_with_track).isdisjoint(playlists_with_track) and playlists_with_track != [] and
                include_playlists_with_track != []
            )
        ):
        match_playlists_with_track=True

    if (
            (
                [s for s in playlists_with_track if any(xs in s for xs in include_playlists_with_track)] and
                playlists_with_track != [] and
                include_playlists_with_track != [] and
                substring == True
            )
        ):
        match_playlist_with_track_substring=True

    if (
            (
                not set(include_playlists_with_track_A).isdisjoint(playlists_with_track) and
                playlists_with_track != [] and
                include_playlists_with_track_A != []
            )
            and (
                not set(include_playlists_with_track_B).isdisjoint(playlists_with_track) and
                playlists_with_track != [] and
                (include_playlists_with_track_B != [])
            )
        ):
        match_playlist_with_track_combined=True

    if (
            (
                not set(include_playlists_with_artist).isdisjoint(playlists_with_artist) and playlists_with_artist != [] and
                include_playlists_with_artist != []
            )
        ):
        match_playlists_with_artist=True

    if (
            (
                [s for s in playlists_with_artist if any(xs in s for xs in include_playlists_with_artist)] and
                playlists_with_artist != [] and
                include_playlists_with_artist != [] and
                substring == True
            )
        ):
        match_playlist_with_artist_substring=True

    if (
            (
                not set(include_playlists_with_artist_A).isdisjoint(playlists_with_artist) and
                playlists_with_artist != [] and
                include_playlists_with_artist_A != []
            )
            and (
                not set(include_playlists_with_artist_B).isdisjoint(playlists_with_artist) and
                playlists_with_artist != [] and
                (include_playlists_with_artist_B != [])
            )
        ):
        match_playlist_with_artist_combined=True

    if (
            (
                (not set(include_instruments).isdisjoint(instrument_list) and instrument_list != [] and include_instruments != [])  or
                (not set(include_instruments).isdisjoint(genres_local) and genres_local != [] and include_instruments != []) 
            )
            and (
                include_instruments != []
            )
        ):
        match_instruments=True

    if (
            (
                (
                    (not set(include_instruments_A).isdisjoint(instrument_list) and instrument_list != []) or
                    (not set(include_instruments_A).isdisjoint(genres_local) and genres_local != [])
                )
                and (include_instruments_A != [])
            )
            and (
                (
                    (not set(include_instruments_B).isdisjoint(instrument_list) and instrument_list != []) or
                    (not set(include_instruments_B).isdisjoint(genres_local) and genres_local != [])
                )
                and (include_instruments_B != [])
            )
        ):
        match_instruments_combined=True

    if (
            not set(include_genders).isdisjoint(gender_list) and
            gender_list != [] and
            include_genders != []
        ):
        match_genders=True

    if (
            (
                (
                    (not set(include_moods_A).isdisjoint(mood_list) and mood_list != []) or
                    (not set(include_moods_A).isdisjoint(genres_local) and genres_local != [])
                )
                and (include_moods_A != [])
            )
            and (
                (
                    (not set(include_moods_B).isdisjoint(mood_list) and mood_list != []) or
                    (not set(include_moods_B).isdisjoint(genres_local) and genres_local != [])
                )
                and (include_moods_B != [])
            )
        ):
        match_moods_combined=True

##### EXCLUDES

    if (
            (not set(exclude_genres).isdisjoint(genres_local)) or
            (not set(exclude_genres).isdisjoint(comment_list))
        ) and (
            exclude_genres != []
        ):
        match_exclude_genres=True

    if (
            (
                (not set(exclude_genres_A).isdisjoint(comment_list) and comment_list != []) or 
                (not set(exclude_genres_A).isdisjoint(genres_local) and genres_local != [])
            )
            and (
                (not set(exclude_genres_B).isdisjoint(comment_list) and comment_list != []) or
                (not set(exclude_genres_B).isdisjoint(genres_local) and genres_local != [])
            )
            and (
            exclude_genres_A != [] and
            exclude_genres_B != []
            )
        ):
        match_exclude_genres_combined=True

    if (
            (not set(exclude_playlists_with_track).isdisjoint(playlists_with_track)) and
            exclude_playlists_with_track != []
        ):
        match_exclude_playlists_with_track=True


    if (
            (not set(exclude_playlists_with_track_A).isdisjoint(playlists_with_track) and playlists_with_track != []) and 
            (not set(exclude_playlists_with_track_B).isdisjoint(playlists_with_track) and playlists_with_track != []) and 
            exclude_playlists_with_track_A != [] and
            exclude_playlists_with_track_B != []
        ):
        match_exclude_playlists_with_track_combined=True

    if (
            (not set(exclude_playlists_with_artist).isdisjoint(playlists_with_artist)) and
            exclude_playlists_with_artist != []
        ):
        match_exclude_playlists_with_artist=True


    if (
            (not set(exclude_playlists_with_artist_A).isdisjoint(playlists_with_artist) and playlists_with_artist != []) and 
            (not set(exclude_playlists_with_artist_B).isdisjoint(playlists_with_artist) and playlists_with_artist != []) and 
            exclude_playlists_with_artist_A != [] and
            exclude_playlists_with_artist_B != []
        ):
        match_exclude_playlists_with_artist_combined=True

    if (
            (not set(exclude_genders).isdisjoint(gender_list)) and
            exclude_genders != []
        ):
        match_exclude_genders=True

    if (
            (
                (not set(exclude_instruments).isdisjoint(genres_local)) or
                (not set(exclude_instruments).isdisjoint(instrument_list))
            ) and
            exclude_instruments != []
        ):
        match_exclude_instruments=True

    if (
            (
                (not set(exclude_instruments_A).isdisjoint(instrument_list) and instrument_list != []) or
                (not set(exclude_instruments_A).isdisjoint(genres_local) and genres_local != [])
            )    
             and 
            (
                (not set(exclude_instruments_B).isdisjoint(instrument_list) and instrument_list != []) or
                (not set(exclude_instruments_B).isdisjoint(genres_local) and genres_local != [])
            ) and 
            exclude_instruments_A != [] and
            exclude_instruments_B != []
        ):
        match_exclude_instruments_combined=True

    if (
            (
                (not set(exclude_moods_A).isdisjoint(mood_list) and mood_list != []) or
                (not set(exclude_moods_A).isdisjoint(genres_local) and genres_local != [])
            )    
             and 
            (
                (not set(exclude_moods_B).isdisjoint(mood_list) and mood_list != []) or
                (not set(exclude_moods_B).isdisjoint(genres_local) and genres_local != [])
            ) and 
            exclude_moods_A != [] and
            exclude_moods_B != []
        ):
        match_exclude_moods_combined=True

    if (
            (
                (not set(exclude_moods).isdisjoint(genres_local)) or
                (not set(exclude_moods).isdisjoint(mood_list))
            ) and
            exclude_moods != []
        ):
        match_exclude_moods=True


    try:
        if (
                (not set(exclude_artists).isdisjoint([artist])) and
                exclude_artists != []
            ):
            match_exclude_artists=True
    except:
        logger.debug(f"match_exclude_artists remains false for: {genre_to_append}")


    if (
            (match_genre == True) or
            (match_genre_combined == True) or
            (
                (match_genre_substring == True) and
                (substring == True)   
            )
        ):
        match_with_genre=True

    if (
            (match_playlists_with_track == True) or
            (match_playlist_with_track_combined == True) or
            (match_playlists_with_artist == True) or
            (match_playlist_with_artist_combined == True) or
            (
                (substring == True)  and
                (
                    (match_playlist_with_track_substring == True) or
                    (match_playlist_with_artist_substring == True)
                )
            )
        ):
        match_with_playlist=True

    if (#artist and genre
            (
                (
                    (artist != '') and
                    (match_artist == True)
                ) or
                (
                    (artist == '')
                )
            ) and
            ( match_with_genre == True )
        ):
        match_with_genre_and_artist=True

    if ( 
            (match_artist == True) and
            (artists_only == True)
        ):
        match_with_artist_only=True


    if ( 
            (playlists_only == True) and
            ( match_with_playlist == True )
        ):
        match_with_playlists_only=True

    if (
            (match_mood == True) or
            (match_moods_combined == True)
        ):
        match_with_mood=True

    if (
            (match_instruments == True) or
            (match_instruments_combined == True)
        ):
        match_with_instrument=True

    if ( 
            (mood_only == True) and
            ( match_with_mood == True )
        ):
        match_with_mood_only=True

    if (
            (match_with_genre_and_artist == True ) and
            (len(mood_list) == 0) and
            (len(instrument_list) == 0) and
            (len(gender_list) == 0) and
            (len(playlists_with_track) == 0) and
            (len(include_playlists_with_track) == 0) and
            (len(include_playlists_with_track_A) == 0) and
            (len(include_playlists_with_track_B) == 0) and
            (len(include_playlists_with_artist) == 0) and
            (len(include_playlists_with_artist_A) == 0) and
            (len(include_playlists_with_artist_B) == 0) and
            (len(include_instruments) == 0) and
            (len(include_instruments_A) == 0) and
            (len(include_instruments_B) == 0) and
            (len(include_genders) == 0) and
            (len(include_moods) == 0) and
            (len(include_moods_A) == 0) and
            (len(include_moods_B) == 0) and
            (len(playlists_with_artist) == 0)
        ):
        match_with_genre_and_artist_only=True

    if (
            (match_with_genre == True) and
            ( # moods
                (
                    (len(include_moods) == 0) and
                    (len(include_moods_A) == 0) and
                    (len(include_moods_B) == 0)
                ) or
                (match_with_mood == True)
            ) and
            ( # playlists
                (
                    (len(playlists_with_track) == 0) and
                    (len(include_playlists_with_track) == 0) and
                    (len(include_playlists_with_track_A) == 0) and
                    (len(include_playlists_with_track_B) == 0) and
                    (len(include_playlists_with_artist) == 0) and
                    (len(include_playlists_with_artist_A) == 0) and
                    (len(include_playlists_with_artist_B) == 0)
                ) or
                (match_with_playlist == True)
            ) and
            ( # instruments
                (
                    (len(include_instruments) == 0) and
                    (len(include_instruments_A) == 0) and
                    (len(include_instruments_B) == 0)
                ) or
                (match_with_instrument == True)
            ) and
            ( # gender
                (
                    (len(gender_list) == 0) and
                    (len(include_genders) == 0)
                ) or
                (match_genders == True)
            )
        ):
        match_with_everything_available=True

    match_bool=False
    exclude_bool=False

    if (
            (match_with_artist_only == True ) or
            (match_with_playlists_only == True) or
            (match_with_mood_only == True) or
            (match_with_genre_and_artist_only == True) or
            (match_with_everything_available == True)
        ):
        match_bool=True

    if (
            (match_exclude_artists == True ) or
            (match_exclude_moods_combined == True ) or
            (match_exclude_moods == True ) or
            (match_exclude_genres == True ) or
            (match_exclude_genres_combined == True ) or
            (match_exclude_playlists_with_track == True ) or
            (match_exclude_playlists_with_track_combined == True) or
            (match_exclude_playlists_with_artist == True ) or
            (match_exclude_playlists_with_artist_combined == True ) or
            (match_exclude_instruments == True ) or
            (match_exclude_instruments_combined == True ) or
            (match_exclude_genders == True )
        ):
        exclude_bool=True    

    if (
        (
            (exclude_only == True) or
            (match_bool == True)
        ) and
        (exclude_bool == False)
    ):
        genres_local.append(genre_to_append)

    if (debug == True):
        match_values = {}
        match_values["match_bool"]=match_bool
        match_values["exclude_bool"]=exclude_bool
        match_values["matches"] = {}
        match_values["excludes"] = {}
    
        match_values["matches"]["match_artist"]=str(match_artist)
        match_values["excludes"]["match_exclude_artists"]=str(match_exclude_artists)

        match_values["matches"]["match_mood"]=str(match_mood)
        match_values["matches"]["match_moods_combined"]=str(match_moods_combined)
        match_values["excludes"]["match_exclude_moods_combined"]=str(match_exclude_moods_combined)
        match_values["excludes"]["match_exclude_moods"]=str(match_exclude_moods)

        match_values["matches"]["match_genre"]=str(match_genre)
        match_values["matches"]["match_genre_combined"]=str(match_genre_combined)
        match_values["matches"]["match_genre_substring"]=str(match_genre_substring)
        match_values["excludes"]["match_exclude_genres"]=str(match_exclude_genres)
        match_values["excludes"]["match_exclude_genres_combined"]=str(match_exclude_genres_combined)

        match_values["matches"]["match_playlists_with_track"]=str(match_playlists_with_track)
        match_values["matches"]["match_playlist_with_track_substring"]=str(match_playlist_with_track_substring)
        match_values["matches"]["match_playlist_with_track_combined"]=str(match_playlist_with_track_combined)
        match_values["excludes"]["match_exclude_playlists_with_track"]=str(match_exclude_playlists_with_track)
        match_values["excludes"]["match_exclude_playlists_with_track_combined"]=str(match_exclude_playlists_with_track_combined)

        match_values["matches"]["match_playlists_with_artist"]=str(match_playlists_with_artist)
        match_values["matches"]["match_playlist_with_artist_substring"]=str(match_playlist_with_artist_substring)
        match_values["matches"]["match_playlist_with_artist_combined"]=str(match_playlist_with_artist_combined)
        match_values["excludes"]["match_exclude_playlists_with_artist"]=str(match_exclude_playlists_with_artist)
        match_values["excludes"]["match_exclude_playlists_with_artist_combined"]=str(match_exclude_playlists_with_artist_combined)

        match_values["matches"]["match_instruments"]=str(match_instruments)
        match_values["matches"]["match_instruments_combined"]=str(match_instruments_combined)
        match_values["excludes"]["match_exclude_instruments"]=str(match_exclude_instruments)
        match_values["excludes"]["match_exclude_instruments_combined"]=str(match_exclude_instruments_combined)

        match_values["matches"]["match_genders"]=str(match_genders)
        match_values["excludes"]["match_exclude_genders"]=str(match_exclude_genders)
        

        match_values["matches"]["match_with_artist_only"]=str(match_with_artist_only)
        match_values["matches"]["match_with_instrument"]=str(match_with_instrument)
        match_values["matches"]["match_with_playlist"]=str(match_with_playlist)
        match_values["matches"]["match_with_playlists_only"]=str(match_with_playlists_only)
        match_values["matches"]["match_with_mood"]=str(match_with_mood)
        match_values["matches"]["match_with_mood_only"]=str(match_with_mood_only)
        match_values["matches"]["match_with_genre"]=str(match_with_genre)
        match_values["matches"]["match_with_genre_and_artist"]=str(match_with_genre_and_artist)
        match_values["matches"]["match_with_genre_and_artist_only"]=str(match_with_genre_and_artist_only)
        match_values["matches"]["match_with_everything_available"]=str(match_with_everything_available)
        print(json.dumps(match_values, indent=1))


    if (
        (
            len(exclude_artists) > 0  and
            (match_exclude_artists == True)  and
            (not set([genre_to_append]).isdisjoint(genres_local))
        )
    ):
        genres_local.remove(genre_to_append)

    return genres_local

#def neue_deutsche_haerte(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
#    if (('neue deutsche harte' in  genres_remote) and ('Neue Deutsche Härte' not in  genres_local)):
#        genres_local = np.append(genres_local, "Neue Deutsche Härte")
#        joined_genres = ";".join(genres_local)
#        genre_set(path, joined_genres)
#        print(EasyID3(path)['Genre'])
#    return genres_local


def hip_rap(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    include_genres= [
        'old school rap',
        'country rap',
        'scam rap',
        'russian gangster rap',
        'nyc rap',
        'german cloud rap',
        'new jersey rap',
        'chicano rap',
        'melodic rap',
        'schweizer rap',
        'alabama rap',
        'christlicher rap',
        'dmv rap',
        'cali rap',
        'philly rap',
        'london rap',
        'indie pop rap',
        'emo rap',
        'chicago rap',
        'bayerischer rap',
        'ottawa rap',
        'comedy rap',
        'west coast rap',
        'underground rap',
        'dirty south rap',
        'jazz rap',
        'gangster rap',
        'pop rap',
        'german alternative rap',
        'native american hip hop',
        'bronx hip hop',
        'portland hip hop',
        'ohio hip hop',
        'old school uk hip hop',
        'indie hip hop',
        'north carolina hip hop',
        'milwaukee hip hop',
        'lithuanian hip hop',
        'latin hip hop',
        'mississippi hip hop',
        'persian hip hop',
        'harlem hip hop',
        'bc underground hip hop',
        'memphis hip hop',
        'asian american hip hop',
        'miami hip hop',
        'swiss hip hop',
        'hawaiian hip hop',
        'danish hip hop',
        'cologne hip hop',
        'west australian hip hop',
        'desi hip hop',
        'zambian hip hop',
        'russian hip hop',
        'turkish hip hop',
        'australian underground hip hop',
        'dutch hip hop',
        'birmingham hip hop',
        'canadian indigenous hip hop',
        'south african hip hop',
        'uk hip hop',
        'spiritual hip hop',
        'queens hip hop',
        'seattle hip hop',
        'canadian hip hop',
        'psychedelic hip hop',
        'australian hip hop',
        'israeli hip hop',
        'spanish hip hop',
        'scottish hip hop',
        'oakland hip hop',
        'lgbtq+ hip hop',
        'french hip hop',
        'jewish hip hop',
        'industrial hip hop',
        'minnesota hip hop',
        'deep german hip hop',
        'detroit hip hop',
        'experimental hip hop',
        'abstract hip hop',
        'old school hip hop',
        'uk alternative hip hop',
        'underground hip hop',
        'hardcore hip hop',
        'atl hip hop',
        'southern hip hop',
        'east coast hip hop',
        'political hip hop',
        'hamburg hip hop',
        'conscious hip hop',
        'alternative hip hop',
        'german hip hop',
        'j-rap',
        'hip hop'
    ]
    genre_to_append = 'Hip Rap'
    exclude_artists = [
        'Otava Yo',
        'Zwo Eins Risiko',
        'Birdy Nam Nam',
        'Danger Dan',
        'Dodie',
        'Darren Korb',
        'Bo Burnham',
        'System Of A Down',
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        artist=t["artist"],
        include_genres=include_genres,
        exclude_artists=exclude_artists,
        exclude_genres=[genre_to_append]
    )
    return genres_local

def german_hip_hop(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'German Hip Hop'
    include_genres= [
       'german hip hop',
        'deep german hip hop',
        'bayerischer rap',
        'hamburg hip hop',
        'german cloud rap',
        'indie deutschrap',
        'german alternative rap',
        'schweizer rap',
        'swiss rap '
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        include_genres=include_genres,
        exclude_genres=[genre_to_append]
    )
    return genres_local

def metal(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Metal'
    include_genres_from_comment = [
        'jazz metal',
        'folk black metal',
        'sci-fi metal',
        'central american metal',
        'christian metal',
        'black metal',
        'norwegian black metal',
        'swedish heavy metal',
        'technical groove metal',
        'florida death metal',
        'doom metal',
        'technical death metal',
        'cyber metal',
        'greek metal',
        'dutch death metal',
        'utah metal',
        'icelandic metal',
        'italian power metal',
        'canadian metal',
        'finnish heavy metal',
        'russian metal',
        'birmingham metal',
        'uk doom metal',
        'dutch metal',
        'brazilian groove metal',
        'brazilian metal',
        'brazilian thrash metal',
        'italian folk metal',
        'belgian metal',
        'drone metal',
        'icelandic black metal',
        'pagan black metal',
        'viking black metal',
        'buffalo ny metal',
        'russian folk metal',
        'symphonic death metal',
        'boston metal',
        'louisiana metal',
        'kawaii metal',
        'slavic metal',
        'welsh metal',
        'symphonic black metal',
        'danish black metal',
        'post-black metal',
        'italian metal',
        'gothenburg metal',
        'post-doom metal',
        'autonomous black metal',
        'german thrash metal',
        'french death metal',
        'brutal death metal',
        'tolkien metal',
        'norwegian metal',
        'swedish metal',
        'neo-trad metal',
        'french metal',
        'southern metal',
        'instrumental progressive metal',
        'australian metal',
        'melodic groove metal',
        'progressive metal',
        'swedish power metal',
        'sludge metal',
        'finnish power metal',
        'progressive groove metal',
        'vancouver metal',
        'chinese metal',
        'glam metal',
        'german pagan metal',
        'austrian metal',
        'deep folk metal',
        'swedish death metal',
        'finnish death metal',
        'opera metal',
        'danish metal',
        'latin metal',
        'retro metal',
        'german metal',
        'symphonic power metal',
        'gothic metal',
        'polish folk metal',
        'polish metal',
        'scottish metal',
        'canadian metal',
        'swedish metal',
        'celtic metal',
        'oriental metal',
        'slavic folk metal',
        'gothic symphonic metal',
        'thrash metal',
        'stoner metal',
        'avant-garde metal',
        'swiss metal',
        'death metal',
        'irish metal',
        'speed metal',
        'symphonic metal',
        'neo classical metal',
        'melodic metal',
        'finnish metal',
        'groove metal',
        'comic metal',
        'melodic metal',
        'melodic death metal',
        'viking metal',
        'funk metal',
        'german metal',
        'folk metal',
        'rap metal',
        'power metal',
        'industrial metal',
        'nu metal',
        'metal',
        'alternative metal'
    ]
    include_genres_from_genres = []
    include_genres = include_genres_from_genres + include_genres_from_comment
    for genre in include_genres_from_comment:
        include_genres.append(genre.title())
    exclude_genres = [
        genre_to_append,
        'Futurepop'
    ]
    exclude_artists = [
        'Bloodhound Gang',
        'Moderat',
        'The Offspring',
        'Otava Yo',
        'Red Hot Chili Peppers',
        'Sum 41'
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append='Metal',
        artist=t["artist"],
        include_genres=include_genres,
        exclude_genres=exclude_genres,
        exclude_artists=exclude_artists
    )
    return genres_local


def nu_metal(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Nu Metal'
    include_genres = [
        'nu metal'
    ]
    exclude_genres = [
        genre_to_append
    ]
    exclude_artists = [
        'Bloodhound Gang',
        'Sum 41'
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        artist=t["artist"],
        include_genres=include_genres,
        exclude_genres=exclude_genres,
        exclude_artists=exclude_artists
    )
    return genres_local



def chillhop(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    chillhop_genres= [
        'ambeat',
        'chillhop',
        'japanese chillhop'
    ]
    if ((not set(chillhop_genres).isdisjoint(genres_remote)) and ('Chillóp' not in  genres_local)):
        genres_local.append("Chillóp")
    return genres_local

def arabic(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    arabic_genres= [
        'arab alternative'
    ]
    if ((not set(arabic_genres).isdisjoint(genres_remote)) and ('Arabic' not in  genres_local)):
        genres_local.append("Arabic")
    return genres_local

def japanese(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Japanese'
    #if (genre_to_append in  genres_local):
    #    genres_local.remove(genre_to_append)
    include_genres= [
        'japanese soundtrack',
        'j-rap',
        'japanese math rock',
        'japanese post-rock',
        'j-rock',
        'j-poprock',
        'j-reggae',
        'j-pop'
    ]
    exclude_genres = [
        genre_to_append,
    ]
    include_artists = [
        'Hifana'
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        include_genres=include_genres,
        exclude_genres=exclude_genres
    )
    return genres_local

def reggae(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Reggae'
    include_genres= [
        'reggae rock',
        'french reggae',
        'uk reggae',
        'roots reggae',
        'deep ragga'
    ]
    exclude_genres = [
        genre_to_append,
        'indie folk'
    ]
    exclude_artists = [
        'State Radio',
        'Iam',
        'Skindred',
        'Dispatch'
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        artist=t["artist"],
        include_genres=include_genres,
        exclude_genres=exclude_genres,
        exclude_artists=exclude_artists
    )
    return genres_local

def electropop(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append ='Electropop'
    # if (genre_to_append in  genres_local):
    #     genres_local.remove(genre_to_append)
    exclude_genres = [
        genre_to_append,
        'Acoustic',
        'Indie Café',
        'Mashup',
        'chillwave',
        'Indie Rock',
        'Shoegaze',
        'Hip Rap',
        'neo mellow',
        'Radio5',
        'Radio3',
        'Radio1',
        'pop soul',
        'Radio6',
        'piano rock'
        'vapor soul'
        'vapor twitch'
    ]
    exclude_artists = [
        'Coldplay',
        'Taylor Swift'
    ]
    include_genres= [
        'electropop',
        'indie electropop',
        'australian electropop',
        'japanese electropop',
        'chinese electropop',
        'danish electropop',
        'spanish electropop',
        'canadian electropop',
        'swedish electropop',
        'canadian electropop'
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        t["genres"]["spotify"],
        genre_to_append,
        artist=t["artist"],
        exclude_artists=exclude_artists,
        include_genres=include_genres,
        exclude_genres=exclude_genres
    )

    include_playlists_with_artist = [
        'The Sounds of Spotify - The Sound of Electropop',
        'Particle Detector - Intro to Electropop',
        'Particle Detector - The Pulse of Electropop',
        'Particle Detector - The Edge of Electropop',
        'The Sounds of Spotify - The Sound of Danish Electropop',
        'The Sounds of Spotify - The Sound of Swedish Electropop',
        'The Sounds of Spotify - The Sound of Canadian Electropop',
        'The Sounds of Spotify - The Sound of Japanese Electropop',
        'The Sounds of Spotify - The Sound of Indie Electropop',
        'The Sounds of Spotify - The Sound of Spanish Electropop',
        'The Sounds of Spotify - The Sound of Korean Electropop',
        'The Sounds of Spotify - The Sound of Chinese Electropop',
        'The Sounds of Spotify - The Sound of Australian Electropop',
    ]
    #genre_list = append_to_genre_list_v2(
    #    genre_list,
    #    spotify_genres_from_comment,
    #    genre_to_append,
    #    artist=t["artist"],
    #    exclude_artists=exclude_artists,
    #    playlists_only=True,
    #    playlists_with_artist=t["playlists"]["with_artist"],
    #    include_playlists_with_artist=include_playlists_with_artist,
    #    exclude_genres=exclude_genres
    #)

    include_playlists_with_track = [
        'The Sounds of Spotify - The Sound of Electropop',
        'Particle Detector - Intro to Electropop',
        'Particle Detector - The Pulse of Electropop',
        'Particle Detector - The Edge of Electropop',
        'The Sounds of Spotify - The Sound of Danish Electropop',
        'The Sounds of Spotify - The Sound of Swedish Electropop',
        'The Sounds of Spotify - The Sound of Canadian Electropop',
        'The Sounds of Spotify - The Sound of Japanese Electropop',
        'The Sounds of Spotify - The Sound of Indie Electropop',
        'The Sounds of Spotify - The Sound of Spanish Electropop',
        'The Sounds of Spotify - The Sound of Korean Electropop',
        'The Sounds of Spotify - The Sound of Chinese Electropop',
        'The Sounds of Spotify - The Sound of Australian Electropop',
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        t["genres"]["spotify"],
        genre_to_append,
        playlists_only=True,
        artist=t["artist"],
        exclude_artists=exclude_artists,
        playlists_with_track=t["playlists"]["with_track"],
        include_playlists_with_track=include_playlists_with_track,
        exclude_genres=[genre_to_append]
    )
    return genres_local

def reggae_rock(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    include_genres= [
        'reggae rock',
    ]
    genre_to_append ='Reggae Rock'
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        include_genres=include_genres,
        exclude_genres=[genre_to_append]
    )
    return genres_local


def rock(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    rock_genres= [
        'rock',
        'album rock',
        'icelandic rock',
        'pop rock',
        'alternative rock',
        'rock-and-roll'
    ]
    if ((not set(rock_genres).isdisjoint(genres_remote)) and ('Rock' not in  genres_local)):
        genres_local.append("Rock")
    return genres_local

def lo_fi_beats(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = "Chillóp"
    include_genres=[
        "lo-fi beats",
        "lo-fi chill",
    ]
    exclude_genres=[
        "Comedy",
        "escape room",
        "Escape Room",
        "Indie Dance",
        "German Indie",
        "Jazzap",
    ]
    exclude_artists=[
        "Freundeskreis, Joy Denalane",
        "Laing",
        "Nelly Furtado",
        "Charles & Eddie",
        "Justin Timberlake",
        "Pras, Ol' Dirty Bastard, Mya",
        "fun., Janelle Monáe",
        "Fort Minor, John Legend",
        "Kanye West, Jamie Foxx",
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        artist=t["artist"],
        exclude_artists=exclude_artists,
        exclude_genres=exclude_genres,
        include_genres=include_genres
    )

    include_playlists_with_track = [
        "The Sounds of Spotify - The Sound of Lo-Fi",
        "Lofi Girl - lofi hip hop music - beats to relax/study to",
        "Lofi Girl - lofi hip hop music - beats to sleep/chill to",
        "Lofi Girl - Study Session - lofi beats to focus/work to",
        "Lofi Girl - Lofi Girl - Best of lofi hip hop 2021 ✨",
        "Majestic Casual - Lofi Hip Hop ",
        "The Sounds of Spotify - The Sound of Lo-Fi Jazzhop",
        "The Sounds of Spotify - The Sound of Lo-Fi Beats",
        "The Sounds of Spotify - The Sound of Lo-Fi Chill",
        "Spotify - Chill Lofi Study Beats",
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        playlists_only=True,
        playlists_with_track=t["playlists"]["with_track"],
        include_playlists_with_track=include_playlists_with_track,
        exclude_genres=exclude_genres
    )
    return genres_local

def schwarze_nacht(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    schwarze_nacht_genres= [
        'dark wave',
        'futurepop',
        'dark post-punk',
        'electro-industrial',
        'aggrotech',
        'corrosion',
        'dark electro'
    ]
    schwarze_nacht_exclude_genres = [
        'Schwarze Nacht',
        '8-Bit'
    ]
    if (
        ((not set(schwarze_nacht_genres).isdisjoint(genres_remote))  or (not set(schwarze_nacht_genres).isdisjoint(genres_local)))
        and not (
            (not set(schwarze_nacht_exclude_genres).isdisjoint(genres_local)) or (not set(schwarze_nacht_exclude_genres).isdisjoint(genres_remote))
            )
    ):
        genres_local.append("Schwarze Nacht")
    return genres_local


def minimal_effort(t, genre_to_append,other_matching_genres=[]) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    include_genres=[
        genre_to_append.lower()
    ]
    if len(other_matching_genres) > 0:
        include_genres = include_genres + [x.lower() for x in other_matching_genres]
    exclude_genres=[
        genre_to_append
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        exclude_genres=exclude_genres,
        include_genres=include_genres
    )

    include_playlists_with_track = [
        'The Sounds of Spotify - The Sound of '+ genre_to_append,  
    ]
    if other_matching_genres:
        include_playlists_with_track = include_playlists_with_track + ['The Sounds of Spotify - The Sound of '+ x for x in other_matching_genres]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        playlists_only=True,
        playlists_with_track=t["playlists"]["with_track"],
        include_playlists_with_track=include_playlists_with_track,
        exclude_genres=exclude_genres
    )
    return genres_local


def neo_soul(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = "Neo Soul"
    # if (genre_to_append in  genres_local):
    #     genres_local.remove(genre_to_append)
    include_genres=[
        "neo soul"
    ]
    exclude_genres=[
        "Comedy",
        "escape room",
        "Escape Room",
        "Indie Dance",
        "German Indie",
        "Jazzap",
    ]
    exclude_artists=[
        "Freundeskreis, Joy Denalane",
        "Laing",
        "Soom T",
        "Nelly Furtado",
        "Charles & Eddie",
        "Justin Timberlake",
        "Pras, Ol' Dirty Bastard, Mya",
        "fun., Janelle Monáe",
        "Fort Minor, John Legend",
        "Kanye West, Jamie Foxx",
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        artist=t["artist"],
        exclude_artists=exclude_artists,
        exclude_genres=exclude_genres,
        include_genres=include_genres
    )

    include_playlists_with_track = [
        'The Sounds of Spotify - The Sound of Neo Soul',  
        'Spotify - Next Wave Neo-Soul',  
        'Spotify - I Love My Neo-Soul',  
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        playlists_only=True,
        playlists_with_track=t["playlists"]["with_track"],
        include_playlists_with_track=include_playlists_with_track,
        exclude_genres=exclude_genres
    )
    return genres_local

def fierce_femmes(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append ='Fierce Femmes'
    if (genre_to_append in  genres_local):
        genres_local.remove(genre_to_append)
    exclude_genres=[
        genre_to_append,
        'Carefree',
        'Cool Off',
        'Quiet',
        'Coverchill',
        'atmospheric post-rock',
        'Focus',
        'Calm',
        'Epic',
        'Guilty Pleasures',
        'Radio Chill'
    ]
    include_genres_A=[
        'alt-z',
        'modern alternative rock',
        'modern rock',
        'indie poptism',
        'pop',
        'electropop',
        'pixie',
        'indie pop',
        'indie electropop',
        'pop punk',
        'social media pop',
        'slayer',
        'indie punk',
        'rock',
        'australian garage punk',
        'pop emo',
        'modern alternative rock',
        'escape room',
        'alternative metal',
        'dance pop',
    ]
    include_genres_B = [
        'Loud',
        'Amped',
        'Forward',
    ]
    exclude_artists=[
        'Basshunter',
        'AJR',
        'ABBA',
        'Billy Talent',
        'Britney Spears',
        'Cascade',
        'Edguy',
        "Christina Aguilera, Lil' Kim, Mýa, P!nk",
        'Knorkator',
        'Faith And The Muse',
        'Deadlock',
        'Covet',
        'Dobocarol',
        'Christina Aguilera',
        'Beyonce',
        'JAY Z',
        'Belinda Carlisle',
        'Eminem',
        'Bondage Fairies',
        'Falco',
    ]
    include_moods = [
        'cyanite-energetic',
        'cyanite-aggressive',
    ]
    exclude_moods = [
        'cyanite-calm',
        'cyanite-romantic',
        'cyanite-ethereal',
        'cyanite-chilled',
    ]
    include_playlists_with_artist = [
        'Spotify - Fierce Femmes', 
        'Spotify - Riot Grrrl', 
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        playlists_only=True,
        playlists_with_artist=t["playlists"]["with_artist"],
        include_playlists_with_artist=include_playlists_with_artist,
        exclude_genres=exclude_genres
    )
    # if t["cyanite_additional_info"]:
    #     if (
    #         (cyanite_additional_info["energyLevel"] == "high") and
    #         (float(cyanite_additional_info['voice'].get('female') or 0) > float(0.9)) and
    #          (cyanite_additional_info["predominantVoiceGender"] == "female") 
    #         ):
    #         # genre_list = append_to_genre_list_v2(
    #         #     genre_list,
    #         #     comment_list,
    #         #     genre_to_append,
    #         #     artist=t["artist"],
    #         #     exclude_artists=exclude_artists,
    #         #     exclude_genres=exclude_genres,
    #         #     include_genres_A=include_genres_A,
    #         #     include_genres_B=include_genres_B
    #         # )
    #         genres_local = append_to_genre_list_v2(
    # ["_"]["cal"],
    # ["_"]["mote"],
    #             genre_to_append,
    #             artist=t["artist"],
    #             mood_list=t["mood"]["all"],
    #             include_moods=include_moods,
    #             exclude_moods=exclude_moods,
    #             exclude_artists=exclude_artists,
    #             exclude_genres=exclude_genres,
    #             include_genres=include_genres_A
    #         )
    return genres_local

def aggrotech(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append ='Aggrotech'
    include_genres=[
        'aggrotech'
    ]
    exclude_genres=[
        genre_to_append
    ]
    exclude_artists=[
        'Covenant'
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        artist=t["artist"],
        exclude_genres=exclude_genres,
        exclude_artists=exclude_artists,
        include_genres=include_genres
    )
    return genres_local


def retronight(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append ='RetroNight'
    include_genres=[
        'dark synth',
        'synthwave'
    ]
    exclude_genres=[
        genre_to_append
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        exclude_genres=exclude_genres,
        include_genres=include_genres
    )
    return genres_local



def retrochill(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = "RetroChill"
    include_genres= [
        'chillwave',
        'Cool Off'
        'Dreamy'
    ]
    exclude_genres= [
        'RetroRun',
        'RetroChill'
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        exclude_genres=exclude_genres,
        include_genres=include_genres
    )
    return genres_local

def retrorun(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append ='RetroRun'
    include_genres=[
        'RetroNight'
    ]
    exclude_genres=[
        genre_to_append,
        'RetroChill'
    ]
    if t["spotify_additional_info"]["features"]:
        danceability = t["spotify_additional_info"]["features"]['danceability']
        energy = t["spotify_additional_info"]["features"]['energy']
        if (
            ('dance' in t["mood"]["all"]) and
            ('beat' in t["instruments"]["all"]) and
            ('drums' in t["instruments"]["all"]) and
            ('synth' in t["instruments"]["all"]) and
            (float(0.4) < float(danceability)) and 
            (float(0.6) < float(energy))
        ):
            genres_local = append_to_genre_list_v2(
                genres_local,
                genres_remote,
                genre_to_append,
                exclude_genres=exclude_genres,
                include_genres=include_genres
            )
    return genres_local

def tunnel(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append ='SpotifyTunnel'
    if (genre_to_append in  genres_local):
        genres_local.remove(genre_to_append)
    include_genres_A=[
        'Dark Clubbing',
        'Cyberpunk 2077',
    ]
    include_genres_B=[
        'Amped'
    ]
    exclude_genres=[
        genre_to_append,
        'Tunnel',
        'Synthwave',
        'RetroChill',
        'Powerful Ambient Electronic'
    ]
    # if t["spotify_additional_info"]["features"]:
    #     danceability = t["spotify_additional_info"]["features"]['danceability']
    #     energy = t["spotify_additional_info"]["features"]['energy']
    #     if (
    #         (float(0.7) < float(danceability)) and 
    #         (float(0.7) < float(energy))
    #     ):
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        exclude_genres=exclude_genres,
        include_genres_A=include_genres_A,
        include_genres_B=include_genres_B
    )
    return genres_local

# def cool_off(t) -> list:
#     genres_remote = t["genres"]["remote"]
#     genres_local = t["genres"]["local"]
#     #genre_to_append= 'Cool Off'
#     genre_to_append= 'Feature2'
#     exclude_artists= [
#         'clipping.'
#     ]
#     exclude_genres= [
#         genre_to_append,
#         'Dreamy',
#         'RetroRun',
#         'Rowdy',
#         'ZaTa!',
#         'Aggrotech',
#         'Power Metal',
#         'Brash',
#         'Hanging Out',
#         'Warm On',
#         'Forwardish',
#         'Banger',
#         'Forward',
#         'Powerful',
#         'Epic',
#         'Tunnel',
#         'Speed Metal',
#         'Küche',
#         'Calm',
#         'Aggressive',
#         'Empowering'
#     ]
#     if t["spotify_additional_info"]["features"]:
#         tempo = t["spotify_additional_info"]["features"]['tempo']
#         energy = t["spotify_additional_info"]["features"]['energy']
#         valence = t["spotify_additional_info"]["features"]['valence']
#         danceability = t["spotify_additional_info"]["features"]['danceability']
#         liveness = t["spotify_additional_info"]["features"]['liveness']
#         instrumentalness = t["spotify_additional_info"]["features"]['instrumentalness']
#         speechiness = t["spotify_additional_info"]["features"]['speechiness']
#         mode = t["spotify_additional_info"]["features"]['mode']
#         acousticness = t["spotify_additional_info"]["features"]['acousticness']
#         loudness = t["spotify_additional_info"]["features"]['loudness']
#         if (
#                 (            
#                     (
#                         (float(loudness) < float(-5))  and 
#                         (float(90) < float(tempo)) and 
#                         (float(tempo) < float(110)) and 
#                         (float(0.5) < float(energy)) and 
#                         (float(energy) < float(0.8))
#                     )or
#                     (
#                         (float(-10) < float(loudness)) and 
#                         (float(loudness) < float(-5))  and 
#                         (float(110) < float(tempo)) and
#                         (float(danceability) < float(0.7)) and
#                         (float(0.25) < float(energy))and
#                         (float(energy) < float(0.4))
#                     )or
#                     (
#                         (float(-10) < float(loudness)) and 
#                         (float(0.7) < float(acousticness)) and 
#                         (float(140) < float(tempo)) and
#                         (float(danceability) < float(0.7)) and
#                         (float(0.25) < float(energy))and
#                         (float(energy) < float(0.4))
#                     )or
#                     (
#                         (float(0.4) < float(danceability)) and
#                         (float(danceability) < float(0.7)) and
#                         (float(loudness) < float(-7))  and 
#                         (float(0.55) < float(energy)) and
#                         (float(tempo) < float(125)) and
#                         (float(valence) < float(0.4))
#                     )or
#                     (
#                         (float(0.65) < float(danceability)) and
#                         (float(danceability) < float(0.8))  and 
#                         (float(0.3) < float(energy)) and
#                         (float(energy) < float(0.7))  and 
#                         (float(loudness) < float(-8))  and 
#                         (float(-12) < float(loudness))  and 
#                         (float(0.3) < float(valence)) and
#                         (float(valence) < float(0.8)) and
#                         (float(80) < float(tempo)) and
#                         ( float(tempo) < float(110))
#                     )or

#                     (
#                             (float(0.6) < float(danceability)) and
#                             (float(danceability) < float(0.8))  and 
#                             (float(0.3) < float(energy)) and
#                             (float(energy) < float(0.7))  and 
#                             (float(loudness) < float(-8))  and 
#                             (float(-12) < float(loudness))  and 
#                             (float(80) < float(tempo)) and
#                             ( float(tempo) < float(115))
#                     )or(
#                             (float(120) < float(tempo)) and
#                             (float(danceability) < float(0.5)) 
#                     )or(
#                             (float(140) < float(tempo)) and
#                             (float(-5) > float(loudness)) and
#                             (float(0.7) > float(energy)) and
#                             (float(danceability) < float(0.6)) 
#                     #)or (
#                     #        (float(100) > float(tempo))and 
#                     #        (float(0.5) < float(danceability))
#                     )
#                 )and not
#                 (
#                     (
#                         ((float(-6) < float(loudness)) or (float(0.5) < float(energy))) and
#                         (float(150) < float(tempo))
#                     )or
#                     (
#                         (float(-8) > float(loudness)) and
#                         (float(0.5) < float(acousticness)) and
#                         (float(0.5) > float(energy))
#                     )or
#                     (
#                         (float(-10) > float(loudness)) or
#                         (float(-5) < float(loudness))
#                     )or
#                     (
#                         (float(-8.5) > float(loudness)) and
#                         (float(5) > float(danceability)) and
#                         (float(0.1) > float(speechiness)) and
#                         (float(0.85) < float(instrumentalness))
#                     )or
#                     (
#                         (float(125) < float(tempo)) and
#                         (float(0.7) < float(energy))
#                     )or
#                     (
#                         (float(140) < float(tempo)) and
#                         (float(0.9) < float(valence))
#                     )or
#                     (
#                         (float(-7) < float(loudness)) and
#                         (float(90) < float(tempo)) and
#                         (float(0.4) < float(energy)) and
#                         (float(0.5) < float(danceability))
#                     )or
#                     (
#                         (float(100) < float(tempo)) and
#                         (float(0.9) < float(energy)) and
#                         (float(0.5) < float(danceability))
#                     )or
#                     (
#                         (float(0.8) < float(energy)) and
#                         (float(0.8) < float(danceability)) and
#                         (float(-6) < float(loudness)) and 
#                         (float(110) < float(tempo))
#                     )or
#                     (
#                         (float(-7) < float(loudness)) and
#                         (float(0.5) < float(danceability)) and
#                         (float(0.5) > float(instrumentalness)) and
#                         (float(0.6) > float(speechiness)) and
#                         (float(0.1) > float(acousticness)) and
#                         (float(0.4) < float(valence)) and
#                         (float(0.8) < float(energy))
#                     )or
#                     (
#                         (float(0.7) < float(energy)) and
#                         (float(0.7) < float(danceability)) and
#                         (float(0.1) > float(acousticness)) and
#                         (float(-8) < float(loudness)) and
#                         (
#                             (float(0.15) < float(speechiness)) or
#                             (float(0.6) < float(valence))

#                         )and
#                         (float(120) < float(tempo))
#                     )
#                 )
#             ):
#             genres_local = append_to_genre_list_v2(
#                 genres_local,
#                 genres_remote,
#                 genre_to_append,
#                 artist=t["artist"],
#                 exclude_genres=exclude_genres,
#                 exclude_artists=exclude_artists,
#                 exclude_only=True
#             )
#             #if ('Cool Off' not in  genres_local):
#             #    genre_list.append("Feature2")
#             #print("yay")
#         else:
#             if ('Cool Off' in  genres_local):
#                 genres_local.append("MaybeNoCool")
#         if (
#                 (
#                     ('Cool Off'in  genres_local)and
#                     (
#                         (float(0.5) < float(liveness)) or
#                         (float(0.5) < float(instrumentalness)) 
#                     ) and
#                     (
#                         (float(0.5) > float(energy)) and
#                         (float(0.8) > float(danceability))
#                     )
#                 )
#             ):
#             genres_local.append("Feature1")
#     return genres_local

def level0(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append= 'Level0'
    exclude_genres= [
        genre_to_append,
        'Unterwasser'
    ]
    if t["spotify_additional_info"]["features"]:
        tempo = t["spotify_additional_info"]["features"]['tempo']
        energy = t["spotify_additional_info"]["features"]['energy']
        valence = t["spotify_additional_info"]["features"]['valence']
        danceability = t["spotify_additional_info"]["features"]['danceability']
        liveness = t["spotify_additional_info"]["features"]['liveness']
        instrumentalness = t["spotify_additional_info"]["features"]['instrumentalness']
        speechiness = t["spotify_additional_info"]["features"]['speechiness']
        mode = t["spotify_additional_info"]["features"]['mode']
        acousticness = t["spotify_additional_info"]["features"]['acousticness']
        loudness = t["spotify_additional_info"]["features"]['loudness']
        if (
                (float(-20) > float(loudness))
            ):
            genres_local = append_to_genre_list_v2(
                genres_local,
                genres_remote,
                genre_to_append,
                exclude_genres=exclude_genres,
                exclude_only=True
            )
    return genres_local

def level1(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append= 'Level1'
    exclude_genres= [
        genre_to_append,
        'Level0'
    ]
    if t["spotify_additional_info"]["features"]:
        tempo = t["spotify_additional_info"]["features"]['tempo']
        energy = t["spotify_additional_info"]["features"]['energy']
        valence = t["spotify_additional_info"]["features"]['valence']
        danceability = t["spotify_additional_info"]["features"]['danceability']
        liveness = t["spotify_additional_info"]["features"]['liveness']
        instrumentalness = t["spotify_additional_info"]["features"]['instrumentalness']
        speechiness = t["spotify_additional_info"]["features"]['speechiness']
        mode = t["spotify_additional_info"]["features"]['mode']
        acousticness = t["spotify_additional_info"]["features"]['acousticness']
        loudness = t["spotify_additional_info"]["features"]['loudness']
        if (
                (float(-15) > float(loudness)) and
                (float(0.5) > float(energy)) and
                (float(0.5) > float(danceability))
            ):
            genres_local = append_to_genre_list_v2(
                genres_local,
                genres_remote,
                genre_to_append,
                exclude_genres=exclude_genres,
                exclude_only=True
            )
    return genres_local

def level2(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append= 'Level2'
    exclude_genres= [
        genre_to_append,
        'Level0',
        'Level1'
    ]
    if t["spotify_additional_info"]["features"]:
        tempo = t["spotify_additional_info"]["features"]['tempo']
        energy = t["spotify_additional_info"]["features"]['energy']
        valence = t["spotify_additional_info"]["features"]['valence']
        danceability = t["spotify_additional_info"]["features"]['danceability']
        liveness = t["spotify_additional_info"]["features"]['liveness']
        instrumentalness = t["spotify_additional_info"]["features"]['instrumentalness']
        speechiness = t["spotify_additional_info"]["features"]['speechiness']
        mode = t["spotify_additional_info"]["features"]['mode']
        acousticness = t["spotify_additional_info"]["features"]['acousticness']
        loudness = t["spotify_additional_info"]["features"]['loudness']
        if (
                (float(-15) > float(loudness)) and
                (float(0.75) > float(energy))
            ):
            genres_local = append_to_genre_list_v2(
                genres_local,
                genres_remote,
                genre_to_append,
                exclude_genres=exclude_genres,
                exclude_only=True
            )
    return genres_local

def level3(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append= 'Level3'
    exclude_genres= [
        genre_to_append,
        'Level0',
        'Level1',
        'Level2'
    ]
    if t["spotify_additional_info"]["features"]:
        tempo = t["spotify_additional_info"]["features"]['tempo']
        energy = t["spotify_additional_info"]["features"]['energy']
        valence = t["spotify_additional_info"]["features"]['valence']
        danceability = t["spotify_additional_info"]["features"]['danceability']
        liveness = t["spotify_additional_info"]["features"]['liveness']
        instrumentalness = t["spotify_additional_info"]["features"]['instrumentalness']
        speechiness = t["spotify_additional_info"]["features"]['speechiness']
        mode = t["spotify_additional_info"]["features"]['mode']
        acousticness = t["spotify_additional_info"]["features"]['acousticness']
        loudness = t["spotify_additional_info"]["features"]['loudness']
        if (
                (float(-10) > float(loudness))and
                (float(0.5) > float(energy)) and
                (float(0.5) > float(danceability))
            ):
            genres_local = append_to_genre_list_v2(
                genres_local,
                genres_remote,
                genre_to_append,
                exclude_genres=exclude_genres,
                exclude_only=True
            )
    return genres_local

def level4(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append= 'Level4'
    exclude_genres= [
        genre_to_append,
        'Level0',
        'Level1',
        'Level2',
        'Level3'
    ]
    if t["spotify_additional_info"]["features"]:
        tempo = t["spotify_additional_info"]["features"]['tempo']
        energy = t["spotify_additional_info"]["features"]['energy']
        valence = t["spotify_additional_info"]["features"]['valence']
        danceability = t["spotify_additional_info"]["features"]['danceability']
        liveness = t["spotify_additional_info"]["features"]['liveness']
        instrumentalness = t["spotify_additional_info"]["features"]['instrumentalness']
        speechiness = t["spotify_additional_info"]["features"]['speechiness']
        mode = t["spotify_additional_info"]["features"]['mode']
        acousticness = t["spotify_additional_info"]["features"]['acousticness']
        loudness = t["spotify_additional_info"]["features"]['loudness']
        if (
                (float(-10) > float(loudness))and
                (float(0.75) > float(energy)) and
                (float(0.75) > float(danceability))
            ):
            genres_local = append_to_genre_list_v2(
                genres_local,
                genres_remote,
                genre_to_append,
                exclude_genres=exclude_genres,
                exclude_only=True
            )
    return genres_local

def level6(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append= 'Level6'
    exclude_genres= [
        genre_to_append,
        'Level0',
        'Level1',
        'Level2',
        'Level3',
        'Level4',
        'Level5'
    ]
    if t["spotify_additional_info"]["features"]:
        tempo = t["spotify_additional_info"]["features"]['tempo']
        energy = t["spotify_additional_info"]["features"]['energy']
        valence = t["spotify_additional_info"]["features"]['valence']
        danceability = t["spotify_additional_info"]["features"]['danceability']
        liveness = t["spotify_additional_info"]["features"]['liveness']
        instrumentalness = t["spotify_additional_info"]["features"]['instrumentalness']
        speechiness = t["spotify_additional_info"]["features"]['speechiness']
        mode = t["spotify_additional_info"]["features"]['mode']
        acousticness = t["spotify_additional_info"]["features"]['acousticness']
        loudness = t["spotify_additional_info"]["features"]['loudness']
        if (
                (float(-10) > float(loudness))and
                (float(131) > float(tempo))and
                (float(0.75) > float(danceability))
            ):
            genres_local = append_to_genre_list_v2(
                genres_local,
                genres_remote,
                genre_to_append,
                exclude_genres=exclude_genres,
                exclude_only=True
            )
    return genres_local

def level5(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append= 'Level5'
    exclude_genres= [
        genre_to_append,
        'Level0',
        'Level1',
        'Level2',
        'Level3',
        'Level4',
        'Level6'
    ]
    if t["spotify_additional_info"]["features"]:
        tempo = t["spotify_additional_info"]["features"]['tempo']
        energy = t["spotify_additional_info"]["features"]['energy']
        valence = t["spotify_additional_info"]["features"]['valence']
        danceability = t["spotify_additional_info"]["features"]['danceability']
        liveness = t["spotify_additional_info"]["features"]['liveness']
        instrumentalness = t["spotify_additional_info"]["features"]['instrumentalness']
        speechiness = t["spotify_additional_info"]["features"]['speechiness']
        mode = t["spotify_additional_info"]["features"]['mode']
        acousticness = t["spotify_additional_info"]["features"]['acousticness']
        loudness = t["spotify_additional_info"]["features"]['loudness']
        if (
                (float(-10) > float(loudness))and
                (float(0.75) > float(energy))
            ):
            genres_local = append_to_genre_list_v2(
                genres_local,
                genres_remote,
                genre_to_append,
                exclude_genres=exclude_genres,
                exclude_only=True
            )
    return genres_local

def level7(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append= 'Level7'
    exclude_genres= [
        genre_to_append,
        'Level0',
        'Level1',
        'Level2',
        'Level3',
        'Level4',
        'Level5',
        'Level6'
    ]
    if t["spotify_additional_info"]["features"]:
        tempo = t["spotify_additional_info"]["features"]['tempo']
        energy = t["spotify_additional_info"]["features"]['energy']
        valence = t["spotify_additional_info"]["features"]['valence']
        danceability = t["spotify_additional_info"]["features"]['danceability']
        liveness = t["spotify_additional_info"]["features"]['liveness']
        instrumentalness = t["spotify_additional_info"]["features"]['instrumentalness']
        speechiness = t["spotify_additional_info"]["features"]['speechiness']
        mode = t["spotify_additional_info"]["features"]['mode']
        acousticness = t["spotify_additional_info"]["features"]['acousticness']
        loudness = t["spotify_additional_info"]["features"]['loudness']
        if (
                (float(-10) > float(loudness))
            ):
            genres_local = append_to_genre_list_v2(
                genres_local,
                genres_remote,
                genre_to_append,
                exclude_genres=exclude_genres,
                exclude_only=True
            )
    return genres_local

def level8(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append= 'Level8'
    exclude_genres= [
        genre_to_append,
        'Level0',
        'Level1',
        'Level2',
        'Level3',
        'Level4',
        'Level5',
        'Level6'
    ]
    if t["spotify_additional_info"]["features"]:
        tempo = t["spotify_additional_info"]["features"]['tempo']
        energy = t["spotify_additional_info"]["features"]['energy']
        valence = t["spotify_additional_info"]["features"]['valence']
        danceability = t["spotify_additional_info"]["features"]['danceability']
        liveness = t["spotify_additional_info"]["features"]['liveness']
        instrumentalness = t["spotify_additional_info"]["features"]['instrumentalness']
        speechiness = t["spotify_additional_info"]["features"]['speechiness']
        mode = t["spotify_additional_info"]["features"]['mode']
        acousticness = t["spotify_additional_info"]["features"]['acousticness']
        loudness = t["spotify_additional_info"]["features"]['loudness']
        if (
                (float(-5) > float(loudness))and
                (float(0.5) > float(energy)) and
                (float(0.5) > float(danceability))
            ):
            genres_local = append_to_genre_list_v2(
                genres_local,
                genres_remote,
                genre_to_append,
                exclude_genres=exclude_genres,
                exclude_only=True
            )
    return genres_local


def forwardish(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append= 'Forwardish'
    exclude_genres= [
        genre_to_append,
        'Aggrotech',
        'Banger',
        'Aggressive'
    ]
    include_genres= [
        'Calm'
    ]
    if t["spotify_additional_info"]["features"]:
        tempo = t["spotify_additional_info"]["features"]['tempo']
        energy = t["spotify_additional_info"]["features"]['energy']
        danceability = t["spotify_additional_info"]["features"]['danceability']
        loudness = t["spotify_additional_info"]["features"]['loudness']
        if (
            (float(-7) < float(loudness)) and
            (float(loudness) < float(-5))  and
            (float(90) < float(tempo)) and
            (float(tempo) < float(120)) and
            (float(0.6) < float(energy)) and
            (float(energy) < float(0.8)) and
            (float(0.5) < float(danceability))
        ):
            genres_local = append_to_genre_list_v2(
                genres_local,
                genres_remote,
                genre_to_append,
                exclude_genres=exclude_genres,
                exclude_only=True
                #include_genres=include_genres
            )
    return genres_local

#def chilly(t) -> list:
# genres_remote = t["genres"]["remote"]
# genres_local = t["genres"]["local"]
#    genre_to_append= 'Feature1'
#    include_genres= [
#        'Indie Chill',
#        'Chillóp',
#        'RetroChill'
#    ]
#    exclude_genres= [
#        genre_to_append,
#        'Chilly',
#        'RetroRun',
#        'Hanging Out',
#        'Banger',
#        'Tunnel',
#        'Aggressive',
#        'Empowering'
#    ]
#    #if t["spotify_additional_info"]["features"]:
#    #    speechiness = t["spotify_additional_info"]["features"]['speechiness']
#    #    energy = t["spotify_additional_info"]["features"]['energy']
#    #    tempo = t["spotify_additional_info"]["features"]['tempo']
#    #    if (
#    #        (float(0.3) < float(speechiness)) and 
#    #        (float(0.45) > float(speechiness)) and 
#    #        (float(100) > float(tempo)) and 
#    #        (float(energy) < float(0.8))
#    #    ):
#    #        genres_local = append_to_genre_list_v2(
            # t_genres["local"],
            # t_genres["remote"],
#    #            genre_to_append,
#    #            exclude_genres=exclude_genres,
#    #            exclude_only=True
#    #        )
#    genres_local = append_to_genre_list_v2(
# ["_"]["cal"],
# ["_"]["mote"],
#        genre_to_append,
#        exclude_genres=exclude_genres,
#        include_genres=include_genres
#    )
#    return genres_local

def no_mood(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append= 'NoMood'
    exclude_genres= [
        genre_to_append,
        'Dreamy',
        'Soul Soothers',
        'Restless',
        'Comfort Zone',
        'Cool Off',
        'Unhinged',
        'Party',
        'Fast',
        'Slow',
        'Hanging Out',
        'Banger',
        'Flowing',
        'Rowdy',
        'Focus',
        'Forward',
        'Hängematte',
        'Forwardish',
        'Warm On',
        'Dancey',
        'Playful',
        'Ambient',
        'Soft',
        'Sad',
        'Zornie',
        'Uplifting',
        'Powerful',
        'Fun',
        'Carefree',
        'Calm',
        'Warm',
        'Aggressive',
        'Empowering',
        'Depri Disko',
        'Heartbreak',
        'Weltschmerz',
        'Hollow Release',
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        exclude_genres=exclude_genres,
        exclude_only=True
    )
    return genres_local

#def kueche(genre_list, comment_list,artist,title):
#    kueche_genres= [
#        'funk',
#        'Funk',
#        'soul',
#        'blues rock',
#        'jam band',
#        'r&b',
#        'brill building pop',
#        'rockabilly'
#    ]
#    kueche_exclude_genres= [
#        'Dreamy',
#        'RetroRun',
#        'Hanging Out',
#        'Electronic',
#        'gangster rap',
#        'pop rap',
#        'post-teen pop',
#        'synth funk',
#        'urban contemporary',
#        'old school hip hop',
#        'hardcore hip hop',
#        'edm',
#        'EDM',
#        'Banger',
#        'Pop',
#        'instrumental funk',
#        'dance pop',
#        'Schwarze Nacht',
#        'Tunnel',
#        'disco',
#        'blues rock',
#        'stomp and holler',
#        'rock',
#        'Rock',
#        'Lamenting',
#        'disco',
#        'downtempo',
#        'Folk',
#        'Hanging Out',
#        'Küche',
#        'Calm',
#        'Cool Off',
#        'Küche'
#    ]
#    if (
#        ((not set(kueche_genres).isdisjoint(genres_remote))  or (not set(kueche_genres).isdisjoint(genres_local)))
#        and not (
#            (not set(kueche_exclude_genres).isdisjoint(genres_local)) or (not set(kueche_exclude_genres).isdisjoint(genres_remote))
#            )
#    ):
#        genres_local.append("Küche")
#        #try:
#        #    print("Artist: " + str(artist))
#        #    print("Title: " + str(title))
#        #    print("Spotify genres: " + str(comment_list))
#        #    print("My genres: " + str(genre_list))
#        #    print("\n")
#        #except:
#        #    print("THIS CAUSED AN ERROR" + path_in_str )
#    return genres_local

def microhouse(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    if (('microhouse' in  genres_remote) and ('Microhouse' not in  genres_local)):
        genres_local.append("Microhouse")
    if (('microhouse' in  genres_remote) and ('Focus' not in  genres_local)):
        genres_local.append("Focus")
    return genres_local

def north(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'North'
    include_genres = [
        'Rune Folk',
        'Viking Folk',
        'Medieval Folk',
        'Neoclassical Darkwave'
    ]
    exclude_genres = [
        genre_to_append,
        'Futurepop',
        'Electronic',
        'Englisch',
        'Contemporary Classical Music',
        'oceania soundtrack',
        'Aggrotech',
        'Art Pop'
    ]
    exclude_artists = [
        'Nils Frahm'
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        artist=t["artist"],
        include_genres=include_genres,
        exclude_genres=exclude_genres,
        exclude_artists=exclude_artists
    )
    return genres_local

def medieval_focus(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Medieval Focus'
    include_genres = [
        'North'
    ]
    exclude_genres = [
        genre_to_append,
        'Dancey',
        'Distracting',
        'Neue Deutsche Härte'
    ]
    exclude_artists = [
        'Nils Frahm'
    ]
    if t["spotify_additional_info"]["features"]:
        energy = t["spotify_additional_info"]["features"]['energy']
        if (
            (float(0.8) > float(energy))
        ):
            genres_local = append_to_genre_list_v2(
                genres_local,
                genres_remote,
                genre_to_append,
                artist=t["artist"],
                include_genres=include_genres,
                exclude_artists=exclude_artists,
                exclude_genres=exclude_genres
            )
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        artist=t["artist"],
        include_genres=['Medieval Concentration'],
        exclude_artists=exclude_artists,
        exclude_genres=[genre_to_append]
    )
    return genres_local

def amped(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Amped'
    exclude_genres = [
        genre_to_append
    ]
    if t["spotify_additional_info"]["features"]:
        energy = t["spotify_additional_info"]["features"]['energy']
        if (
            (float(0.8) < float(energy))
        ):
            if ((genre_to_append not in  genres_local)):
                t["genres"]["local"].append(genre_to_append)
    return genres_local

def quiet(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Quiet'
    exclude_artists = ['Hypernova']
    exclude_genres = [
        genre_to_append,
        'NotQuiet',
        'Dancey'
    ]
    if t["spotify_additional_info"]["features"]:
        loudness = t["spotify_additional_info"]["features"]['loudness']
        if (
            (float(-60) < float(loudness)) and
            (float(-10) > float(loudness))
        ):
            genres_local = append_to_genre_list_v2(
                genres_local,
                genres_remote,
                genre_to_append,
                exclude_only=True,
                artist=t["artist"],
                exclude_artists=exclude_artists,
                exclude_genres=exclude_genres
            )
    return genres_local

def loud(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Loud'
    exclude_genres = [
        genre_to_append
    ]
    if t["spotify_additional_info"]["features"]:
        loudness = t["spotify_additional_info"]["features"]['loudness']
        if (
            (float(-5) < float(loudness))
        ):
            if ((genre_to_append not in  genres_local)):
                t["genres"]["local"].append(genre_to_append)
    return genres_local

# def instrumental_focus(t) -> list:
#     genres_remote = t["genres"]["remote"]
#     genres_local = t["genres"]["local"]
#     #genre_to_append = 'Instrumental Concentration'
#     genre_to_append = 'Feature1'
#     include_genres = [
#         'Focus'
#     ]
#     exclude_genres = [
#         genre_to_append
#     ]
#     if t["spotify_additional_info"]["features"]:
#         instrumentalness = t["spotify_additional_info"]["features"]['instrumentalness']
#         if (
#             (float(0.8) > float(instrumentalness))
#         ):
#             genres_local = append_to_genre_list_v2(
#                 genres_local,
#                 genres_remote,
#                 genre_to_append,
#                 include_genres=include_genres,
#                 exclude_genres=exclude_genres
#             )
#     return genres_local


def neoclassical_darkwave(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Neoclassical Darkwave'
    include_genres = [
        'neoclassical darkwave'
    ]
    exclude_genres = [
        genre_to_append
    ]
    exclude_artists = [
        'Nils Frahm'
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        artist=t["artist"],
        include_genres=include_genres,
        exclude_artists=exclude_artists,
        exclude_genres=exclude_genres
    )
    return genres_local


def jazz_rap(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Jazzap'
    include_genres = [
        'jazz rap'
    ]
    exclude_genres = [
        genre_to_append
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        include_genres=include_genres,
        exclude_genres=exclude_genres
    )

    include_playlists_with_track = [
        'Particle Detector - Intro to Jazz Rap',
        'Particle Detector - The Edge of Jazz Rap',
        'Particle Detector - The Pulse of Jazz Rap',
        'Spotify - Jazz Rap',
        'The Sounds of Spotify - The Sound of Jazz Rap',
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        playlists_only=True,
        playlists_with_track=t["playlists"]["with_track"],
        include_playlists_with_track=include_playlists_with_track,
        exclude_genres=exclude_genres
    )
    return genres_local

def feel_good_indie_rock(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Feel-Good Indie Rock'
    include_genres_A = [
        'Indie Rock'
    ]
    include_genres_B = [
        'Carefree',
        # 'SpotifyHappy'
    ]
    exclude_genres = [
        genre_to_append,
        'Sad',
        'House',
        'Lamenting',
        'EDM',
        'Quiet',
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        include_genres_A=include_genres_A,
        include_genres_B=include_genres_B,
        exclude_genres=exclude_genres
    )

    include_playlists_with_track = [
        'Spotify - Feel-Good Indie Rock',
        'Spotify - Feelgood Indie',
        'Spotify - Sing-Along Indie Hits',
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        playlists_only=True,
        playlists_with_track=t["playlists"]["with_track"],
        include_playlists_with_track=include_playlists_with_track,
        exclude_genres=exclude_genres
    )
    return genres_local

#swedish, finnish, norway, icelandic, italian, danish, finland, sweden, scandinavian,swiss, norwegian

def decolonized(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Decolonized'
    if (genre_to_append in  genres_local):
        genres_local.remove(genre_to_append)
    include_genres = [
        'african',
        #'afro',
        #'Afro',
        #'afro-cuban jazz',
        'arabic',
        'azonto',
        'balearic beat',
        'balearich',
        'bangla',
        'bangladeshi',
        #'barbados',
        'bhangra',
        'classic arab pop',
        'classic persian pop',
        #'columbia',
        #'cuban',
        'ethno',
        'farsi',
        'ghanaian alternative',
        'ghanaian hip hop',
        'hawaiian',
        'hebrew',
        'hungarian',
        'indian',
        'iran',
        'iranian',
        'iranian singers',
        'israel',
        'israeli composer',
        #'jamaican ska',
        #'j-rock',
        'katmandu',
        #'kolumbien',
        #'latin',
        #'latin pop',
        #'latin rock',
        'lebanese',
        'maldivian pop',
        #'mexican',
        'middle east',
        'mongolian',
        'negro spiritual',
        'nigerian',
        'oriental',
        'oriental metal',
        'pakistani',
        'persian',
        'persian hip hop',
        'punjabi',
        'raï',
        'romani',
        'romanian',
        'romanian band',
        'rumanian',
        'south africa',
        'sri lanka',
        'sri lankan',
        'tehran',
        'trinidadiens',
        'turkish',
        'afrofuturismo brasileiro',
        'indie curitibano',
    ]
        #'balkan', gypsy, iceland
    exclude_genres = [
        genre_to_append,
        'aggrotech',
        'Adult Standards',
        'american',
        'asian',
        'australia',
        'australian',
        'austrian',
        'Balkan',
        'berlin',
        'british asian',
        'british folk',
        'british rhythm & blues',
        'britpop',
        'brooklyn indie',
        'california',
        'canadia',
        'canadian',
        'canadien',
        'celtic',
        'china',
        'Chinese',
        'danish',
        'Deutsch',
        'deutschland',
        'deutschrock',
        'eurodance',
        'düsseldorf',
        'england',
        'france',
        'german',
        'germany',
        'greek',
        'hamburg',
        'irish',
        'japan',
        'japanese',
        'Japanese',
        'j-metal',
        'j-pop metal',
        'liedermaching',
        'neue deutsche härte',
        'new orleans',
        'new orleans funk',
        'North',
        'norway',
        'norwegian',
        'ny',
        'russian',
        'Russian',
        'scandinavian',
        'scotland',
        'seattle hip hop',
        'spain',
        'sweden',
        'swedish',
        'swedish rock',
        'swedish hip hop',
        'swiss',
        'swiss metal',
        'texas',
        'uk',
        'uk dance',
        'uk hip hop',
        'united kingdom',
        'united states',
        'usa',
        'viking',
    ]
    exclude_artists = [
        'The Lonely Island, Rihanna',
        'We Plants Are Happy Plants',
        'Rihanna',
        'The Lonely Island',
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        artist=t["artist"],
        exclude_artists=exclude_artists,
        include_genres=include_genres,
        exclude_genres=exclude_genres
    )

    #include_playlists_with_track = [
    #    'Decolonize Weekly',
    #]
    #genre_list = append_to_genre_list_v2(
    #    genre_list,
    #    comment_list,
    #    genre_to_append,
    #    playlists_only=True,
    #    playlists_with_track=t["playlists"]["with_track"],
    #    include_playlists_with_track=include_playlists_with_track,
    #    exclude_genres=exclude_genres
    #)
    return genres_local

def jazzy(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = "Jazzy"
    include_genres= [
        'jazz metal',
        'smooth jazz',
        'danish jazz',
        'korean jazz',
        'gypsy jazz',
        'jazz drums',
        'new orleans jazz',
        'british jazz',
        'jazz quartet',
        'ecm-style jazz',
        'ska jazz',
        'jazz guitar',
        'japanese jazz',
        'jazz pop',
        'jazz piano',
        'dark jazz',
        'jazz blues',
        'progressive jazz fusion',
        'jazz trumpet',
        'contemporary vocal jazz',
        'jazz organ',
        'jazz trio',
        'jazz rock',
        'electro jazz',
        'free jazz',
        'jazz saxophone',
        'turkish jazz',
        'avant-garde jazz',
        'modern jazz trio',
        'vocal jazz',
        'indie jazz',
        'soul jazz',
        'contemporary jazz',
        'cool jazz',
        'jazz',
        'french jazz',
        'jazz funk',
        'latin jazz',
        'jazz fusion',
        'jazz rap',
        'jazz boom bap',
        'bebop',
        'retro soul',
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        include_genres=include_genres,
        exclude_genres=[genre_to_append]
    )

    return genres_local


def dreamy_cleanup(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_remove = 'Dreamy'
    exclude_moods = [
        'musicnn-fast',
        'musicnn-loud',
        'musicnn-party',
        'musicnn-dance',
    ]
    if ((genre_to_remove in  genres_local)) and (not set(exclude_moods).isdisjoint(t["mood"]["all"]) and t["mood"]["all"] != ['']):
        genres_local.remove(genre_to_remove)
    return genres_local

    
    return genres_local

def dreamy(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Dreamy'
    exclude_genres = [
        genre_to_append,
        'Küche',
        'Slow Power',
        'Calm',
        'Cool Off',
        'Escape Room',
        'industrial',
        'Neue Deutsche Härte',
        'Aggrotech'
    ]
    if t["spotify_additional_info"]["features"]:
        energy = t["spotify_additional_info"]["features"]['energy']
        speechiness = t["spotify_additional_info"]["features"]['speechiness']
        tempo = t["spotify_additional_info"]["features"]['tempo']
        danceability = t["spotify_additional_info"]["features"]['danceability']
        loudness = t["spotify_additional_info"]["features"]['loudness']
        if (
            (float(-10) > float(loudness)) and
            (float(0.5) > float(danceability)) and
            (float(0.15) > float(speechiness)) and
            (float(125) > float(tempo)) and
            (float(0.3) > float(energy))
        ):
            genres_local = append_to_genre_list_v2(
                genres_local,
                genres_remote,
                genre_to_append,
                exclude_genres=exclude_genres,
                exclude_only=True
            )
    return genres_local

def dancey(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Dancey'
    exclude_genres = [
        genre_to_append,
        'Slow Power',
        'Calm',
        'Dreamy',
        'Cool Off'
    ]
    include_genres = [
        'Indie Dance'
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        exclude_genres=exclude_genres,
        include_genres=include_genres
    )
    if t["spotify_additional_info"]["features"]:
        energy = t["spotify_additional_info"]["features"]['energy']
        danceability = t["spotify_additional_info"]["features"]['danceability']
        instrumentalness = t["spotify_additional_info"]["features"]['instrumentalness']
        speechiness = t["spotify_additional_info"]["features"]['speechiness']
        acousticness = t["spotify_additional_info"]["features"]['acousticness']
        valence = t["spotify_additional_info"]["features"]['valence']
        loudness = t["spotify_additional_info"]["features"]['loudness']
        if (
            (float(-7) < float(loudness)) and
            (float(0.5) < float(danceability)) and
            (float(0.5) > float(instrumentalness)) and
            (float(0.6) > float(speechiness)) and
            (float(0.1) > float(acousticness)) and
            (float(0.4) < float(valence)) and
            (float(0.8) < float(energy))
        ):
            genres_local = append_to_genre_list_v2(
                genres_local,
                genres_remote,
                genre_to_append,
                exclude_genres=exclude_genres,
                exclude_only=True
            )
    return genres_local
#
#def banger(t) -> list:
# genres_remote = t["genres"]["remote"]
# genres_local = t["genres"]["local"]
#    genre_to_append = 'Feature1'
#    exclude_genres = [
#        genre_to_append,
#        'Banger',
#        'Slow Power',
#        'Calm',
#        'Dreamy',
#        'Cool Off'
#    ]
#    if t["spotify_additional_info"]["features"]:
#        energy = t["spotify_additional_info"]["features"]['energy']
#        liveness = t["spotify_additional_info"]["features"]['liveness']
#        instrumentalness = t["spotify_additional_info"]["features"]['instrumentalness']
#        acousticness = t["spotify_additional_info"]["features"]['acousticness']
#        tempo = t["spotify_additional_info"]["features"]['tempo']
#        danceability = t["spotify_additional_info"]["features"]['danceability']
#        loudness = t["spotify_additional_info"]["features"]['loudness']
#        if (
#            (float(-5) < float(loudness)) and
#            (float(0.2) > float(liveness)) and
#            (float(0.1) > float(acousticness)) and
#            (float(0.1) > float(instrumentalness)) and
#            (float(0.8) < float(danceability)) and
#            (float(130) < float(tempo)) and
#            (float(0.8) < float(energy))
#        ):
#            genres_local = append_to_genre_list_v2(
# ["_"]["cal"],
# ["_"]["mote"],
#                genre_to_append,
#                exclude_genres=exclude_genres,
#                exclude_only=True
#            )
#    return genres_local
#



def classify(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    if (('classify' in  genres_remote) and ('Contemporary Classical Music' not in  genres_local)):
        genres_local.append("Contemporary Classical Music")
    return genres_local

def assi(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Assi'
    include_genres = [
        'Assi Flow'
    ]
    exclude_genres = [
        genre_to_append
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        include_genres=include_genres,
        exclude_genres=exclude_genres
    )
    return genres_local

def balkan(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    balkan_genres = [
        'balkan brass',
        'gypsy fusion',
        'gypsy jazz',
        'gypsy',
        'gypsy punk'
    ]
    if (( not set(balkan_genres).isdisjoint(genres_remote)) and ('Balkan' not in  genres_local)):
        genres_local.append("Balkan")
    return genres_local

def epic(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    if (('epicore' in  genres_remote) and ('Epic' not in  genres_local)):
        genres_local.append("Epic")
    if (('scorecore' in  genres_remote) and ('Epic' not in  genres_local)):
        genres_local.append("Epic")
    return genres_local

def elektropunk(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Elektropunk'
    include_genres = [
        'elektropunk'
    ]
    exclude_genres = [
        genre_to_append
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        include_genres=include_genres,
        exclude_genres=exclude_genres
    )

    include_playlists_with_track = [
        'Particle Detector - Intro to Elektropunk',
        'Particle Detector - The Edge of Elektropunk',
        'Particle Detector - The Pulse of Elektropunk',
        'The Sounds of Spotify - The Sound of Elektropunk',
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        playlists_only=True,
        playlists_with_track=t["playlists"]["with_track"],
        include_playlists_with_track=include_playlists_with_track,
        exclude_genres=exclude_genres
    )
    return genres_local


def indie_rock(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Indie Rock'
    include_genres = [
        'indie rock',
        'german indie rock',
        'swedish indie rock'
    ]
    exclude_genres = [
        genre_to_append
    ]
    exclude_artists = [
        'The Beatles',
        'Yann Tiersen'
    ]
    include_artists = [
        'Zwo Eins Risiko'
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        artist=t["artist"],
        include_genres=include_genres,
        include_artists=include_artists,
        exclude_genres=exclude_genres,
        exclude_artists=exclude_artists
    )

    include_playlists_with_track = [
        'Particle Detector - Intro to Indie Rock',
        'Particle Detector - The Edge of Indie Rock',
        'Particle Detector - The Pulse of Indie Rock',
        'Spotify - Indie Rock Club',
        'The Sounds of Spotify - The Sound of Indie Rock',
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        playlists_only=True,
        playlists_with_track=t["playlists"]["with_track"],
        include_playlists_with_track=include_playlists_with_track,
        exclude_genres=exclude_genres
    )

    if (
        ((not set(exclude_artists).isdisjoint([t["artist"]]))  and (not set([genre_to_append]).isdisjoint(genres_local)))
    ):
        genres_local.remove(genre_to_append)
    return genres_local



def pop(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Pop'
    exclude_artists = [
        'The Beatles'
    ]
    include_genres = [
        'french indie pop',
        'shimmer pop',
        'indie pop',
        'dance pop',
        'kazakh pop',
        'norwegian pop',
        'candy pop',
        'austrian pop',
        'pop',
        'arab pop',
        'electropop',
        'punjabi pop',
        'J-Pop',
        'K-Pop',
        'israeli pop'
    ]
    exclude_genres = [
        'Pop',
        'Adult Standards'
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        include_genres=include_genres,
        exclude_genres=exclude_genres
    )
    if (
        ((not set(exclude_artists).isdisjoint([t["artist"]]))  and (not set([genre_to_append]).isdisjoint(genres_local)))
    ):
        genres_local.remove(genre_to_append)
    return genres_local

def german_indie(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'German Indie'
    include_genres = [
        'german indie',
        'swiss indie',
        'dusseldorf indie',
        'austrian indie',
        'vienna indie',
        'german indie rock',
        'deep german indie',
        'cologne indie',
        'salzburg indie'
    ]
    exclude_genres = [
        genre_to_append,
        'Punk',
        'german punk',
        'Electronic',
        'Hip Rap',
        'german punk rock',
        'swedish indie rock'
    ]
    exclude_artists = [
        '77 Bombay Street',
        'Avec',
        'BOY',
        'FIL BO RIVA',
        'Egopusher',
        'Leoniden',
        'Ogris Debris',
        'Naked Cameo',
        'Olympique',
        'Mavi Phoenix',
        'We Walk Walls'
    ]
    include_genres_A = [
        'Deutsch'
    ]
    include_genres_B = [
        'Indie Rock',
        'Indie Pop'
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        artist=t["artist"],
        exclude_artists=exclude_artists,
        include_genres=include_genres,
        include_genres_A=include_genres_A,
        include_genres_B=include_genres_B,
        exclude_genres=exclude_genres
    )



    include_playlists_with_artist = [
        'Spotify - Deutscher Indie',
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        playlists_only=True,
        playlists_with_artist=t["playlists"]["with_artist"],
        include_playlists_with_artist=include_playlists_with_artist,
        exclude_genres=exclude_genres
    )    
    return genres_local

def post_rock(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Post-Rock'
    include_genres = [
        'post-rock',
        'american post-rock',
        'german post-rock',
        'instrumental post-rock',
        'japanese post-rock'
    ]
    exclude_genres = [
        genre_to_append
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        include_genres=include_genres,
        exclude_genres=exclude_genres
    )
    return genres_local

def radio1(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Radio1'
    include_genres = [
        'Küche',
        'Mellow Gold',
        'beatlesque',
        'Adult Standards'
    ]
    exclude_genres = [
        'Dreamy',
        'Radio1',
        'Radio2',
        'Radio3',
        'Radio4',
        'Radio5',
        'Radio6',
        'Radio7',
        'Radio8',
        'Radio9',
        'Radio10'
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        include_genres=include_genres,
        exclude_genres=exclude_genres
    )
    return genres_local

def radio2(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Radio2'
    include_genres = [
        'Indie Pop',
        'Neo Synthpop',
        'Indie Rock',
        'Art Pop',
        'German Indie'
    ]
    exclude_genres = [
        'Dreamy',
        'Microhouse',
        'Electropop',
        'Metal',
        'Punk',
        'Pop Punk',
        'Escape Room',
        'Deconstructed Club',
        'viral rap',
        'witch house',
        'Synth Punk',
        'Küche',
        'House',
        'Filter Lounge',
        'EDM',
        'Mellow Gold',
        'Adult Standards',
        'Escape Room',
        'Schwarze Nacht',
        'Neue Deutsche Härte',
        'Radio1',
        'Radio2',
        'Radio3',
        'Radio4',
        'Radio6',
        'Radio7',
        'Radio8',
        'Radio9',
        'Radio10'
    ]
    exclude_artists = [
        'The Beatles',
        'Yann Tiersen'
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        include_genres=include_genres,
        exclude_artists=exclude_artists,
        exclude_genres=exclude_genres
    )
    if (
        ((not set(exclude_artists).isdisjoint([t["artist"]]))  and (not set([genre_to_append]).isdisjoint(genres_local)))
    ):
        genres_local.remove(genre_to_append)
    return genres_local

def radio3(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    radio3_genres = [
        'Orgánica',
        'Unterwasser',
        'Microhouse',
        'Minimal Techno',
        'Tropical House',
        'Filter House',
        'Ethnotronica',
        'Powerful Ambient Electronic'
    ]
    radio3_exclude_genres = [
        'Dreamy',
        'Metal',
        'Punk',
        'Filter Lounge',
        'Pop Punk',
        'Synth Punk',
        'Küche',
        'Mellow Gold',
        'Adult Standards',
        'Escape Room',
        'Schwarze Nacht',
        'Tunnel',
        'Radio1',
        'Radio2',
        'Radio3',
        'Radio4',
        'Radio5',
        'Radio6',
        'Radio7',
        'Radio8',
        'Radio9',
        'Radio10'
    ]
    if (
        ((not set(radio3_genres).isdisjoint(genres_remote))  or (not set(radio3_genres).isdisjoint(genres_local)))
        and not (
            (not set(radio3_exclude_genres).isdisjoint(genres_local)) or (not set(radio3_exclude_genres).isdisjoint(genres_remote))
            )
    ):
        genres_local.append("Radio3")
    return genres_local

def radio4(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    radio4_genres = [
        'Schwarze Nacht'
    ]
    radio4_exclude_genres = [
        'Dreamy',
        'Metal',
        'Power Noise',
        'power noise',
        'Noise',
        'hardcore techno',
        'noise',
        'Punk',
        'North',
        'electro-industrial',
        'aggrotech',
        'dark electro',
        'Pop Punk',
        'Synth Punk',
        'Küche',
        'Mellow Gold',
        'Adult Standards',
        'Escape Room',
        'Tunnel',
        'Radio1',
        'Radio2',
        'Radio3',
        'Radio4',
        'Radio5',
        'Radio6',
        'Radio7',
        'Radio8',
        'Radio9',
        'Radio10'
    ]
    if (
        ((not set(radio4_genres).isdisjoint(genres_remote))  or (not set(radio4_genres).isdisjoint(genres_local)))
        and not (
            (not set(radio4_exclude_genres).isdisjoint(genres_local)) or (not set(radio4_exclude_genres).isdisjoint(genres_remote))
            )
    ):
        genres_local.append("Radio4")
    return genres_local

def radio5(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Radio5'
    include_genres = [
        'Acoustic Cover',
        'Acoustic',
        'Acoustic Pop',
        'Reggae Rock',
        'Folk',
        'Earthy',
        'Folk Pop',
        'acoustic punk'
    ]
    exclude_genres = [
        'Dreamy',
        'Metal',
        'Power Noise',
        'power noise',
        'Noise',
        'hardcore techno',
        'noise',
        'Punk',
        'North',
        'electro-industrial',
        'aggrotech',
        'dark electro',
        'Pop Punk',
        'Synth Punk',
        'Neue Deutsche Härte',
        'Küche',
        'Balkan',
        'Mellow Gold',
        'Adult Standards',
        'Escape Room',
        'Indie Dance',
        'Slow',
        'Tunnel',
        'Radio1',
        'Radio3',
        'Radio4',
        'Radio5',
        'Radio6',
        'Radio7',
        'Radio8',
        'Radio9',
        'Radio10'
    ]
    exclude_artists = [
        'The Beatles',
        'Alligatoah'
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        include_genres=include_genres,
        exclude_artists=exclude_artists,
        exclude_genres=exclude_genres
    )
    if (
        ((not set(exclude_artists).isdisjoint([t["artist"]]))  and (not set([genre_to_append]).isdisjoint(genres_local)))
    ):
        genres_local.remove(genre_to_append)
    return genres_local


def radio6(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    radio6_genres = [
        'Bum Hop',
        # 'Dark Trap',
        'Escape Room'
    ]
    radio6_exclude_genres = [
        'Dreamy',
        'Metal',
        'Power Noise',
        'power noise',
        'Noise',
        'hardcore techno',
        'noise',
        'electro-industrial',
        'aggrotech',
        'dark electro',
        'Pop Punk',
        'Synth Punk',
        'Küche',
        'Mellow Gold',
        'Adult Standards',
        'Tunnel',
        'Radio1',
        'Radio2',
        'Radio3',
        'Radio4',
        'Radio5',
        'Radio6',
        'Radio7',
        'Radio8',
        'Radio9',
        'Radio10'
    ]
    if (
        ((not set(radio6_genres).isdisjoint(genres_remote))  or (not set(radio6_genres).isdisjoint(genres_local)))
        and not (
            (not set(radio6_exclude_genres).isdisjoint(genres_local)) or (not set(radio6_exclude_genres).isdisjoint(genres_remote))
            )
    ):
        genres_local.append("Radio6")
    return genres_local

def radio7(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Radio7'
    if (genre_to_append in  genres_local):
        genres_local.remove(genre_to_append)
    include_genres = [
        'Bass Trip',
        'Bass Trap',
        'future bass',
    ]
    exclude_genres = [
        genre_to_append
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        include_genres=include_genres,
        exclude_genres=exclude_genres
    )
    return genres_local

def radio8(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Radio8'
    if (genre_to_append in  genres_local):
        genres_local.remove(genre_to_append)
    include_genres = [
        'Black Metal',
        'Comic Metal',
        'Death Metal',
        'Depressive Black Metal',
        'Doom Metal',
        'Folk Metal',
        'Gothic Symphonic Metal',
        'Grindcore',
        'Instrumental Progressive Metal',
        'J-Metal',
        'Melodic Death Metal',
        'Melodic Metal',
        'Metalcore',
        'Neo Classical Metal',
        'Nordic Folk Metal',
        'Power Metal',
        'Power Thrash',
        'Progressive Metal',
        'Slayer',
        'Speed Metal',
        'Tanzmetall',
        'Viking Metal',
    ]
    exclude_genres = [
        genre_to_append,
        "Futurepop",
        "Punk",
        "Neoclassical Darkwave",
        "Hip Rap",
        'Medieval Focus',
        "Epic",
        "Grunge",
        "Dreamy",
        "EDM",
        "Pop Punk",
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        include_genres=include_genres,
        exclude_genres=exclude_genres
    )
    return genres_local

def radio0(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    if ('Radio0' in  genres_local):
        genres_local.remove('Radio0')
    if ('Radio' in  genres_local):
        genres_local.remove('Radio')
    genre_to_append = 'Radio0'
    include_genres = [
        'Radio1',
        'Radio2',
        'Radio3',
        'Radio4',
        'Radio5',
        'Radio6',
        'Radio7',
        'Radio8',
        'Radio9'
    ]
    exclude_genres = [
        genre_to_append,
        'Dreamy',
        'Level0',
        'Epic',
        'Long',
        'Death Metal',
        'Depressive Black Metal',
        'Speed Metal',#
        'Power Noise',
        'power noise',
        'Noise',
        'Disturbing',
        'noise',
        'deathcore',
        'Deathcore',
        'Aggrotech',
        'Assi',
        'slayer',#
        'Aggressive',
        'Grindcore',
        'hardcore techno',
        'Neue Deutsche Härte',
        'neue deutsche harte',
        #'electro-industrial',
    ]
    exclude_genres_A = [
        'Sad',
        'Ambient',
        'Loud',
        'Instrumental Focus',
        'Downtempo'
    ]
    exclude_genres_B = [
        'Quiet',
    ]
    exclude_artists = [
        'Mick Gordon',
        'Lil Dicky, Brain',
        'HGich.T',
        'Tom Angelripper',
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        artist=t["artist"],
        include_genres=include_genres,
        exclude_genres_A=exclude_genres_A,
        exclude_genres_B=exclude_genres_B,
        exclude_artists=exclude_artists,
        exclude_genres=exclude_genres
    )
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        exclude_only=True,
        artist=t["artist"],
        exclude_artists=exclude_artists,
        exclude_genres_A=exclude_genres_A,
        exclude_genres_B=exclude_genres_B,
        exclude_genres=exclude_genres
    )
    assi_flow_exclude_genres = exclude_genres
    assi_flow_exclude_genres.remove('Assi')
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        include_genres=['Assi Flow'],
        artist=t["artist"],
        exclude_artists=exclude_artists,
        exclude_genres_A=exclude_genres_A,
        exclude_genres_B=exclude_genres_B,
        exclude_genres=assi_flow_exclude_genres
    )
    melodic_death_metal_exclude_genres = exclude_genres
    melodic_death_metal_exclude_genres.remove('Death Metal')
    if  (
        (not set(['Melodic Death Metal']).isdisjoint(genres_remote) and genres_remote != [''])  or
        (not set(['Melodic Death Metal']).isdisjoint(genres_local) and genres_local != ['']) 
    ):
        genres_local = append_to_genre_list_v2(
            genres_local,
            genres_remote,
            genre_to_append,
            include_genres=['Melodic Death Metal'],
            artist=t["artist"],
            exclude_artists=exclude_artists,
            exclude_genres_A=exclude_genres_A,
            exclude_genres_B=exclude_genres_B,
            exclude_genres=melodic_death_metal_exclude_genres
        )
    return genres_local

def radio_nrj(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    if ('Radio NRJ' in  genres_local):
        genres_local.remove('Radio NRJ')
    genre_to_append = 'Radio NRJ'
    include_genres_A = [
        'Amped',
        'Banger',
        'Mashup',
        'Dancey',
        'RetroRun',
        'Forwardish',
        'Forward',
        'Loud'
    ]
    include_genres_B = [
        'Radio0'
    ]
    exclude_genres = [
        genre_to_append,
        'Dreamy',
        'Quiet',
        'Calm'
    ]
    exclude_genres_A = [
        'Balkan'
    ]
    exclude_genres_B = [
        'Folk'
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        # debug=True,
        include_genres_A=include_genres_A,
        include_genres_B=include_genres_B,
        exclude_genres=exclude_genres
    )
    return genres_local

def radio_background(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    if ('Radio Background' in  genres_local):
        genres_local.remove('Radio Background')
    genre_to_append = 'Radio Background'
    include_genres = [
        'Radio Chill'
    ]
    exclude_genres = [
        genre_to_append,
        'Deutsch',
        'Distracting'
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        include_genres=include_genres,
        exclude_genres=exclude_genres
    )
    return genres_local

def radio_chill(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    if ('Radio Chill' in  genres_local):
        genres_local.remove('Radio Chill')
    genre_to_append = 'Radio Chill'
    include_genres = [
        'Radio0'
    ]
    exclude_genres = [
        genre_to_append,
        'Radio NRJ',
        'Amped',
        'Fun',
        'Unhinged',
        'Antideutsche',
        'Banger',
        'Mashup',
        'Assi Flow',
        'Dancey',
        'Depri Disko',
        'Power Metal',
        'RetroRun',
        'Forwardish',
        'Forward',
        'Loud'  
    ]
    include_moods = [
        'musicnn-chillout',
        'musicnn-easy listening',
        'musicnn-soft'
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        mood_list=t["mood"]["all"],
        include_moods=include_moods,
        exclude_genres=exclude_genres,
        include_genres=include_genres
    )
    exclude_genres.append('NoMood')
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        include_genres=include_genres,
        exclude_genres=exclude_genres
    )
    return genres_local

# def party(t) -> list:
#     genres_remote = t["genres"]["remote"]
#     genres_local = t["genres"]["local"]
#     genre_to_append = 'Feature1'
#     include_genres = [
#         'Radio Chill'
#     ]
#     exclude_genres = [
#         genre_to_append,
#     ]
#     if (
#         ('party' in t["mood"]["all"])
#     ):
#         genres_local = append_to_genre_list_v2(
#             genres_local,
#             genres_remote,
#             genre_to_append,
#             exclude_genres=exclude_genres,
#             include_genres=include_genres
#         )
#     return genres_local

def organica(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Orgánica'
    include_genres = [
        'arab electronic',
        'turkish electronic',
        'persian electronic',
        'Ethnotronica',
        'organic electronic',
        'afro house',
        'organic house'
    ]
    include_genres_A = [
        'Arabic',
        'Turkish',
        'Persian',
    ]
    include_genres_B = [
        'Electronic',
        'House',
    ]
    exclude_genres = [
        genre_to_append
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        include_genres=include_genres,
        exclude_genres=exclude_genres
    )
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        include_genres_A=include_genres_A,
        include_genres_B=include_genres_B,
        exclude_genres=exclude_genres
    )
    return genres_local

def sad(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Sad'
    include_playlists_with_track = [
        'Spotify - Life Sucks',
        'Spotify - Rainy Day Jazz',
        'Spotify - Sad Beats',
        'Spotify - Sad Classical',
        'Spotify - Tender',
        'Spotify - Broken Heart',
        'Spotify - Melancholy Instrumentals',
        'Spotify - text me back',
        'Spotify - Alone Again',
        'Spotify - Sad Indie',
        'Spotify - Sad Songs',
        'Spotify - Sad 10s',
        'Spotify - Sad 00s',
        'Spotify - Sad 90s',
        'Spotify - Sad 80s',
        'Spotify - Sad 70s',
        'Spotify - Sad Covers',
        'Spotify - Heartache',
        'Spotify - Tout doux',
        'Spotify - Heartbreaker',
        'Spotify - sad song club',
        'Spotify - Classics For Crying',
        'Spotify - Sad Guitar',
        'Spotify - Heartbreak Jazz',
        'Spotify - Rainy Day Blues',
        'Spotify - Just Me and the Blues',
        'Spotify - Devastating',
        'Spotify - Sad Bops',
    ]
    exclude_genres = [
        genre_to_append
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        playlists_only=True,
        playlists_with_track=t["playlists"]["with_track"],
        include_playlists_with_track=include_playlists_with_track,
        exclude_genres=exclude_genres
    )
    return genres_local

def happy(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'SpotifyHappy'
    if (genre_to_append in  genres_local):
        genres_local.remove(genre_to_append)
    include_playlists_with_track = [
        'Spotify - Happy Beats',
        'Spotify - Happy Dance',
        'Spotify - Happy Folk',
        # 'Spotify - Feel Good Dinner',
        # 'Spotify - Feelgood Indie',
        'Spotify - Wake Up Happy',
        # 'Spotify - Good Feeling 00s',
        # 'Spotify - Good Feeling 70s',
        'Spotify - Good Vibes',
        # 'theuniplanet - Happy Songs',
        "Spotify - Get Happy!",
        "Spotify - Get Home Happy!",
        "Spotify - Happy Days",
        # "Spotify - Happy Drive",
        "Spotify - Happy Hits!",
        "Spotify - Just Smile",
        "Spotify - Mood Booster",
        # "Spotify - Have a Great Day!",
        "Spotify - Feelin' Good",
        "Spotify - Happy Throwbacks",
        "Spotify - Happy Tunes",
        # "The Sound Of - Happy Songs + Music | Gute Laune Musik | Good Vibes",
    ]
    exclude_genres = [
        genre_to_append,
        'Depri Disko',
        'Lamenting',
        'Sad',
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        playlists_only=True,
        playlists_with_track=t["playlists"]["with_track"],
        include_playlists_with_track=include_playlists_with_track,
        exclude_genres=exclude_genres
    )
    return genres_local


def fun(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Fun'
    include_playlists_with_track = [
        'Spotify - Have A Laugh',
    ]
    exclude_genres = [
        genre_to_append,
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        playlists_only=True,
        playlists_with_track=t["playlists"]["with_track"],
        include_playlists_with_track=include_playlists_with_track,
        exclude_genres=exclude_genres
    )
    return genres_local

def depri_disko(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'SpotifyDepriDisko'
    if (genre_to_append in  genres_local):
        genres_local.remove(genre_to_append)
    include_playlists_with_track = [
        'Depri Disko',
        'melinaknobi - Depri Disko ☔️',
        'Julia Krnsch - Depri Disko.',
        'Spotify - crying on the dance floor',
    ]
    exclude_genres = [
        genre_to_append,
        'Depri Disko'
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        playlists_only=True,
        # substring=True,
        playlists_with_track=t["playlists"]["with_track"],
        include_playlists_with_track=include_playlists_with_track,
        exclude_genres=exclude_genres
    )
    return genres_local

def schiebt_anders(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Schiebt Anders'
    if (genre_to_append in  genres_local):
        genres_local.remove(genre_to_append)
    include_playlists_with_track = [
            'Spotify - schiebt anders',
    ]
    exclude_genres = [
        genre_to_append,
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        playlists_only=True,
        # substring=True,
        playlists_with_track=t["playlists"]["with_track"],
        include_playlists_with_track=include_playlists_with_track,
        exclude_genres=exclude_genres
    )
    return genres_local

def bravo_hits(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Bravo Hits'
    include_playlists_with_track = [
        'Bravo Hits',
        'BRAVO Hits'
    ]
    exclude_genres = [
        genre_to_append
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        playlists_only=True,
        substring=True,
        playlists_with_track=t["playlists"]["with_track"],
        include_playlists_with_track=include_playlists_with_track,
        exclude_genres=exclude_genres
    )
    return genres_local

def electro_industrial(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Electro-Industrial'
    include_genres = [
        'electro-industrial'
    ]
    exclude_genres = [
        genre_to_append
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        include_genres=include_genres,
        exclude_genres=exclude_genres
    )
    return genres_local

def brocken_party(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Brocken Party'
    include_genres_A = [
        'Rocken am Brocken'
    ]
    include_genres_B = [
        'Dancey',
        'Indie Dance'
    ]
    exclude_genres = [
        genre_to_append
    ]
    exclude_artists = [
        'SXTN'
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        artist=t["artist"],
        include_genres_A=include_genres_A,
        include_genres_B=include_genres_B,
        exclude_genres=exclude_genres,
        exclude_artists=exclude_artists
    )
    return genres_local

def rocken_am_brocken(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Rocken am Brocken'
    include_artists = [
        "5BUGS",
        "Abby",
        "Acollective",
        "Adam Angst",
        "Adolar",
        "AElement",
        "Alice Merton",
        "Alli Neumann",
        "Andreas Liebert",
        "AnnenMayKantereit",
        "Anti-Flag",
        "Antilopengang",
        "Aron L Flow",
        "Art Brut",
        "B6BBO",
        "Bakkushan",
        "Balthazar",
        "Barns Courtney",
        "Beauty & The Beats",
        "Berlin Syndrome",
        "Blackout Problems",
        "Black Sea Dahu",
        "Blaudzun",
        "Blocksperga",
        "Blond",
        "Blood Red Shoes",
        "Bonaparte",
        "Bondage Fairies",
        "Bosse",
        "Brett",
        "Budzillus",
        "Bukahara",
        "Casimir von Oettingen",
        "Cassia",
        "Clixx",
        "Cocoon Fire",
        "Cosby",
        "Cucumba Poo",
        "Das Pack",
        "Das Paradies",
        "David Dorad",
        "Der Tante Renate",
        "Der Täubling",
        "Die Höchste Eisenbahn",
        "Die Nerven",
        "Dirty Doering",
        "DJ Binichnich aka Harris",
        "DJ !MAUF",
        "Dominic Lanfer",
        "Donots",
        "Drangsal",
        "Dúné",
        "Dÿse",
        "Eau Rouge",
        "Elliver",
        "Emily's Giant",
        "Empire Escape",
        "Eternal Tango",
        "Exclusive",
        "Faber",
        "FABER",
        "Feine Sahne Fischfilet",
        "Fertig",
        "Findus",
        "Fire in the Attic",
        "Florian Künstler",
        "FM Belfast",
        "Footprint Project",
        "Fotos",
        "Foxon",
        "French Films",
        "Frére",
        "Friska Viljor",
        "Frittenbude",
        "Fuck Art",
        "Fullax",
        "Game Ove",
        "Georgia",
        "Ghost of Tom Joad",
        "Giant Rooks",
        "Gisbert zu Knyphausen",
        "Go Go Berlin",
        "Good Shoes",
        "Graham Candy",
        "Granada",
        "Grey Television",
        "Gurr",
        "Heisskalt",
        "Henja",
        "Henry and the Waiter",
        "Herr Arendt",
        "Herr Lehrmann",
        "Herr von Grau",
        "Hesse",
        "His Statue Falls",
        "Honig",
        "Hope",
        "Hurricane Dean",
        "I'm Not A Band",
        "Impala Ray",
        "Indie.Disco.Gehn.",
        "Intergalactic Lovers",
        "INVSN",
        "Irie Révoltés",
        "Isolation Berlin",
        "Itchy Poopzkid",
        "Jake Issac",
        "Jenix",
        "Jennifer Rostock",
        "Jeremias",
        "Jetlights",
        "Joco",
        "John Dee",
        "Johnossi",
        "Jonah",
        "Jukno",
        "Jupiter Jones",
        "Juse Ju",
        "Kakkmaddafakka",
        "Kapelle Petra",
        "Käptn Peng & Die Tentakel von Delphi",
        "Kat Frankie",
        "Keir",
        "Kid Dad",
        "Kid Simius",
        "Kilians",
        "Kill It Kit",
        "King Kong Kicks",
        "Klan",
        "Kock's Motel",
        "Kollektiv Ost",
        "K-Paul",
        "Kraftklub",
        "Kytes",
        "Lambert",
        "L’Aupaire",
        "Le Fly",
        "Leoniden",
        "Let’s Dance!",
        "Liedfett",
        "Lion Sphere",
        "Los!",
        "Love A",
        "LSD on CIA",
        "LSD On CIA",
        "Luke Noa & The Basement Beats",
        "Madsen",
        "Malleus",
        "Maria Basel",
        "Massendefekt",
        "Mavi Phoenix",
        "Middleman",
        "Mighty Oaks",
        "Milliarden",
        "Minni the Moocher",
        "Modell Bianka",
        "Montreal",
        "Moop Mama",
        "Mother’s Cake",
        "Muff Potter",
        "Mutabor",
        "Neonschwarz",
        "Neufundland",
        "New Desert Blues",
        "Noga Erez",
        "No King.No Crown",
        "No.Mads",
        "Northern Lite",
        "North West",
        "Nosoyo",
        "Not Called Jinx",
        "OK Kid",
        "Oliver Koletzki",
        "Olli Schulz",
        "Oracles",
        "Pabst",
        "Parasite Single",
        "Parcels",
        "Pish",
        "Portugal. The Man",
        "Provinz",
        "Querbeat",
        "Radio Havanna",
        "Rampue",
        "Rantanplan",
        "Raum27",
        "RAUM27",
        "Razz",
        "Rikas",
        "Rockstah",
        "Rogers",
        "Romano Licker",
        "Roosevelt",
        "Royal Republic",
        "Satellite Stories",
        "Schluck den Druck",
        "Schmitz Katze",
        "Schmutzki",
        "Screw FM",
        "Sea Moya",
        "Seth Schwarz",
        "SIND",
        "Sizarr",
        "Ska-T",
        "Sofia Portanet",
        "Son",
        "Sonic Mobilée",
        "Son of Polvo",
        "Sophie Hunger",
        "Spitting Off Tall Buildings",
        "Stompin’ Souls",
        "Super700",
        "Swiss & Die Anderen",
        "SXTN",
        "Tak Tak Tak",
        "Tante Polly",
        "The Adicts",
        "The Animen",
        "The Audience",
        "The Busters",
        "The/Das",
        "The Districts",
        "The Eternal Spirit",
        "The Holy",
        "The Love Bülow",
        "The Majority Says",
        "The Notwist",
        "The Prosecution",
        "The Secnd",
        "The Sexinvaders",
        "The Skatoons",
        "The Smith Street Band",
        "The Subways",
        "Tiger Lou",
        "Tom Gatza",
        "Tom Klose",
        "Tom Klose & Band",
        "Tonbandgerät",
        "Torpus and the Art Directors",
        "Triggerfinger",
        "Trip Fontaine",
        "Tristesse",
        "Turbostaat",
        "Ubershark",
        "Van Holzen",
        "Vierkanttretlager",
        "Vizediktator",
        "VÖK",
        "Von Wegen Lisbeth",
        "Warhaus",
        "We Are Nuts",
        "We Are Scientists",
        "Welshy Arms",
        "We Take The Biscuit",
        "We Were Promised Jetpacks",
        "Who killed Frank?",
        "WhoMadeWho",
        "Wilson Jr.",
        "Xul Zolar",
        "Yetti Meissner",
        "Young Rebel Set",
        "Zugezogen Maskulin",
        "Zulu"
    ]
    exclude_genres = [
        genre_to_append
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        artist=t["artist"],
        artists_only=True,
        include_artists=include_artists,
        exclude_genres=exclude_genres
    )
    return genres_local

def russian_pop(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Russian Pop'
    include_genres = [
        'russian pop'
    ]
    include_genres_A = [
        'Russian'
    ]
    include_genres_B = [
        'Pop'
    ]
    exclude_genres = [
        genre_to_append
    ]
    exclude_artists = [
        "Little Big"
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        artist=t["artist"],
        include_genres=include_genres,
        exclude_genres=exclude_genres,
        exclude_artists=exclude_artists
    )
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        artist=t["artist"],
        include_genres_A=include_genres_A,
        include_genres_B=include_genres_B,
        exclude_genres=exclude_genres,
        exclude_artists=exclude_artists
    )
    return genres_local

def russian(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Russian'
    include_genres = [
        'russian',
        'Russian Pop',
        'Russian Hip Hop',
        'russisch'
    ]
    exclude_genres = [
        genre_to_append,
        'Deutsch'
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        include_genres=include_genres,
        exclude_genres=exclude_genres,
        substring=True
    )
    return genres_local

def french(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'French'
    include_genres = [
        'french',
        'french rock',
        'french hip hop',
    ]
    exclude_genres = [
        genre_to_append,
        'Deutsch'
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        include_genres=include_genres,
        exclude_genres=exclude_genres,
        substring=True
    )
    return genres_local

def italian(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Italian'
    include_genres = [
        'italian',
        'indie rock italiano',
        'Italian Pop',
        'italienisch'
    ]
    exclude_genres = [
        genre_to_append,
        'Deutsch'
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        include_genres=include_genres,
        exclude_genres=exclude_genres,
        substring=True
    )
    return genres_local

def italian_pop(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Italian Pop'
    include_genres = [
        'italian pop',
        'italian adult pop',
    ]
    exclude_genres = [
        genre_to_append,
        'Deutsch'
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        include_genres=include_genres,
        exclude_genres=exclude_genres,
        substring=True
    )
    return genres_local

def spanish(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Spanish'
    include_genres = [
        'spanish',
        'spanisch'
    ]
    exclude_genres = [
        genre_to_append,
        'Deutsch'
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        include_genres=include_genres,
        exclude_genres=exclude_genres,
        substring=True
    )
    return genres_local

def reggaeton(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Reggaeton'
    include_genres = [
        'reggaeton'
    ]
    exclude_genres = [
        genre_to_append,
        'Deutsch'
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        include_genres=include_genres,
        exclude_genres=exclude_genres,
        substring=True
    )
    return genres_local

def classic_rock(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Classic Rock'
    include_genres = [
        'classic rock'
    ]
    exclude_genres = [
        genre_to_append
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        include_genres=include_genres,
        exclude_genres=exclude_genres
    )
    return genres_local

def tropical_house(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Tropical House'
    include_genres = [
        'tropical house'
    ]
    exclude_genres = [
        genre_to_append
    ]
    exclude_artists = [
        'Parov Stelar',
        'Bastille',
        'Tove Lo',
        'Imagine Dragons'
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        artist=t["artist"],
        include_genres=include_genres,
        exclude_artists=exclude_artists,
        exclude_genres=exclude_genres
    )
    return genres_local

def deep_tropical_house(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Deep Tropical House'
    include_genres = [
        'deep tropical house'
    ]
    exclude_genres = [
        genre_to_append
    ]
    exclude_artists = [
        'Parov Stelar',
        'Bastille',
        'Imagine Dragons'
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        artist=t["artist"],
        include_genres=include_genres,
        exclude_artists=exclude_artists,
        exclude_genres=exclude_genres
    )
    return genres_local

def deep_groove_house(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Deep Groove House'
    include_genres = [
        'deep groove house'
    ]
    exclude_genres = [
        genre_to_append
    ]
    exclude_artists = [
        'Parov Stelar',
        'Bastille',
        'Imagine Dragons'
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        artist=t["artist"],
        include_genres=include_genres,
        exclude_artists=exclude_artists,
        exclude_genres=exclude_genres
    )
    return genres_local


def psytrance(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Psytrance'
    include_genres = [
        'psychedelic trance',
        'psytrance',
        'dark psytrance',
        'progressive psytrance',
        'full-on',
        'suomisaundi',
        'zenonesque'
    ]
    exclude_genres = [
        genre_to_append
    ]
    exclude_artists = [
        'Nasser'
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        artist=t["artist"],
        include_genres=include_genres,
        exclude_genres=exclude_genres,
        exclude_artists=exclude_artists
    )
    if (
        ((not set(exclude_artists).isdisjoint([t["artist"]]))  and (not set([genre_to_append]).isdisjoint(genres_local)))
    ):
        genres_local.remove(genre_to_append)
    return genres_local


def death_metal(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Death Metal'
    include_genres = [
        'death metal',
        'melodic death metal',
        'progressive death metal',
        'swedish death metal'
    ]
    exclude_genres = [
        genre_to_append
    ]
    exclude_artists = [
        'Nickelback'
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        artist=t["artist"],
        include_genres=include_genres,
        exclude_genres=exclude_genres,
        exclude_artists=exclude_artists
    )
    return genres_local

def indie_pop(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Indie Pop'
    include_genres = [
        'indie pop',
        'swedish indie pop',
        'french indie pop'
    ]
    exclude_genres = [
        genre_to_append,
        'Adult Standards'
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        include_genres=include_genres,
        exclude_genres=exclude_genres
    )
    return genres_local

def funk(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Funk'
    include_genres = [
        'funk',
        'deep funk',
        'modern funk',
        'instrumental funk',
        'traditional funk',
        'cuban funk',
        'jazz funk',
        'neapolitan funk',
        'modern funk',
        'funky breaks',
        'brit funk',
        'funk mexicano',
        'new orleans funk',
        'latin funk',
        'synth funk',
        'afro-funk',
    ]
    exclude_genres = [
        genre_to_append,
        'Adult Standards',
        'adult standards',
        'Punk',
        'ska jazz',
        'Electropop',
        'Chillóp',
        'garage rock',
        'underground hip hop',
        'lo-fi brasileiro',
        'post-teen pop',
        'jazztronica',
        'rockabilly',
        'Indie Pop',
        'Balkan',
        'Metal',
        'filter house',
        'Orgánica',
        'Turntablism',
        'jazz blues',
        'House',
        'hard house',
        'Filter House',
        'atl hip hop',
        'dubstep',
        'future garage',
        'new rave',
        'dance-punk',
        'dirty south',
        'Indie Rock',
        'Nu Metal',
        'steampunk',
        'rapcore',
        'Dubstep',
        'Reggae',
        'EDM',
        'Tropical House',
        'german hip hop',
        'volkspop',
        'synthpop',
        'chanson',
        'eurodance',
        'Eurodance',
        'dirty south rap',
        'vapor soul',
        'funk metal',
        'australian indie',
        'etherpop',
        'australian alternative rock',
        'electro swing'
    ]
    include_playlists_with_track = [
        "Spotify - Nu-Funk",
        "Spotify - All Funked Up",
        "Spotify - Funk Outta Here",
        "Spotify - Funk Mix"
        "Spotify - Funky Heavy Bluesy",
        "Spotify - I Love My '70s Funk",
        "Spotify - Instrumental Funk",
        "The Sounds of Spotify - The Sound of Traditional Funk",
        "The Sounds of Spotify - The Sound of Deep Funk",
        "The Sounds of Spotify - The Sound of Cuban Funk",
        "The Sounds of Spotify - The Sound of Funk",
        "The Sounds of Spotify - The Sound of Instrumental Funk",
        "The Sounds of Spotify - The Sound of Jazz Funk",
        "The Sounds of Spotify - The Sound of Neapolitan Funk",
        "The Sounds of Spotify - The Sound of Modern Funk",
        "The Sounds of Spotify - The Sound of Funky Breaks",
        "The Sounds of Spotify - The Sound of Brit Funk",
        "The Sounds of Spotify - The Sound of Funk Mexicano",
        "The Sounds of Spotify - The Sound of New Orleans Funk",
        "The Sounds of Spotify - The Sound of Latin Funk",
        "The Sounds of Spotify - The Sound of Synth Funk",
        "The Sounds of Spotify - The Sound of Afro-Funk",
        # "The Sounds of Spotify - The Sound of Drumfunk",
        # "The Sounds of Spotify - The Sound of Neurofunk",
        # "The Sounds of Spotify - The Sound of Liquid Funk",
        # "The Sounds of Spotify - The Sound of Deep Funk House",
        # "The Sounds of Spotify - The Sound of G Funk",
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        playlists_only=True,
        playlists_with_track=t["playlists"]["with_track"],
        include_playlists_with_track=include_playlists_with_track,
        exclude_genres=exclude_genres
    )
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        artist=t["artist"],
        include_genres=include_genres,
        exclude_genres=exclude_genres
    )
    return genres_local

def r_n_b(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'R&B'
    include_genres = [
        'r&b'
        'contemporary r&b'
        'south african r&b'
        'alternative r&b'
        'indie r&b'
        'afro r&b'
        'uk contemporary r&b'
        'chill r&b'
        'deep smooth r&b'
    ]
    exclude_genres = [
        genre_to_append,
    ]
    include_playlists_with_track = [
        "Spotify - Fresh Finds R&B",
        "Spotify - R&B Classics",
        "Spotify - Women of R&B",
        "Spotify - R&B Right Now",
        "Spotify - Groovin' R&B",
        "Spotify - I Love My '10s R&B",
        "Spotify - I Love My '00s R&B",
        "Spotify - I Love My '90s R&B",
        "Spotify - Chilled R&B"
        "The Sounds of Spotify - The Sound of Chill R&B",
        "The Sounds of Spotify - The Sound of R&B",
        "The Sounds of Spotify - The Sound of Contemporary R&B",
        "The Sounds of Spotify - The Sound of South African R&B",
        "The Sounds of Spotify - The Sound of Alternative R&B",
        "The Sounds of Spotify - The Sound of Indie R&B",
        "The Sounds of Spotify - The Sound of Afro R&B",
        "The Sounds of Spotify - The Sound of UK Contemporary R&B",
        "The Sounds of Spotify - The Sound of Deep Smooth R&B",
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        playlists_only=True,
        playlists_with_track=t["playlists"]["with_track"],
        include_playlists_with_track=include_playlists_with_track,
        exclude_genres=exclude_genres
    )
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        artist=t["artist"],
        include_genres=include_genres,
        exclude_genres=exclude_genres
    )
    return genres_local

def soul(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Soul'
    include_genres = [
        "soul"
        "neo soul"
        "neo soul-Jazz"
        "memphis soul"
        "chicago soul"
        "souldies"
        "classic soul"
        "retro soul"
        "indie soul"
        "soul blues"
        "soul flow"
        "afro soul"
        "british soul"
        "instrumental soul"
        "smooth soul"
        "traditional soul"
    ]
    exclude_genres = [
        genre_to_append,
    ]
    include_playlists_with_track = [
        "Spotify - Soulful Blend",
        "Spotify - Soul Revived",
        "Spotify - Timeless Soul Ballads",
        "Spotify - Soul Food",
        "Spotify - 70s Soul Classics",
        "Spotify - Next Wave Neo-Soul",
        "Spotify - I Love My Neo-Soul",
        "Spotify - Soulful Day",
        "Spotify - Sweet Soul Chillout",
        "Spotify - Soul Coffee",
        "Spotify - Acoustic Soul",
        "Spotify - Soul Soothers",
        "Spotify - Soul Lounge"
        "The Sounds of Spotify - The Sound of Soul",
        "The Sounds of Spotify - The Sound of Neo Soul",
        "The Sounds of Spotify - The Sound of Neo Soul-Jazz",
        "The Sounds of Spotify - The Sound of Memphis Soul",
        "The Sounds of Spotify - The Sound of Chicago Soul",
        "The Sounds of Spotify - The Sound of Souldies",
        "The Sounds of Spotify - The Sound of Classic Soul",
        "The Sounds of Spotify - The Sound of Retro Soul",
        "The Sounds of Spotify - The Sound of Indie Soul",
        "The Sounds of Spotify - The Sound of Soul Blues",
        "The Sounds of Spotify - The Sound of Soul Flow",
        "The Sounds of Spotify - The Sound of Afro Soul",
        "The Sounds of Spotify - The Sound of British Soul",
        "The Sounds of Spotify - The Sound of Instrumental Soul",
        "The Sounds of Spotify - The Sound of Smooth Soul",
        "The Sounds of Spotify - The Sound of Traditional Soul",
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        playlists_only=True,
        playlists_with_track=t["playlists"]["with_track"],
        include_playlists_with_track=include_playlists_with_track,
        exclude_genres=exclude_genres
    )
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        artist=t["artist"],
        include_genres=include_genres,
        exclude_genres=exclude_genres
    )
    return genres_local

def afro(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Afro'
    include_genres = [
        'afropop',
        'african',
        'south african pop',
        'afro-beat',
        'africa',
        'nigerian',
        'Afro Indie',
        'afrobeat'
    ]
    exclude_genres = [
        genre_to_append
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        include_genres=include_genres,
        exclude_genres=exclude_genres
    )
    return genres_local

def math_rock(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Math Rock'
    include_genres = [
        'japanese Math Rock',
        'math rock',
        'math-rock'
    ]
    exclude_genres = [
        genre_to_append
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        include_genres=include_genres,
        exclude_genres=exclude_genres
    )
    return genres_local

def release_year(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    year="Year-"+str(t["album_release_year"])
    if year not in genres_local:
        t["genres"]["local"].append(year)
    return genres_local

def release_decade(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    decade="Decade-"+str(math.floor((int(t["album_release_year"]))/10)*10)
    if decade not in genres_local:
        t["genres"]["local"].append(decade)
    return genres_local

def assi_flow(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Assi Flow'
    exclude_genres = [
        genre_to_append
    ]
    include_artists = [
        'Bonez MC',
        'Bonez MC, RAF Camora',
        'Bonez MC, RAF Camora, Maxwell',
        'RAF Camora',
        'Capo',
        'Apache 207'
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        include_artists=include_artists,
        exclude_genres=exclude_genres,
        artist=t["artist"]
    )
    return genres_local

def eight_bit(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = '8-Bit'
    include_genres = [
       'nintendocore',
        'chiptune'
    ]
    exclude_genres = [
        genre_to_append,
        'dance-punk',
       'indie rock',
       'RetroNight',
       'Rock'
    ]
    exclude_artists = [
        'Crystal Castles'
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        artist=t["artist"],
        include_genres=include_genres,
        exclude_artists=exclude_artists,
        exclude_genres=exclude_genres
    )
    return genres_local

def folk(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Folk'
    include_genres =  [
        'folk rock',
        'folk-pop',
        'russian folk',
        'modern folk rock',
        'indie folk'
    ]
    exclude_genres = [
        genre_to_append,
        'anti-folk'
    ]
    exclude_artists = [
        'Mando Diao',
        'The Beatles'
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        artist=t["artist"],
        include_genres=include_genres,
        exclude_artists=exclude_artists,
        exclude_genres=exclude_genres
    )
    if (
        ((not set(exclude_artists).isdisjoint([t["artist"]]))  and (not set([genre_to_append]).isdisjoint(genres_local)))
    ):
        genres_local.remove(genre_to_append)
    return genres_local

def cover(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Cover'
    include_genres = [
        'Acoustic Cover',
        'Coverchill',
        'Metal Cover',
        'Bluegrass Cover',
        'Electronic Cover',
        'Reggae Cover',
        'Instrumental Cover'
    ]
    exclude_genres = [
        genre_to_append
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        include_genres=include_genres,
        exclude_genres=exclude_genres
    )

    include_playlists_with_track = [
        'Songpickr - Indie Cover Songs',
        'Spotify - Indie Covers',
        'Spotify - Rock Covers',
        'Spotify - Sad Covers',
        'Spotify - Folksy Covers',
        'Spotify - Classic Covers',
        'Spotify - Bluegrass Covers',
        'Spotify - Chill Covers',
        'Spotify - Ultimate Covers',
        'Spotify - Pop Covers',
        'Spotify - Covers Unplugged',
        'Spotify - Cozy Coffeehouse Covers',
        'The Sounds of Spotify - The Sound of Coverchill',
        'midnightstudios - Disney Covers',
        'The Sounds of Spotify - The Sound of Bossa Nova Cover',
        'The Sounds of Spotify - The Sound of Lo-Fi Cover',
        'The Sounds of Spotify - The Sound of Piano Cover',
        'The Sounds of Spotify - The Sound of Reggae Cover',
        'The Sounds of Spotify - The Sound of Acoustic Cover',
        'The Sounds of Spotify - The Sound of Jazz Cover'
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        playlists_only=True,
        playlists_with_track=t["playlists"]["with_track"],
        include_playlists_with_track=include_playlists_with_track,
        exclude_genres=exclude_genres
    )
    return genres_local

def daft_punk_samples(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Daft Punk Samples'
    exclude_genres = [
        genre_to_append
    ]
    exclude_artists = [
        'Daft Punk'
    ]
    include_playlists_with_track = [
        'Daft Punk Samples',
        'DAFT PUNK SAMPLES'
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        artist=t["artist"],
        playlists_only=True,
        substring=True,
        playlists_with_track=t["playlists"]["with_track"],
        include_playlists_with_track=include_playlists_with_track,
        exclude_artists=exclude_artists,
        exclude_genres=exclude_genres
    )
    return genres_local

def deutschrap_klassiker(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Deutschrap Klassiker'
    exclude_genres = [
        genre_to_append
    ]
    include_playlists_with_track = [
        'Spotify - Deutschrap: Die Klassiker'
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        playlists_only=True,
        playlists_with_track=t["playlists"]["with_track"],
        include_playlists_with_track=include_playlists_with_track,
        exclude_genres=exclude_genres
    )
    return genres_local

def pop_klassiker(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Pop Klassiker'
    exclude_genres = [
        genre_to_append
    ]
    include_playlists_with_track = [
        'Spotify - Pop Klassiker'
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        playlists_only=True,
        playlists_with_track=t["playlists"]["with_track"],
        include_playlists_with_track=include_playlists_with_track,
        exclude_genres=exclude_genres
    )
    return genres_local

def indie_klassiker(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Indie Klassiker'
    exclude_genres = [
        genre_to_append
    ]
    include_playlists_with_track = [
        'Spotify - Indie Klassiker',
        'Spotify - Indie Classics',
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        playlists_only=True,
        playlists_with_track=t["playlists"]["with_track"],
        include_playlists_with_track=include_playlists_with_track,
        exclude_genres=exclude_genres
    )
    return genres_local

def cyberpunk_2077(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Cyberpunk 2077'
    exclude_genres = [
        genre_to_append
    ]
    include_playlists_with_track = [
        "F Ʌ Y D - Cyberpunk 2077",
        "Gaming Music - Cyberpunk 2077 Soundtrack (2021)"
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        playlists_only=True,
        playlists_with_track=t["playlists"]["with_track"],
        include_playlists_with_track=include_playlists_with_track,
        exclude_genres=exclude_genres
    )
    return genres_local

def reggae_klassiker(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Reggae Klassiker'
    exclude_genres = [
        genre_to_append
    ]
    include_playlists_with_track = [
        'Spotify - Reggae Classics'
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        playlists_only=True,
        playlists_with_track=t["playlists"]["with_track"],
        include_playlists_with_track=include_playlists_with_track,
        exclude_genres=exclude_genres
    )
    return genres_local

def floorfillers(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Floorfillers'
    exclude_genres = [
        genre_to_append
    ]
    include_playlists_with_track = [
        'Spotify - 100% Floorfillers'
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        playlists_only=True,
        playlists_with_track=t["playlists"]["with_track"],
        include_playlists_with_track=include_playlists_with_track,
        exclude_genres=exclude_genres
    )
    return genres_local

def kaffeehausmusik(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Kaffeehausmusik'
    if (genre_to_append in  genres_local):
        genres_local.remove(genre_to_append)
    exclude_genres = [
        genre_to_append
    ]
    include_playlists_with_track = [
        "The Sound Of - Coffee Break + Chill | Coffeehouse | Kaffeehausmusik | Café and Croissant – It's Always Coffee Time!",
        "Spotify - Kaffeehausmusik",
        "Spotify - Your Favorite Coffeehouse",
        "Spotify - Morning Coffee",
        "Spotify - Coffee Table Jazz",
        "Spotify - Soul Coffee",
        "Spotify - Coffee Club",
]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        playlists_only=True,
        playlists_with_track=t["playlists"]["with_track"],
        include_playlists_with_track=include_playlists_with_track,
        exclude_genres=exclude_genres
    )
    return genres_local

def comfort_zone(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Comfort Zone'
    # if "Spotify - Quality Time" in t["playlists"]["with_track"] and genre_to_append in genres_local:
    #     genres_local.remove(genre_to_append)
    if "Adult Standards" in genres_local and genre_to_append in genres_local:
        genres_local.remove(genre_to_append)
    include_genres = [
       'Warm',
       'Coverchill',
    ]
    include_genres_A = [
       'Vapor Soul',
    ]
    include_genres_B = [
       'Dreamy',
    ]
    exclude_genres = [
        genre_to_append,
        'Adult Standards',
        'Ambience Of The Voiceless',
        'Forward',
        'Forwardish',
        'Loud',
        'Vapor Twitch',
        'Future Base',
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        artist=t["artist"],
        exclude_artists=[
            'Johnny Cash'
        ],
        include_genres_A=include_genres_A,
        include_genres_B=include_genres_B,
        include_genres=include_genres,
        exclude_genres=exclude_genres
    )
    exclude_playlists_with_track = [
        'Spotify - Classic Rock Workout',
        'Spotify - Rocktail Hour',
    ]

    include_playlists_with_track = [
        "Spotify - Even Flow",
        "Spotify - Smooth Morning",
        "Spotify - Comfort Zone",
        "Spotify - Zuhause",
        "Spotify - Kaffeehausmusik",
        "Spotify - Your Favorite Coffeehouse",
        "Spotify -  Relax & Unwind",
        "Spotify -  Boho + Chill",
        "Spotify -  After Party//Come Down",
        "Spotify -  Cozy Coffeehouse Covers",
        "Spotify -  Tantalizers",
        "Spotify -  Low-key Weekend",
        "Spotify -  Stay in Bed",
        "Spotify -  Morning Coffee",
        "Spotify -  Morgenkaffe",
        "Spotify -  Morning Stroll",
        "Spotify - self care",
        # "Spotify - Quality Time",
        # "Spotify -  Soak Up the Sun",
        # "Spotify - Chilled Classics",
        # "Spotify - Mellow Classics",
        #Soul Coffee
        #Spotify - Lazy Sunday
        #Spotify - Cozy Acoustic Morning"
        #Spotify - Easy On Sunday
        #Spotify - New Happy Tunes"
        #potify - Your Coffee Break
        # Spotify - Coffee + Chill
        # Spotify - Positive Vibes",
        # Spotify - Good Times",
]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        playlists_only=True,
        playlists_with_track=t["playlists"]["with_track"],
        include_playlists_with_track=include_playlists_with_track,
        exclude_playlists_with_track=exclude_playlists_with_track,
        exclude_genres=exclude_genres
    )
    return genres_local

def ultimate_indie(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Ultimate Indie'
    exclude_genres = [
        genre_to_append
    ]
    include_playlists_with_track = [
        'Spotify - Ultimate Indie'
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        playlists_only=True,
        playlists_with_track=t["playlists"]["with_track"],
        include_playlists_with_track=include_playlists_with_track,
        exclude_genres=exclude_genres
    )
    return genres_local

def chill_and_dance(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    if ("Chill&Dance" in  genres_local):
        genres_local.remove("Chill&Dance")
    genre_to_append = "Chill'n'Dance"
    exclude_genres = [
        genre_to_append
    ]
    include_playlists_with_track = [
        'Spotify - Chilled Dance Hits'
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        playlists_only=True,
        playlists_with_track=t["playlists"]["with_track"],
        include_playlists_with_track=include_playlists_with_track,
        exclude_genres=exclude_genres
    )
    return genres_local

def sleepy(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Sleepy'
    exclude_genres = [
        genre_to_append
    ]
    include_playlists_with_track = [
        "Spotify - Sleep",
        "Spotify - Gute Nacht Klassik",
        "Spotify - Night Rain",
        "Spotify - Peaceful Piano",
        "Spotify - Deep Sleep",
        "Spotify - Baby Sleep",
        "Spotify - Nightstorms",
        "Spotify - Floating Through Space",
        "Spotify - Sleepy Piano",
        "Spotify - Nachklang",
        "Spotify - Dreamy Vibes",
        "Spotify - Fall Asleep",
        "Spotify - Jazz for Sleep",
        "Spotify - Sleep Tight",
        "Spotify - Hawaiian Dreams",
        "Spotify - DayDreamer",
        "Spotify - Lava Lamp",
        "Spotify - Under the Stars",
        "Spotify - Sleep Calm & Dream On",
        "Spotify - Stargazer",
        "Spotify - A Different Sleeping List",
        "Spotify - Strings for Sleeping"
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        playlists_only=True,
        playlists_with_track=t["playlists"]["with_track"],
        include_playlists_with_track=include_playlists_with_track,
        exclude_genres=exclude_genres
    )
    return genres_local

def sing_along(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'SpotifySingAlong'
    exclude_genres = [
        genre_to_append
    ]
    include_playlists_with_track = [
        "Spotify - Sing-along: 90's to Now",
        "Spotify - Sing-Along Indie Hits"
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        playlists_only=True,
        playlists_with_track=t["playlists"]["with_track"],
        include_playlists_with_track=include_playlists_with_track,
        exclude_genres=exclude_genres
    )
    return genres_local

def acoustic(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Acoustic'
    include_genres = [
        'Acoustic Cover',
        'Acoustic Pop'
    ]
    exclude_genres = [
        genre_to_append,
        'Electronic',
        'Epic',
        'Electropop',
        'Küche',
        'North',
        'Mellow Gold',
        'Poetry',
        'Filter Lounge',
        'Adult Standards',
        'Ska',
        'Psychedelic',
        'A Capella',
        'Contemporary Classical Music'
    ]
    if t["spotify_additional_info"]["features"]:
        acousticness = t["spotify_additional_info"]["features"]['acousticness']
        instrumentalness = t["spotify_additional_info"]["features"]['instrumentalness']
        if (float(acousticness) > 0.85) and (float(instrumentalness) < 0.1) and (genre_to_append not in  genres_local):
            #print(artist,acousticness)
            genres_local = append_to_genre_list_v2(
                genres_local,
                genres_remote,
                genre_to_append,
                exclude_genres=exclude_genres,
                exclude_only=True
            )
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        include_genres=include_genres,
        exclude_genres=exclude_genres
    )
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        include_genres=include_genres
    )


    include_playlists_with_track = [
        'Particle Detector - Intro to Acoustic Blues',
        'Particle Detector - Intro to Acoustic Chill',
        'Particle Detector - Intro to Acoustic Pop',
        'Particle Detector - Intro to Acoustic Punk',
        'Particle Detector - Intro to Instrumental Acoustic Guitar',
        'Particle Detector - The Edge of Acoustic Blues',
        "Particle Detector - The Edge of Acoustic Chill"
        'Particle Detector - The Edge of Acoustic Chill',
        'Particle Detector - The Edge of Acoustic Pop',
        'Particle Detector - The Edge of Acoustic Punk',
        'Particle Detector - The Edge of Instrumental Acoustic Guitar',
        'Particle Detector - The Pulse of Acoustic Blues',
        'Particle Detector - The Pulse of Acoustic Chill',
        'Particle Detector - The Pulse of Acoustic Pop',
        'Particle Detector - The Pulse of Acoustic Punk',
        'Particle Detector - The Pulse of Instrumental Acoustic Guitar',
        'Songpickr - Acoustic Singer Songwriter',
        'Spotify - 70s & 80s Acoustic',
        'Spotify - 90s Acoustic',
        'Spotify - Acoustic Blues',
        'Spotify - Acoustic Cafe International',
        'Spotify - Acoustic Christmas',
        'Spotify - Acoustic Commute',
        'Spotify - Acoustic Concentration',
        'Spotify - Acoustic Covers',
        'Spotify - Acoustic Grit',
        'Spotify - Acoustic Hits',
        'Spotify - Acoustic Hits: Oldies but Goodies',
        'Spotify - Acoustic Love',
        'Spotify - Acoustic Rock',
        'Spotify - Acoustic Summer',
        'Spotify - Afternoon Acoustic',
        'Spotify - Autumn Acoustic',
        'Spotify - Calming Acoustic',
        'Spotify - Classic Acoustic',
        'Spotify - Evening Acoustic',
        'Spotify - Feel Good Acoustic',
        'Spotify - Hot Acoustics',
        'Spotify - Infinite Acoustic',
        'Spotify - K-Pop Acoustic',
        'Spotify - Laidback Acoustic',
        'Spotify - Hit Acoustique',
        'Spotify - mint Acoustic',
        'Spotify - Morning Acoustic',
        'Spotify - Yoga Acoustics',
        'The Sounds of Spotify - The Sound of Acoustic Blues',
        'The Sounds of Spotify - The Sound of Acoustic Chill',
        'The Sounds of Spotify - The Sound of Acoustic Guitar Cover',
        'The Sounds of Spotify - The Sound of Acoustic OPM',
        'The Sounds of Spotify - The Sound of Acoustic Pop',
        'The Sounds of Spotify - The Sound of Acoustic Punk',
        'The Sounds of Spotify - The Sound of Acoustic Rock',
        'The Sounds of Spotify - The Sound of Instrumental Acoustic Guitar',
        'The Sounds of Spotify - The Sound of J-Acoustic',
        'Spotify - Acoustic Throwbacks',
        'Spotify - Acoustic Favorites',
        'Spotify - Acoustic Soul',
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        playlists_only=True,
        playlists_with_track=t["playlists"]["with_track"],
        include_playlists_with_track=include_playlists_with_track,
        exclude_genres=exclude_genres
    )
    return genres_local

def acoustic_chill(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Acoustic'
    include_genres_A = [
        'Acoustic',
    ]
    include_genres_B = [
        'Cool Off',
        'Coverchill',
        'Indie Chill',
        'Calm',
    ]
    exclude_genres = [
        genre_to_append,
        'Electronic',
        'Epic',
        'Electropop',
        'Küche',
        'North',
        'Poetry',
        'Filter Lounge',
        'Adult Standards',
        'Ska',
        'Psychedelic',
        'A Capella',
        'Contemporary Classical Music'
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        exclude_genres,
        include_genres_A=include_genres_A,
        include_genres_B=include_genres_B
    )


    include_playlists_with_track = [
        'Particle Detector - The Edge of Acoustic Chill'
        'Particle Detector - The Edge of Acoustic Chill',
        'The Sounds of Spotify - The Sound of Acoustic Chill',
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        playlists_only=True,
        playlists_with_track=t["playlists"]["with_track"],
        include_playlists_with_track=include_playlists_with_track,
        exclude_genres=exclude_genres
    )
    return genres_local

def mariachi_death_surf(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Mariachi Death Surf'
    include_genres_A = [
        'surf',
    ]
    include_genres_B = [
        'instrumental surf',
    ]
    include_genres = [
        'dark surf',
    ]
    exclude_genres = [
        genre_to_append,
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        exclude_genres,
        include_genres_A=include_genres_A,
        include_genres_B=include_genres_B,
        include_genres=include_genres
    )


    include_playlists_with_track = [
        'Petter Kjørstad - Surf & Twang'
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        playlists_only=True,
        playlists_with_track=t["playlists"]["with_track"],
        include_playlists_with_track=include_playlists_with_track,
        exclude_genres=exclude_genres
    )
    return genres_local

def throwback_00s(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Throwback 00s'
    if (genre_to_append in  genres_local):
        genres_local.remove(genre_to_append)
    include_genres_A = [
        'Throwback',
    ]
    include_genres_B = [
        'Decade-2000',
    ]
    exclude_genres = [
        genre_to_append,
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        exclude_genres,
        include_genres_A=include_genres_A,
        include_genres_B=include_genres_B
    )
    return genres_local

def throwback_90s(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Throwback 90s'
    include_genres_A = [
        'Throwback',
    ]
    include_genres_B = [
        'Decade-1990',
    ]
    exclude_genres = [
        genre_to_append,
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        exclude_genres,
        include_genres_A=include_genres_A,
        include_genres_B=include_genres_B
    )
    return genres_local

def throwback_80s(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Throwback 80s'
    include_genres_A = [
        'Throwback',
    ]
    include_genres_B = [
        'Decade-1980',
    ]
    exclude_genres = [
        genre_to_append,
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        exclude_genres,
        include_genres_A=include_genres_A,
        include_genres_B=include_genres_B
    )
    return genres_local

def throwback(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Throwback'
    include_playlists_with_track = [
        'Spotify - Summertime Throwbacks',
        'Spotify - Throwback Thursday AU & NZ',
        'Spotify - Happy Throwbacks',
        'Spotify - Throwback Thursday',
        'Spotify - Throwback Workout',
        'Spotify - Throwback Jams',
        'Spotify - Acoustic Throwbacks',
        'Spotify - Throwback Thursday',
        'Spotify - Summer Throwbacks',
        'Spotify - #ThrowbackThursday',
        'Spotify - Summertime Throwbacks',
        'Spotify - Rock Throwback',
        'Spotify - Throwback Workout',
        'Spotify - Throwback Party',
        'Spotify - Zurück in die 80er',
        'Spotify - Zurück in die 90er',
        'Spotify - Zurück in die 00er',
    ]
    exclude_genres = [
        genre_to_append,
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        playlists_only=True,
        playlists_with_track=t["playlists"]["with_track"],
        include_playlists_with_track=include_playlists_with_track,
        exclude_genres=exclude_genres
    )
    return genres_local

def confidence(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Confidence'
    include_playlists_with_track = [
        'Spotify - Confidence Boost',
        'Spotify - Walk Like A Badass',
    ]
    exclude_genres = [
        genre_to_append,
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        playlists_only=True,
        playlists_with_track=t["playlists"]["with_track"],
        include_playlists_with_track=include_playlists_with_track,
        exclude_genres=exclude_genres
    )
    return genres_local

def calm(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Calm'
    include_genres = [
        'Coverchill'
    ]
    exclude_genres = [
        genre_to_append,
        'Cool Off',
        'glitch',
        'popping',
        'Amped',
        'Tunnel',
        'Forwardish',
        'Indie Dance',
        'Dreamy'
    ]
    if t["spotify_additional_info"]["features"]:
        loudness = t["spotify_additional_info"]["features"]['loudness']
        energy = t["spotify_additional_info"]["features"]['energy']
        tempo = t["spotify_additional_info"]["features"]['tempo']
        danceability = t["spotify_additional_info"]["features"]['danceability']
        if (
            (float(-15) < float(loudness)) and
            (float(loudness) < float(-5)) and
            (float(75) < float(tempo)) and
            (float(tempo) < float(130)) and
            (float(0.3) < float(energy)) and
            (float(energy) < float(0.5)) and
            (float(danceability) < float(0.5))
        )and not(
            (float(-7) < float(loudness)) and 
            (float(90) < float(tempo)) and 
            (float(0.6) < float(energy)) and 
            (float(0.5) < float(danceability))
        ):
            genres_local = append_to_genre_list_v2(
                genres_local,
                genres_remote,
                genre_to_append,
                exclude_genres=exclude_genres,
                exclude_only=True
            )
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        include_genres=include_genres,
        exclude_genres=exclude_genres
    )            
    return genres_local

#def live(t) -> list:
# genres_remote = t["genres"]["remote"]
# genres_local = t["genres"]["local"]
#    genre_to_append = 'Live'
#    if t["spotify_additional_info"]["features"]:
#        liveness = t["spotify_additional_info"]["features"]['liveness']
#        if (float(liveness) > 0.9) and (genre_to_append not in  genres_local):
#            #print(artist,acousticness)
#            t["genres"]["local"].append(genre_to_append)
#    return genres_local

def acoustic_cover(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Acoustic Cover'
    include_genres = []
    include_artists = ['Pomplamoose']
    exclude_genres = [
        genre_to_append
    ]
    include_genres_A = [
        'Acoustic'
    ]
    include_genres_B = [
        'Cover'
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        artist=t["artist"],
        include_artists=include_artists,
        include_genres_A=include_genres_A,
        include_genres_B=include_genres_B,
        exclude_genres=exclude_genres
    )
    #if ((genre_to_append not in  genres_local) and ('Pomplamoose' == artist)):
    #    genre_list.append(genre_to_append)
    return genres_local

def metal_cover(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Metal Cover'
    include_genres = []
    include_artists = ['Leo']
    exclude_genres = [
        genre_to_append
    ]
    include_genres_A = [
        'Metal'
    ]
    include_genres_B = [
        'Cover'
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        artist=t["artist"],
        include_artists=include_artists,
        include_genres_A=include_genres_A,
        include_genres_B=include_genres_B,
        exclude_genres=exclude_genres
    )
    #if ((genre_to_append not in  genres_local) and ('Pomplamoose' == artist)):
    #    genre_list.append(genre_to_append)
    return genres_local

def coverchill(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Coverchill'
    exclude_genres = [
        genre_to_append,
        'Instrumental Cover',
        'Forwardish',
        # 'Bluegrass'
    ]
    exclude_artists = [
        'Iron Horse'
    ]
    include_genres_A = [
        'Calm',
        'Level1',
        'Dreamy',
        'Level0'
    ]
    include_genres_B = [
        'Cover'
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        artist=t["artist"],
        include_genres_A=include_genres_A,
        include_genres_B=include_genres_B,
        exclude_genres=exclude_genres,
        exclude_artists=exclude_artists
    )
    #if ((genre_to_append not in  genres_local) and ('Pomplamoose' == artist)):
    #    genres_local.append(genre_to_append)
    return genres_local

def indie_cafe(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Indie Café'
    exclude_genres = [
        genre_to_append,
        'Bluegrass',
        'Ska',
        'Metal',
        'Level0',
        'Forwardish',
        'Reggae',
        'IDM',
        'EDM',
        'Hypnotic',
        'Explosive',
        'Orgánica',
        'Metal',
        'Indie Dance'
    ]
    include_genres_A = [
        'Indie Rock',
        'Indie',
        'Indie Pop'
    ]
    include_genres_B = [
        'Calm',
        'Acoustic'
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        artist=t["artist"],
        include_genres_A=include_genres_A,
        include_genres_B=include_genres_B,
        exclude_genres=exclude_genres
    )
    include_moods = [
        'musicnn-chillout',
        'musicnn-easy listening',
        'musicnn-soft'
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        mood_list=t["mood"]["all"],
        include_moods=include_moods,
        exclude_genres=exclude_genres,
        include_genres=include_genres_A
    )
    return genres_local

# tag::powerful_ambient_electronic[]
def powerful_ambient_electronic(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Powerful Ambient Electronic'
    if (genre_to_append in  genres_local):
        genres_local.remove(genre_to_append)
    cyanite_genre_list = ["cyanite-" + s for s in t["genres"]["cyanite"]["list"] if not s.startswith("cyanite-")]
    if len(cyanite_genre_list) > 0:
        t["genres"]["cyanite"]["list"] = cyanite_genre_list
    exclude_artists = [
        'Deon Van Heerden'
    ]
    exclude_genres = [
        genre_to_append,
        'metalcore',
        'Metal',
        'Quiet',
        'Punk',
        'classical',
        'Pop',
        'Hip Rap',
        'ZaTa!',
        'Distracting',
        'Schwarze Nacht',
        'Viking Folk',
        'Industrial Metal',
        'cyanite-calm',
        'cyanite-chilled',
        'Indie Rock'
    ]
    include_genres_A = [
        'cyanite-electronicDance',
        'cyanite-abstractIDMLeftfield',
        'cyanite-deepHouse',
        'cyanite-electro',
        'cyanite-house',
        'cyanite-minimal',
        'cyanite-techHouse',
        'cyanite-techno',
        'Electronic',
        'RetroNight',
        'Unterwasser',
        'Dubstep',
    ]
    include_genres_B = [
        'cyanite-ambient'
    ]
    include_moods_A = [
        'musicnn-fast',
        'musicnn-dance'
    ]
    include_moods_B = [
        'musicnn-ambient'
    ]
    exclude_moods = [
        'musicnn-chillout',
        'musicnn-easy listening',
        'musicnn-soft'
    ]
    if (not set(include_genres_A).isdisjoint(genres_remote+t["genres"]["cyanite"]["list"]) and genres_remote != ['']):
        genres_local = append_to_genre_list_v2(
            genres_local,
            genres_remote+t["genres"]["cyanite"]["list"],
            genre_to_append,
            mood_list=t["mood"]["all"],
            artist=t["artist"],
            exclude_genres=exclude_genres,
            exclude_artists=exclude_artists,
            exclude_moods=exclude_moods,
            include_genres=include_genres_A,
            include_moods_A=include_moods_A,
            include_moods_B=include_moods_B
        )
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote+t["genres"]["cyanite"]["list"],
        genre_to_append,
        mood_list=t["mood"]["all"],
        artist=t["artist"],
        exclude_genres=exclude_genres,
        exclude_artists=exclude_artists,
        exclude_moods=exclude_moods,
        include_moods=include_moods_A,
        include_genres_A=include_genres_A,
        include_genres_B=include_genres_B
    )
    return genres_local
# end::powerful_ambient_electronic[]


def electronic_ambience_of_the_voiceless(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Electronic Ambience Of The Voiceless'
    if (genre_to_append in  genres_local):
        genres_local.remove(genre_to_append)
    exclude_genres = [
        genre_to_append,
        'Radio5',
        'Chillóp',
    ]
    exclude_artists = [
        'Analogik'
    ]
    include_genres_A = [
        'Ambience Of The Voiceless'
    ]
    include_genres_B = [
        'Electronic',
        'Unterwasser',
        'Dubstep',
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        artist=t["artist"],
        exclude_artists=exclude_artists,
        include_genres_A=include_genres_A,
        include_genres_B=include_genres_B,
        exclude_genres=exclude_genres
    )
    return genres_local

def instrumental_ambience_of_the_voiceless(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Instrumental Ambience Of The Voiceless'
    if (genre_to_append in  genres_local):
        genres_local.remove(genre_to_append)
    exclude_genres = [
        genre_to_append,
        'Electronic Ambience Of The Voiceless'
    ]
    include_genres = [
        'Ambience Of The Voiceless'
    ]
    include_instruments = [
        'acoustic',
        'cello',
        'drums',
        'flute',
        'guitar',
        'harp',
        'harpsichord',
        'piano',
        'sitar',
        'strings',
        'violin'
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        artist=t["artist"],
        instrument_list=t["instruments"]["all"],
        include_instruments=include_instruments,
        include_genres=include_genres,
        exclude_genres=exclude_genres
    )
    return genres_local



def ambience_of_the_voiceless(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Ambience Of The Voiceless'
    if (genre_to_append in  genres_local):
        genres_local.remove(genre_to_append)
    # if (artist == "Moderat"):
    #     debug_value=True
    # else:
    #     debug_value=False
    exclude_genres = [
        genre_to_append,
        'Explosive',
        'Viking Folk',
        'Epic',
        'Amped',
        'Loud',
        'Tunnel',
    ]
    exclude_instruments = [
        'vocals',
        'opera',
        'voice',
        'singing',
        'vocal',
    ]
    exclude_genders = [
        'female vocal',
        'female vocalist',
        'female vocalists',
        'female voice',
        'male vocal',
        'male vocalists',
        'male voice',
        'man',
        'woman'
    ]
    include_moods_A = [
        'musicnn-quiet',
        'musicnn-calm',
        'musicnn-chilled',
        'musicnn-chillout',
        'musicnn-soft',
        'musicnn-easy listening',
        'musicnn-slow'
    ]
    include_moods_B = [
        'musicnn-ambient'
    ]
    exclude_moods = [
        'musicnn-fast'
    ]

    if t["cyanite_additional_info"]:
        if (t["cyanite_additional_info"]["voicePresenceProfile"] == "none"):
            genres_local = append_to_genre_list_v2(
                genres_local,
                genres_remote,
                genre_to_append,
                mood_list=t["mood"]["all"],
                gender_list=t["gender"]["musicnn"],
                instrument_list=t["instruments"]["all"],
                mood_only=True,
                # debug=debug_value,
                exclude_genres=exclude_genres,
                exclude_moods=exclude_moods,
                exclude_instruments=exclude_instruments,
                exclude_genders=exclude_genders,
                include_moods_A=include_moods_A,
                include_moods_B=include_moods_B
            )

    if t["cyanite_additional_info"]:
        if (
            (t["cyanite_additional_info"]["voicePresenceProfile"] == "none") and
            ('cyanite-ambient' in  genres_remote)
            ):
            genres_local = append_to_genre_list_v2(
                genres_local,
                genres_remote,
                genre_to_append,
                mood_list=t["mood"]["all"],
                gender_list=t["gender"]["musicnn"],
                instrument_list=t["instruments"]["all"],
                mood_only=True,
                # debug=debug_value,
                exclude_genres=exclude_genres,
                exclude_moods=exclude_moods,
                exclude_instruments=exclude_instruments,
                exclude_genders=exclude_genders,
                include_moods=include_moods_A
            )

    if t["spotify_additional_info"]["features"]:
        speechiness = t["spotify_additional_info"]["features"]['speechiness']
        instrumentalness = t["spotify_additional_info"]["features"]['instrumentalness']
        valence = t["spotify_additional_info"]["features"]['valence']
        energy = t["spotify_additional_info"]["features"]['energy']
        sadness = (1 - energy) * (1 - valence)
        happiness = energy * valence
        anger = energy * (1 - valence)
        if (
            (float(0.100) > float(speechiness)) and
            (float(0.100) < float(instrumentalness))
        ):
            genres_local = append_to_genre_list_v2(
                genres_local,
                genres_remote,
                genre_to_append,
                mood_list=t["mood"]["all"],
                gender_list=t["gender"]["musicnn"],
                instrument_list=t["instruments"]["all"],
                mood_only=True,
                # debug=debug_value,
                exclude_genres=exclude_genres,
                exclude_moods=exclude_moods,
                exclude_instruments=exclude_instruments,
                exclude_genders=exclude_genders,
                include_moods_A=include_moods_A,
                include_moods_B=include_moods_B
            )
    return genres_local

def deutscher_partpypunk(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Deutscher Partypunk'
    if (genre_to_append in  genres_local):
        genres_local.remove(genre_to_append)
    exclude_genres = [
        genre_to_append,
        'Elektropunk',
        'Depri Disko',
        'English Lyrics',
    ]
    exclude_artists = [
        'Donots',
        'Kettcar',
        'Bonaparte',
        'Die Goldenen Zitronen',
        'Keine Zähne Im Maul Aber La Paloma Pfeifen'
    ]
    include_genres_A = [
        'Deutschpunk'
    ]
    include_genres_B = [
        'Dancey',
        'Amped'
    ]
    include_moods_A = [
        'musicnn-dance'
    ]
    include_moods_B = [
        'musicnn-party'
    ]
    exclude_moods = [
        'musicnn-chillout',
        'musicnn-easy listening',
        'musicnn-soft'
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        artist=t["artist"],
        exclude_genres=exclude_genres,
        exclude_artists=exclude_artists,
        exclude_moods=exclude_moods,
        include_genres_A=include_genres_A,
        include_genres_B=include_genres_B
    )
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        artist=t["artist"],
        exclude_artists=exclude_artists,
        mood_list=t["mood"]["all"],
        exclude_genres=exclude_genres,
        exclude_moods=exclude_moods,
        include_genres=include_genres_A,
        include_moods_A=include_moods_A, 
        include_moods_B=include_moods_B
    )
    return genres_local

def deutschpunk(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Deutschpunk'
    if (genre_to_append in  genres_local):
        genres_local.remove(genre_to_append)
    exclude_genres = [
        genre_to_append,
        'English Lyrics'
    ]
    exclude_artists = [
        'Donots'
    ]
    include_genres_A = [
        'Deutsch'
    ]
    include_genres_B = [
        'Punk'
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        artist=t["artist"],
        exclude_artists=exclude_artists,        
        exclude_genres=exclude_genres,
        include_genres_A=include_genres_A,
        include_genres_B=include_genres_B
    )
    return genres_local

def hip_hop_cafe(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Hip Hop Café'
    exclude_artists = [
        'Analogik',
        'Flight Of The Conchords',
        'Deichkind',
        'Amewu',
        'Fettes Brot',
        'Die Fantastischen Vier',
        'Antilopen Gang',
        'Alligatoah',
        'Die Antwoord',
        'Too Many Zooz',
        'Weird',
        'Darren Korb'
    ]
    exclude_genres = [
        genre_to_append,
        'Bluegrass',
        'Ska',
        'Electronic Ambience Of The Voiceless',
        'Antideutsche',
        'Metal',
        'Forwardish',
        'Reggae',
        'Dubstep',
        'Assi',
        'redneck',
        'IDM',
        'EDM',
        'Hypnotic',
        'Orgánica',
        'Metal',
        'Dreamy',
        'Indie Dance'
    ]
    include_genres_A = [
        'Hip Rap',
        'Chillóp',
        'Jazzap'
    ]
    include_genres_B = [
        'Calm',
        'Acoustic',
        'Smooth',
    ]
    exclude_moods = [
        'musicnn-loud',
        'musicnn-fast'
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        artist=t["artist"],
        mood_list=t["mood"]["all"],
        exclude_moods=exclude_moods,
        include_genres_A=include_genres_A,
        include_genres_B=include_genres_B,
        exclude_genres=exclude_genres,
        exclude_artists=exclude_artists
    )
    include_moods = [
        'musicnn-chillout',
        'musicnn-easy listening',
        'musicnn-soft'
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        artist=t["artist"],
        mood_list=t["mood"]["all"],
        exclude_moods=exclude_moods,
        include_moods=include_moods,
        exclude_genres=exclude_genres,
        exclude_artists=exclude_artists,
        include_genres=include_genres_A
    )
    if ((genre_to_append in  genres_local)) and (not set(['Electronic Ambience Of The Voiceless']).isdisjoint(genres_local) and genres_local != ['']):
        genres_local.remove(genre_to_append)
    #if ((genre_to_append in  genres_local)) and (not set(exclude_moods).isdisjoint(t["mood"]["all"]) and t["mood"]["all"] != ['']):
    #    genres_local.remove(genre_to_append)
    return genres_local

def soul_soothers(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Soul Soothers'
    if (genre_to_append in  genres_local):
        genres_local.remove(genre_to_append)
    # cyanite_genre_list = ["cyanite-" + s for s in t["genres"]["cyanite"]["list"] if not s.startswith("cyanite-")]
    # if len(cyanite_genre_list) > 0:
    #     t["genres"]["cyanite"]["list"] = cyanite_genre_list
    exclude_genres = [
        genre_to_append,
        'Vapor Soul',
        'Forwardish',
        'Dubstep',
        'EDM',
        'Electronic',
        'Jazzap',
        'Forward',
    ]
    exclude_artists=[
        "Freundeskreis, Joy Denalane",
        "Laing",
        "Soom T",
    ]
    include_genres = [
        'Neo Soul',
        'Soul',
        'R&b',
    ]
    include_moods_A = [
        'cyanite-chilled',
        'cyanite-calm',
        'cyanite-ethereal',
        'musicnn-chillout'
        'musicnn-beautiful'
    ]
    # include_moods_B = [
    #     'cyanite-uplifting',
    #     'musicnn-easy listening',
    # ]
    exclude_moods = [
        'cyanite-aggressive',
        'cyanite-scary',
        'musicnn-party',
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        artist=t["artist"],
        exclude_artists=exclude_artists,
        mood_list=t["mood"]["all"],
        include_moods=include_moods_A,
        # include_moods_B=include_moods_B,
        exclude_moods=exclude_moods,
        include_genres=include_genres,
        exclude_genres=exclude_genres
    )
    # Spotify - Hangover Friendly
    # "Spotify - Feelin' Good",
    # "Spotify - Good Vibes",
    # "Spotify - 元気Booster
    # Spotify - Low-Key",
    include_playlists_with_track = [
        "Spotify - Soul Coffee"
        "Spotify - Chilled R&B"
        "The Sounds of Spotify - The Sound of Chill R&B",
        "Spotify - Soulful Day",
        "Spotify - Sweet Soul Chillout",
        "Spotify - Acoustic Soul",
        "Spotify - Soul Soothers",
        "Spotify - Soul Lounge"
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        artist=t["artist"],
        exclude_artists=exclude_artists,
        playlists_only=True,
        playlists_with_track=t["playlists"]["with_track"],
        include_playlists_with_track=include_playlists_with_track,
        exclude_genres=exclude_genres
    )
    return genres_local

def acoustic_pop(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Acoustic Pop'
    include_genres = [
        'acoustic pop'
    ]
    include_genres_A = [
        'Acoustic'
    ]
    include_genres_B = [
        'Pop'
    ]
    exclude_genres = [
        genre_to_append
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        include_genres_A=include_genres_A,
        include_genres_B=include_genres_B,
        include_genres=include_genres,
        exclude_genres=exclude_genres
    )
    #genre_list = append_to_genre_list_multi(
    #    genre_list,
    #    comment_list,
    #    genre_to_append,
    #    exclude_genres=exclude_genres
    #)
    return genres_local

def trip_hop(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Trip Hop'
    include_genres = [
        'trip hop',
        'triphop',
        'trip-hop'
    ]
    exclude_genres = [
        genre_to_append,
        'Metal',
        'Futurepop',
        'Folk',
        'Indie Rock'
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        include_genres=include_genres,
        exclude_genres=exclude_genres
    )
    return genres_local

def industrial_metal(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Industrial Metal'
    industrial_metal_genres = [
        'industrial metal'
    ]
    industrial_metal_exclude_genres = [
        genre_to_append,
        'Neoclassical Darkwave',
        'Electronic',
        'Futurepop'
    ]

    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        include_genres=industrial_metal_genres,
        exclude_genres=industrial_metal_exclude_genres
    )
    return genres_local

def ska(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    ska_genres = [
        'ska'
    ]
    ska_exclude_genres = [
        'Ska',
        'EDM',
        'Hip Rap',
        'Radio1',
        'Radio3',
        'Radio4',
        'Radio5',
        'Radio6',
        'Radio7',
        'Metal',
        'EBM'
    ]
    if (
        ((not set(ska_genres).isdisjoint(genres_remote))  or (not set(ska_genres).isdisjoint(genres_local)))
        and not (
            (not set(ska_exclude_genres).isdisjoint(genres_local)) or (not set(ska_exclude_genres).isdisjoint(genres_remote))
            )
    ):
        genres_local.append("Ska")
    return genres_local

def indie_dance(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Indie Dance'
    include_genres = [
        'Indie Rock',
        'Indie Pop'
    ]
    exclude_artists = [
        'Sia',
        'THE SIXTH LIE',
        'CHVRCHES'
    ]
    exclude_genres = [
        genre_to_append,
        'Punk',
        'Hip Rap',
        'Focus',
        'Metal',
        'RetroNight',
        'Acoustic',
        'EDM',
        'Acoustic Pop',
        'Dubstep',
        'Calm',
        'Cool Off'
    ]
    if t["spotify_additional_info"]["features"]:
        energy = t["spotify_additional_info"]["features"]['energy']
        danceability = t["spotify_additional_info"]["features"]['danceability']
        loudness = t["spotify_additional_info"]["features"]['loudness']
        if (
            (float(-5) < float(loudness)) and
            (float(0.5) < float(danceability)) and
            (float(0.8) < float(energy))
        ):
            genres_local = append_to_genre_list_v2(
                genres_local,
                genres_remote,
                genre_to_append,
                artist=t['artist'],
                exclude_artists=exclude_artists,
                include_genres=include_genres,
                exclude_genres=exclude_genres
            )

        include_playlists_with_track_A = [
            "Spotify - Indie Klassiker",
            "Spotify - Indie Classics",
            "Spotify - Indie Mix",
            "The Sounds of Spotify - The Sound of Indie Rock",
        ]
        include_playlists_with_track_B = [
            "Spotify - Indie Disco",
            "Spotify - Amped up.",
            "Spotify - Party Deluxe",
            "Spotify - Power Pop",
            "Songpickr - Rock Party",
        ]
        
        genres_local = append_to_genre_list_v2(
            genres_local,
            genres_remote,
            genre_to_append,
            artist=t['artist'],
            exclude_artists=exclude_artists,
            playlists_only=True,
            playlists_with_track=t["playlists"]["with_track"],
            include_playlists_with_track_A=include_playlists_with_track_A,
            include_playlists_with_track_B=include_playlists_with_track_B,
            exclude_genres=exclude_genres
        )
    return genres_local

# def happy_background(t) -> list:
#     genres_remote = t["genres"]["remote"]
#     genres_local = t["genres"]["local"]
#     genre_to_append = 'Feature2'
#     exclude_genres = [
#         genre_to_append,
#         'Schwarze Nacht',
#         'Dancey',
#         'Depri Disko',
#         'Loud',
#         'Sad'
#     ]
#     if t["spotify_additional_info"]["features"]:
#         speechiness = t["spotify_additional_info"]["features"]['speechiness']
#         instrumentalness = t["spotify_additional_info"]["features"]['instrumentalness']
#         valence = t["spotify_additional_info"]["features"]['valence']
#         energy = t["spotify_additional_info"]["features"]['energy']
#         sadness = (1 - energy) * (1 - valence)
#         happiness = energy * valence
#         anger = energy * (1 - valence)
#         if (
#             (float(0.1) > float(speechiness)) and
#             (float(0.1) < float(instrumentalness)) and
#             (float(0.8) < float(happiness)) and
#             (float(0.2) > float(anger))
#         ):
#             genres_local = append_to_genre_list_v2(
#                 genres_local,
#                 genres_remote,
#                 genre_to_append,
#                 exclude_only=True,
#                 exclude_genres=exclude_genres
#             )
#     return genres_local


def russian_hip_hop(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Russian Hip Hop'
    include_genres_A = [
        'Hip Rap'
    ]
    include_genres_B = [
        'Russian'
    ]
    exclude_genres = [
        genre_to_append
    ]
    exclude_artists = [
        "Little Big",
        "DJ Vadim, Big Red, 5'nizza"
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        artist=t["artist"],
        include_genres_A=include_genres_A,
        include_genres_B=include_genres_B,
        exclude_genres=exclude_genres,
        exclude_artists=exclude_artists
    )
    return genres_local

def russian_electronic(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Russian Electronic'
    include_genres = [
        'russian electronic'
    ]
    exclude_genres = [
        genre_to_append
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        artist=t["artist"],
        include_genres=include_genres,
        exclude_genres=exclude_genres
    )
    return genres_local

def tanzmetall(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Tanzmetall'
    include_genres_A = [
        'Dancey'
    ]
    include_genres_B = [
        'Metal'
    ]
    exclude_genres = [
        genre_to_append,
        'Punk',
        'Hip Rap',
        'Futurepop',
        'Focus',
        'RetroNight',
        'Dubstep',
        'Calm',
        'Dreamy',
        'Hanging Out',
        'Cool Off'
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        include_genres_A=include_genres_A,
        include_genres_B=include_genres_B,
        exclude_genres=exclude_genres
    )
    return genres_local

def indie_chill(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Indie Chill'
    include_genres_A = [
        'Radio2',
    ]
    include_genres_B = [
        'Calm',
        'Dreamy',
        'downtempo',
        'Acoustic',
        'chillout'
    ]
    include_artists = [
        'The National'
    ]
    exclude_genres = [
        genre_to_append,
        'Indie Dance'
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        # debug=True,
        include_genres_A=include_genres_A,
        include_genres_B=include_genres_B,
        exclude_genres=exclude_genres,
        include_artists=include_artists
    )
    return genres_local

def soft_north(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    soft_north_genres = [
        'russian folk',
        'polish folk'
    ]
    soft_north_artists = [
        'Laboratorium Pieśni',
        'Waldkauz',
        'Otava Yo'
    ]
    soft_north_exclude_genres = [
        'Soft North'
    ]
    if (
        ((not set(soft_north_genres).isdisjoint(genres_remote))  or (not set(soft_north_genres).isdisjoint(genres_local)) or (not set(soft_north_artists).isdisjoint([t["artist"]])))
        and not (
            (not set(soft_north_exclude_genres).isdisjoint(genres_local)) or (not set(soft_north_exclude_genres).isdisjoint(genres_remote))
            )
    ):
        genres_local.append("Soft North")
    return genres_local

def electronic(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Electronic'
    include_genres = [
        'Tunnel',
        'Unterwasser',
        'arab electronic',
        'russian electronic',
        'ambient electronic',
        'progressive electronic',
        'danish electronic',
        'icelandic electronic',
        'boston electronic',
        'industrial-electronic',
        'psychedelic electronic',
        'ethnic electronic',
        'leipzig electronic',
        'south african electronic',
        'polish experimental electronic',
        'electronic',
        'slovak electronic',
        'cologne electronic',
        'electronic',
        'ukrainian electronic',
        'dusseldorf electronic',
        'electronic',
        'scottish electronic',
        'live electronic',
        'electronic',
        'arab electronic',
        'electronic',
        'electronic',
        'nz electronic',
        'electronic',
        'turkish electronic',
        'frankfurt electronic',
        'polish electronic',
        'electronic',
        'canadian electronic',
        'uk experimental electronic',
        'hamburg electronic',
        'electronic',
        'organic electronic',
        'electronic',
        'slovenian electronic',
        'electronic',
        'dance and electronic',
        'electronic',
        'electronic',
        'Ambient Electronic',
        'polish electronic',
        'House',
        'EDM',
        'Dark Electronic'
    ]
    exclude_genres = [
        genre_to_append,
        'Acoustic'
    ]
    exclude_artists = [
        'Pomplamoose',
        'Nils Frahm'
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        artist=t["artist"],
        include_genres=include_genres,
        exclude_genres=exclude_genres,
        exclude_artists=exclude_artists
    )
    #if (('Pomplamoose' ==  artist)and('Electronic' in  genres_local)):
    #    genres_local.remove('Electronic')
    return genres_local

def ska_punk(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    ska_punk_genres = [
        'ska punk',
        'modern ska punk'
    ]
    ska_punk_exclude_genres = [
        'Ska Punk'
    ]
    if (
        ((not set(ska_punk_genres).isdisjoint(genres_remote))  or (not set(ska_punk_genres).isdisjoint(genres_local)))
        and not (
            (not set(ska_punk_exclude_genres).isdisjoint(genres_local)) or (not set(ska_punk_exclude_genres).isdisjoint(genres_remote))
            )
    ):
        genres_local.append("Ska Punk")
    return genres_local

def piano(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Pianist'
    include_genres =  [
        'pianist',
        'piano rock',
        'russian pianist',
        'experimental piano',
        'prepared piano',
        'new age piano',
        'classical piano',
        'german classical piano',
        'american pianist',
        'jazz piano',
        'piano pop',
        'piano',
        'piano blues',
        'piano pop rock',
    ]
    exclude_genres = [
        genre_to_append
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        include_genres=include_genres,
        exclude_genres=exclude_genres
    )    
    return genres_local

def house(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'House'
    include_genres =  [
        'fidget house',
        'house music',
        'australian house',
        'deep soul house',
        'pop house',
        'canadian house',
        'lo-fi house',
        'brass house',
        'bass house',
        'diva house',
        'outsider house',
        'brazilian house',
        'deep disco house',
        'euro house',
        'hard house',
        'nordic house',
        'italo house',
        'german tech house',
        'float house',
        'deep tropical house',
        'chicago house',
        'vocal house',
        'minimal tech house',
        'progressive electro house',
        'french tech house',
        'disco house',
        'acid house',
        'german house',
        'ambient house',
        'deep euro house',
        'french house',
        'hip house',
        'witch house',
        'organic house',
        'deep house',
        'progressive house',
        'tech house',
        'tropical house',
        'microhouse',
        'house',
        'filter house',
        'electro house'
    ]
    exclude_genres = [
        genre_to_append
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        include_genres=include_genres,
        exclude_genres=exclude_genres
    )
    return genres_local

def idm(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    idm_genres = [
        'deep idm',
        'idm',
        'intelligent dance',
        'intelligent dance music'
    ]
    idm_exclude_genres = [
        'IDM'
    ]
    if (
        ((not set(idm_genres).isdisjoint(genres_remote))  or (not set(idm_genres).isdisjoint(genres_local)))
        and not (
            (not set(idm_exclude_genres).isdisjoint(genres_local)) or (not set(idm_exclude_genres).isdisjoint(genres_remote))
            )
    ):
        genres_local.append("IDM")
    return genres_local

def dubstep(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Dubstep'
    include_genres =  [
        'dubstep',
        'deep dubstep',
        'vocal dubstep',
        'post-dubstep',
        'classic dubstep',
        'melodic dubstep'
    ]
    exclude_genres = [
        genre_to_append
    ]
    exclude_artists = [
        'Taylor Swift'
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        artist=t["artist"],
        exclude_artists=exclude_artists,
        include_genres=include_genres,
        exclude_genres=exclude_genres
    )
    return genres_local

def deutsch(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Deutsch'
    include_genres = [
        'german indie',
        'swiss indie',
        'dusseldorf indie',
        'austrian indie',
        'vienna indie',
        'german indie rock',
        'cologne indie',
        'german punk',
        'german industrial',
        'german punk rock',
        'german oi',
        'german-lyrics',
        'neue deutsche härte',
        'neue deutsche welle',
        'german hip hop',
        'deep german hip hop',
        'high german ',
        'bayerischer rap',
        'hamburg hip hop',
        'german cloud rap',
        'indie deutschrap',
        'german alternative rap',
        'schweizer rap',
        'swiss rap',
        'german singer-songwriter',
        'liedermacher',
        'german post-punk',
        'volkspop',
        'deep german indie',  
        'hamburger schule',
        'blasmusik', 
        'german pop', 
        'deutschpunk', 
        'deutschrock', 
        'german reggae',  
        'german rock',  
        'elektropunk',  
        'antideutsche',  
        'Antideutsche',  
        'salzburg indie'
    ]
    include_artists = [
        'RAUM27',
        'Juli',
        'Heino',
        'KAFFKIEZ'
    ]
    exclude_genres = [
        genre_to_append,
        'swedish indie rock',
        'English Lyrics',
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        include_genres=include_genres,
        exclude_genres=exclude_genres
    )
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        artist=t["artist"],
        artists_only=True,
        include_artists=include_artists,
        exclude_genres=exclude_genres
    )
    return genres_local

def antideutsche(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Antideutsche'
    include_genres = [
        'antideutsche'
    ]
    exclude_genres = [
        genre_to_append
    ]
    exclude_artists = [
        'Shaban & Käptn Peng'
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        artist=t["artist"],
        include_genres=include_genres,
        exclude_genres=exclude_genres,
        exclude_artists=exclude_artists
    )
    return genres_local

def minimal_focus(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Minimal Focus'
    include_genres_A = [
        'Minimal Techno'
    ]
    include_genres_B = [
        'Focus'
    ]
    exclude_genres = [
        genre_to_append
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        include_genres_A=include_genres_A,
        include_genres_B=include_genres_B,
        exclude_genres=exclude_genres
    )
    return genres_local

def focus_beats(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Focus Beats'
    include_genres = [
        'focus beats'
    ]
    include_genres_A = [
        'Minimal Techno',
        'minimal techno',
        'deep minimal techno',
        'tech house',
        'organic electronic',
        'microhouse',
        'chill beats',
        'lo-fi beats',
        'lo-fi cover',
        'lo-fi jazzhop',
        'chillhop',
        'japanese chillhop',
        'jazz boom bap',
        'tropical house',
    ]
    include_genres_B = [
        'Quiet'
    ]
    exclude_genres = [
        genre_to_append
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        include_genres_A=include_genres_A,
        include_genres_B=include_genres_B,
        exclude_genres=exclude_genres
    )
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        include_genres=include_genres,
        exclude_genres=exclude_genres
    )
    if (
            ('chillout' in t["mood"]["all"]) or
            ('easy listening' in t["mood"]["all"]) or
            ('soft' in t["mood"]["all"])
        ):
            genres_local = append_to_genre_list_v2(
                genres_local,
                genres_remote,
                genre_to_append,
                exclude_genres=exclude_genres,
                include_genres=include_genres_A
            )
    return genres_local

def black_metal_focus(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Black Metal Focus'
    include_genres = [
        'atmospheric black metal',
        'voidgaze',
        'atmospheric doom',
        'ambient black metal',
        'blackgaze'
    ]
    exclude_genres = [
        genre_to_append
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        include_genres=include_genres,
        exclude_genres=exclude_genres
    )
    return genres_local

def art_pop(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Art Pop'
    include_genres = [
        'art pop'
    ]
    exclude_artists = [
        'The Beatles',
        'Gorillaz'
    ]
    exclude_genres = [
        genre_to_append
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        include_genres=include_genres,
        exclude_genres=exclude_genres
    )
    if (
        ((not set(exclude_artists).isdisjoint([t["artist"]]))  and (not set([genre_to_append]).isdisjoint(genres_local)))
    ):
        genres_local.remove(genre_to_append)
    return genres_local

# def female_person(t) -> list:
#     genres_remote = t["genres"]["remote"]
#     genres_local = t["genres"]["local"]
#     if (('female' in  genres_remote) and ('person' in  genres_remote) and ('group' not in  genres_remote) and ('Female Person' not in  genres_local)):
#         genres_local.append("Female Person")
#     return genres_local

# def female_vocals(t) -> list:
#     genres_remote = t["genres"]["remote"]
#     genres_local = t["genres"]["local"]
#     if (('female vocals' in  genres_remote) and ('Female Vocals' not in  genres_local)):
#         genres_local.append("Female Vocals")
#     return genres_local

# def male_vocals(t) -> list:
#     genres_remote = t["genres"]["remote"]
#     genres_local = t["genres"]["local"]
#     if (('male vocals' in  genres_remote) and ('Male Vocals' not in  genres_local)):
#         genres_local.append("Male Vocals")
#     return genres_local

# def male_person(t) -> list:
#     genres_remote = t["genres"]["remote"]
#     genres_local = t["genres"]["local"]
#     if (('male' in  genres_remote) and ('person' in  genres_remote) and ('group' not in  genres_remote) and ('Male Person' not in  genres_local)):
#         genres_local.append("Male Person")
#     return genres_local

def house(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    house_genres = [
       'german house',
       'disco house',
       'filter house',
        'deep euro house',
        'euro house',
        'tech house',
        'house'
    ]              
    if (( not set(house_genres).isdisjoint(genres_remote)) and ('House' not in  genres_local)):
        genres_local.append("House")
    return genres_local

def edm(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'EDM'
    include_genres = [
       'german house',
        'deep euro house',
        'euro house',
        'tech house',
        'Tropical House',
        'Deep Tropical House',
        'house',
        'microhouse',
        'filter house',
        'deep minimal techno',
        'deep euro house',
        'deep idm',
        'edm',
        'raggatek',
        'techno',
        'minimal techno',
        'german techno',
        'pop edm'
    ]
    exclude_genres = [
        genre_to_append
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        include_genres=include_genres,
        exclude_genres=exclude_genres
    )
    return genres_local

def unterwasser(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Unterwasser'
    if t["genres"]["cyanite"]["dict"]:
        if (
                float(t["genres"]["cyanite"]["dict"].get('deepHouse') or 0) > float(0.7) and
                float(t["genres"]["cyanite"]["dict"].get('deepHouse') or 0) > float(t["genres"]["cyanite"]["dict"].get('house') or 0)
            ):
            # print(genres_from_cyanite)
            genres_remote = genres_remote+t["genres"]["cyanite"]["list"]
    include_genres = [
       'deep minimal techno',
       'deep idm',
       'deep euro house',
       'deepHouse'
    ]
    exclude_genres = [
        genre_to_append,
        'Powerful Ambient Electronic'
    ]
    if (( not set(['Powerful Ambient Electronic']).isdisjoint(genres_local)) and (genre_to_append in  genres_local)):
        genres_local.remove(genre_to_append)
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        include_genres=include_genres,
        exclude_genres=exclude_genres
    )

    include_playlists_with_track = [
        "Spotify - Deep House Relax",
        "Spotify - Deep Concentration" 
        "Spotify - Truly Deeply House",
        "The Sounds of Spotify - The Sound of Deep House",
        "The Sounds of Spotify - The Sound of Deep Deep House",
        "The Sounds of Spotify - The Sound of Deep Euro House"
        "The Sounds of Spotify - The Sound of Deep IDM",
        #"The Sounds of Spotify - The Sound of Deep DnB",
        "The Sounds of Spotify - The Sound of Deep Liquid Bass"
        #"The Sounds of Spotify - The Sound of Deep Groove House",
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        playlists_only=True,
        playlists_with_track=t["playlists"]["with_track"],
        include_playlists_with_track=include_playlists_with_track,
        exclude_genres=exclude_genres
    )

    return genres_local

def electrosphere(t) -> list:
    genres_remote = t["genres"]["remote"]+t["genres"]["cyanite"]["list"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Electrosphere'
    if (genre_to_append in  genres_local):
        genres_local.remove(genre_to_append)
    include_genres = [
       'Radio3',
       'RetroNight',
       'Focus Beats',
       'Unterwasser',
       'Tunnel',
       'House',
       'Synthwave',
       'Microhouse',
       'EDM',
    ]
    exclude_genres = [
        genre_to_append,
       'Chillóp',
    ]
    include_moods = [
        'cyanite-ethereal',
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        mood_list=t["mood"]["all"],
        include_moods=include_moods,
        include_genres=include_genres,
        exclude_genres=exclude_genres
    )

    return genres_local

def melancholic_metal(t) -> list:
    genres_remote = t["genres"]["remote"]+t["genres"]["cyanite"]["list"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Melancholic Metal'
    if (genre_to_append in  genres_local):
        genres_local.remove(genre_to_append)
    include_genres = [
       'Metal',
    ]
    exclude_genres = [
        genre_to_append,
    ]
    include_moods_A = [
        'cyanite-ethereal',
    ]
    include_moods_B = [
        'cyanite-dark',
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        mood_list=t["mood"]["all"],
        include_moods_A=include_moods_A,
        include_moods_B=include_moods_B,
        include_genres=include_genres,
        exclude_genres=exclude_genres
    )

    return genres_local

def cloudrap(t) -> list:
    genres_remote = t["genres"]["remote"]+t["genres"]["cyanite"]["list"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Rap In The Clouds'
    if (genre_to_append in  genres_local):
        genres_local.remove(genre_to_append)
    include_genres_specific = [
       'Dark Trap',
       'cloud rap',
       'Trip Hop',
       'jazzyHipHop',
       'Jazzap',
       'Ambeat',
       'Chillóp',
    #    'neoSoul',
    #    'conscious Hip Hop',
    ]
    include_genres_general = [
       'german alternative rap',
       'Hip Rap',
       'chanson',
       'rapHipHop',
       'Neoclassical Darkwave',
       'Trap',
    ]
    exclude_genres = [
        genre_to_append,
        'Electrosphere',
        'Indie Café',
        'Ninja',
        'Art Pop',
        'Indie Klassiker',
        'rap rock',
        'Electropop',
        'Electro Swing',
        'Vapor Pop',
    ]
    exclude_artists = [
        "Alligatoah",
        'Analogik',
        'Blumentopf',
        'Antilopen Gang',
        'Darren Korb',
    ]
    include_moods_A = [
        'cyanite-ethereal',
    ]
    include_moods_B = [
        'cyanite-chilled',
        'cyanite-dark',
        'cyanite-calm',
    ]
    if t["cyanite_additional_info"]:
        if (t["cyanite_additional_info"]["voicePresenceProfile"] not in [ "none","low"]):
            genres_local = append_to_genre_list_v2(
                genres_local,
                genres_remote,
                genre_to_append,
                mood_list=t["mood"]["all"],
                artist=t["artist"],
                exclude_artists=exclude_artists,
                include_moods=include_moods_A,
                include_genres=include_genres_specific,
                exclude_genres=exclude_genres
            )
    # genre_list = append_to_genre_list_v2(
    #     genre_list,
    #     comment_list,
    #     genre_to_append,
    #     mood_list=mood,
    #     artist=t["artist"],
    #     exclude_artists=exclude_artists,
    #     include_moods_A=include_moods_A,
    #     include_moods_B=include_moods_B,
    #     include_genres=include_genres_general,
    #     exclude_genres=exclude_genres
    # )

    return genres_local

def punk(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Punk'
    include_genres = [
        'punk',
        'indie punk',
        'german punk',
        'emo punk',
        'german punk rock',
        'german oi',
        'australian garage punk',
        'boston punk'
    ]
    exclude_genres = [
        genre_to_append
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        include_genres=include_genres,
        exclude_genres=exclude_genres
    )
    return genres_local

def brass(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Brass'
    include_genres = [
        'brass band',
        'blasmusik'
    ]
    exclude_genres = [
        genre_to_append
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        include_genres=include_genres,
        exclude_genres=exclude_genres
    )
    return genres_local

def bluegrass(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Bluegrass'
    include_genres = [
        'bluegrass',
        'progressive bluegrass'
    ]
    exclude_genres = [
        genre_to_append
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        include_genres=include_genres,
        exclude_genres=exclude_genres
    )
    return genres_local

def lounge(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    genre_to_append = 'Lounge'
    include_genres = [
        'chill lounge',
        'lounge'
    ]
    exclude_genres = [
        genre_to_append
    ]
    genres_local = append_to_genre_list_v2(
        genres_local,
        genres_remote,
        genre_to_append,
        include_genres=include_genres,
        exclude_genres=exclude_genres
    )
    return genres_local

def adult_lounge(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    if (('Lounge' in  genres_local) and ('Adult Standards' in  genres_local) and ('Adult Lounge' not in  genres_local)):
        genres_local.append("Adult Lounge")
        genres_local.remove("Lounge")
    return genres_local


def filter_lounge(t) -> list:
    genres_remote = t["genres"]["remote"]
    genres_local = t["genres"]["local"]
    if (('Lounge' in  genres_local) and ('filter house' in  genres_remote) and ('Filter Lounge' not in  genres_local)):
        genres_local.append("Filter Lounge")
        genres_local.remove("Lounge")
    if (('indie soul' in  genres_remote) and ('filter house' in  genres_remote) and ('Filter Lounge' not in  genres_local)):
        genres_local.append("Filter Lounge")
    return genres_local


pathlist = Path(MUSIC_PATH).glob('**/*.mp3')


def genre_converter_minimal_effort(
    track_info
):
    t = track_info
    t["genres"]["local"] = minimal_effort(t, "Breakbeat")
    t["genres"]["local"] = minimal_effort(t, "Dark Clubbing")
    t["genres"]["local"] = minimal_effort(t, "Electroclash")
    t["genres"]["local"] = minimal_effort(t, "Speedrun")
    t["genres"]["local"] = minimal_effort(t, "Drift Phonk")
    t["genres"]["local"] = minimal_effort(t, "Darksynth")
    t["genres"]["local"] = minimal_effort(t, "Surf Music")
    t["genres"]["local"] = minimal_effort(t, "Dark Pop")
    t["genres"]["local"] = minimal_effort(t, "Vapor Twitch")
    t["genres"]["local"] = minimal_effort(t, "Traditional Blues")
    t["genres"]["local"] = minimal_effort(t, "Neo-Psychedelic")
    t["genres"]["local"] = minimal_effort(t, "Melancholia")
    t["genres"]["local"] = minimal_effort(t, "Darkstep")
    t["genres"]["local"] = minimal_effort(t, "Shamanic")
    t["genres"]["local"] = minimal_effort(t, "Deep Groove House")
    t["genres"]["local"] = minimal_effort(t, "Tribal House")
    t["genres"]["local"] = minimal_effort(t, "Grunge")
    t["genres"]["local"] = minimal_effort(t, "Turntablism")
    t["genres"]["local"] = minimal_effort(t, "Ambeat")
    t["genres"]["local"] = minimal_effort(t, "Drumm And Bass")
    t["genres"]["local"] = minimal_effort(t, "Raggatek")
    t["genres"]["local"] = minimal_effort(t, "Pop Punk")
    t["genres"]["local"] = minimal_effort(t, "Rune Folk")
    t["genres"]["local"] = minimal_effort(t, "Viking Folk")
    t["genres"]["local"] = minimal_effort(t, "Medieval Folk")
    t["genres"]["local"] = minimal_effort(t, "Raggatek")
    t["genres"]["local"] = minimal_effort(t, "Dark Synth")
    t["genres"]["local"] = minimal_effort(t, "Synthwave")
    t["genres"]["local"] = minimal_effort(t, "Night Drive Phonk") 
    t["genres"]["local"] = minimal_effort(t, "Deconstructed Club") 
    t["genres"]["local"] = minimal_effort(t, "Dark Trap") 
    t["genres"]["local"] = minimal_effort(t, "Weirdcore") 
    t["genres"]["local"] = minimal_effort(t, "Shiver Pop") 
    t["genres"]["local"] = minimal_effort(t, "Gauze Pop") 
    t["genres"]["local"] = minimal_effort(t, "Psychedelic Soul") 
    t["genres"]["local"] = minimal_effort(t, "Folk-Pop") 
    t["genres"]["local"] = minimal_effort(t, "Future Bass") 
    t["genres"]["local"] = minimal_effort(t, "Vapor Soul") 
    t["genres"]["local"] = minimal_effort(t, "Deep IDM") 
    t["genres"]["local"] = minimal_effort(t, "Futurepop") 
    t["genres"]["local"] = minimal_effort(t, "Bass Trap") 
    t["genres"]["local"] = minimal_effort(t, "Vapor Pop")
    t["genres"]["local"] = minimal_effort(t, "Vocaloid")
    t["genres"]["local"] = minimal_effort(t, "Drill and Bass")
    t["genres"]["local"] = minimal_effort(t, "Glitch")
    t["genres"]["local"] = minimal_effort(t, "Otacore")
    t["genres"]["local"] = minimal_effort(t, "Blackgaze")
    t["genres"]["local"] = minimal_effort(t, "Electro Swing")
    t["genres"]["local"] = minimal_effort(t, "Ninja")
    t["genres"]["local"] = minimal_effort(t, "Mellow Gold")
    t["genres"]["local"] = minimal_effort(t, "Ninja")
    t["genres"]["local"] = minimal_effort(t, "Eurodance")
    t["genres"]["local"] = minimal_effort(t, "Pop Punk")
    t["genres"]["local"] = minimal_effort(t, "Abstract Hip Hop")
    t["genres"]["local"] = minimal_effort(t, "Conscious Hip Hop")
    t["genres"]["local"] = minimal_effort(t, "Permanent Wave")
    t["genres"]["local"] = minimal_effort(t, "Beatboxing")
    t["genres"]["local"] = minimal_effort(t, "Album Rock")
    t["genres"]["local"] = minimal_effort(t, "Ethnotronica")
    t["genres"]["local"] = minimal_effort(t, "Minimal Techno")
    t["genres"]["local"] = minimal_effort(t, "Partyschlager")
    t["genres"]["local"] = minimal_effort(t, "Schlager")
    t["genres"]["local"] = minimal_effort(t, "Chillwave")
    t["genres"]["local"] = minimal_effort(t, "Roots Reggae")
    t["genres"]["local"] = minimal_effort(t, "Baroque Pop")
    t["genres"]["local"] = minimal_effort(t, "Modern Rock")
    t["genres"]["local"] = minimal_effort(t, "Progressive Trance House")
    t["genres"]["local"] = minimal_effort(t, "Grindcore")
    t["genres"]["local"] = minimal_effort(t, "Emo")
    t["genres"]["local"] = minimal_effort(t, "Emo Punk")
    t["genres"]["local"] = minimal_effort(t, "Midwest Emo")
    t["genres"]["local"] = minimal_effort(t, "Power Metal")
    t["genres"]["local"] = minimal_effort(t, "Scottish Metal")
    t["genres"]["local"] = minimal_effort(t, "Progressive Metal")
    t["genres"]["local"] = minimal_effort(t, "Comic Metal")
    t["genres"]["local"] = minimal_effort(t, "Gothic Symphonic Metal")
    t["genres"]["local"] = minimal_effort(t, "Instrumental Progressive Metal")
    t["genres"]["local"] = minimal_effort(t, "Melodic Death Metal")
    t["genres"]["local"] = minimal_effort(t, "Melodic Metal")
    t["genres"]["local"] = minimal_effort(t, "Folk Metal")
    t["genres"]["local"] = minimal_effort(t, "Viking Metal")
    t["genres"]["local"] = minimal_effort(t, "Depressive Black Metal")
    t["genres"]["local"] = minimal_effort(t, "Nordic Folk Metal")
    t["genres"]["local"] = minimal_effort(t, "Speed Metal")
    t["genres"]["local"] = minimal_effort(t, "J-Metal")
    t["genres"]["local"] = minimal_effort(t, "Neo Classical Metal")
    t["genres"]["local"] = minimal_effort(t, "Slayer")
    t["genres"]["local"] = minimal_effort(t, "Power Thrash")
    # t["genres"]["local"] = minimal_effort(t, "Metalcore")
    t["genres"]["local"] = minimal_effort(t, "Neo Mellow")
    t["genres"]["local"] = minimal_effort(t, "Complextro")
    t["genres"]["local"] = minimal_effort(t, "Screamo",["Post-Screamo"])
    t["genres"]["local"] = minimal_effort(t, "Shoegaze",[
        "Russian Shoegaze",
        "French Shoegaze",
        "Japanese Shoegaze",
        "American Shoegaze",
        "Nordic Shoegaze",
        "Shoegaze Brasileiro",
        "Shoegaze Chileno",
        "Canadian Shoegaze",
        "Latin Shoegaze",
        "German Shoegaze",
        "Australian Shoegaze",
        "Indonesian Shoegaze",
        ])
    return t["genres"]["local"]

def genre_converter(
    track_info
):
    t = track_info
    if ('Feature1' in t["genres"]["local"]):
        t["genres"]["local"].remove('Feature1')
    if ('Feature2' in t["genres"]["local"]):
        t["genres"]["local"].remove('Feature2')
    if ('Feature3' in t["genres"]["local"]):
        t["genres"]["local"].remove('Feature3')
    if ('NoMood' in t["genres"]["local"]):
        t["genres"]["local"].remove('NoMood')
    if (('Assi' in t["genres"]["local"]) and ('Hip Hop Café' in t["genres"]["local"])):
        t["genres"]["local"].remove('Hip Hop Café')
    if ('Rock' in t["genres"]["local"]):
        t["genres"]["local"].remove('Rock')
    if ('Level1' in t["genres"]["local"]):
        t["genres"]["local"].remove('Level1')
    if ('Level2' in t["genres"]["local"]):
        t["genres"]["local"].remove('Level2')
    if ('Level3' in t["genres"]["local"]):
        t["genres"]["local"].remove('Level3')
    if ('Level4' in t["genres"]["local"]):
        t["genres"]["local"].remove('Level4')
    if ('Level5' in t["genres"]["local"]):
        t["genres"]["local"].remove('Level5')
    if ('Level6' in t["genres"]["local"]):
        t["genres"]["local"].remove('Level6')
    if ('Level7' in t["genres"]["local"]):
        t["genres"]["local"].remove('Level7')
    if ('Level8' in t["genres"]["local"]):
        t["genres"]["local"].remove('Level8')
    if ('Level9' in t["genres"]["local"]):
        t["genres"]["local"].remove('Level9')
    t["genres"]["local"] = hip_rap(t)
    t["genres"]["local"] = sad(t)
    t["genres"]["local"] = happy(t)
    t["genres"]["local"] = fun(t)
    t["genres"]["local"] = depri_disko(t)
    t["genres"]["local"] = bravo_hits(t)
    t["genres"]["local"] = decolonized(t)
    t["genres"]["local"] = daft_punk_samples(t)
    t["genres"]["local"] = deutschrap_klassiker(t)
    t["genres"]["local"] = pop_klassiker(t)
    t["genres"]["local"] = indie_klassiker(t)
    t["genres"]["local"] = reggae_klassiker(t)
    t["genres"]["local"] = floorfillers(t)
    t["genres"]["local"] = cyberpunk_2077(t)
    t["genres"]["local"] = ultimate_indie(t)
    t["genres"]["local"] = chill_and_dance(t)
    t["genres"]["local"] = kaffeehausmusik(t)
    t["genres"]["local"] = electrosphere(t)
    t["genres"]["local"] = cloudrap(t)
    t["genres"]["local"] = metal(t)
    t["genres"]["local"] = death_metal(t)
    t["genres"]["local"] = chillhop(t)
    t["genres"]["local"] = lo_fi_beats(t)
    t["genres"]["local"] = schwarze_nacht(t)
    t["genres"]["local"] = fierce_femmes(t)
    t["genres"]["local"] = neo_soul(t)
    t["genres"]["local"] = aggrotech(t)
    t["genres"]["local"] = microhouse(t)
    t["genres"]["local"] = neoclassical_darkwave(t)
    t["genres"]["local"] = jazz_rap(t)
    t["genres"]["local"] = jazzy(t)
    t["genres"]["local"] = classify(t)
    t["genres"]["local"] = assi(t)
    t["genres"]["local"] = balkan(t)
    t["genres"]["local"] = epic(t)
    t["genres"]["local"] = eight_bit(t)
    t["genres"]["local"] = elektropunk(t)
    t["genres"]["local"] = indie_rock(t)
    t["genres"]["local"] = indie_pop(t)
    t["genres"]["local"] = punk(t)
    t["genres"]["local"] = german_indie(t)
    t["genres"]["local"] = unterwasser(t)
    t["genres"]["local"] = house(t)
    t["genres"]["local"] = art_pop(t)
    t["genres"]["local"] = antideutsche(t)
    t["genres"]["local"] = edm(t)
    t["genres"]["local"] = pop(t)
    t["genres"]["local"] = funk(t)
    t["genres"]["local"] = arabic(t)
    t["genres"]["local"] = japanese(t)
    t["genres"]["local"] = reggae(t)
    t["genres"]["local"] = reggae_rock(t)
    t["genres"]["local"] = folk(t)
    t["genres"]["local"] = brass(t)
    t["genres"]["local"] = deutsch(t)
    t["genres"]["local"] = level0(t)
    t["genres"]["local"] = forwardish(t)
    t["genres"]["local"] = tanzmetall(t)
    t["genres"]["local"] = retrorun(t)
    t["genres"]["local"] = tunnel(t)
    t["genres"]["local"] = no_mood(t)
    t["genres"]["local"] = dancey(t)
    t["genres"]["local"] = calm(t)
    t["genres"]["local"] = indie_dance(t)
    t["genres"]["local"] = adult_lounge(t)
    t["genres"]["local"] = filter_lounge(t)
    t["genres"]["local"] = bluegrass(t)
    t["genres"]["local"] = organica(t)
    t["genres"]["local"] = electro_industrial(t)
    t["genres"]["local"] = rocken_am_brocken(t)
    t["genres"]["local"] = brocken_party(t)
    t["genres"]["local"] = russian_pop(t)
    t["genres"]["local"] = russian(t)
    t["genres"]["local"] = spanish(t)
    t["genres"]["local"] = italian_pop(t)
    t["genres"]["local"] = italian(t)
    t["genres"]["local"] = french(t)
    t["genres"]["local"] = sleepy(t)
    t["genres"]["local"] = reggaeton(t)
    t["genres"]["local"] = classic_rock(t)
    t["genres"]["local"] = tropical_house(t)
    t["genres"]["local"] = deep_tropical_house(t)
    t["genres"]["local"] = deep_groove_house(t)
    t["genres"]["local"] = feel_good_indie_rock(t)
    t["genres"]["local"] = psytrance(t)
    t["genres"]["local"] = comfort_zone(t)
    t["genres"]["local"] = russian_hip_hop(t)
    t["genres"]["local"] = russian_electronic(t)
    t["genres"]["local"] = afro(t)
    t["genres"]["local"] = r_n_b(t)
    t["genres"]["local"] = electropop(t)
    t["genres"]["local"] = idm(t)
    t["genres"]["local"] = post_rock(t)
    t["genres"]["local"] = dubstep(t)
    t["genres"]["local"] = house(t)
    t["genres"]["local"] = piano(t)
    t["genres"]["local"] = cover(t)
    t["genres"]["local"] = coverchill(t)
    t["genres"]["local"] = acoustic_cover(t)
    t["genres"]["local"] = throwback(t)
    t["genres"]["local"] = throwback_80s(t)
    t["genres"]["local"] = throwback_90s(t)
    t["genres"]["local"] = throwback_00s(t)
    t["genres"]["local"] = metal_cover(t)
    t["genres"]["local"] = acoustic_pop(t)
    t["genres"]["local"] = acoustic(t)
    t["genres"]["local"] = acoustic_chill(t)
    t["genres"]["local"] = mariachi_death_surf(t)
    t["genres"]["local"] = industrial_metal(t)
    t["genres"]["local"] = electronic(t)
    t["genres"]["local"] = ska(t)
    t["genres"]["local"] = assi_flow(t)
    t["genres"]["local"] = math_rock(t)
    t["genres"]["local"] = indie_chill(t)
    t["genres"]["local"] = indie_cafe(t)
    t["genres"]["local"] = powerful_ambient_electronic(t)
    t["genres"]["local"] = ambience_of_the_voiceless(t)
    t["genres"]["local"] = instrumental_ambience_of_the_voiceless(t)
    t["genres"]["local"] = electronic_ambience_of_the_voiceless(t)
    t["genres"]["local"] = deutscher_partpypunk(t)
    t["genres"]["local"] = deutschpunk(t)
    t["genres"]["local"] = hip_hop_cafe(t)
    t["genres"]["local"] = soul(t)
    t["genres"]["local"] = soul_soothers(t)
    t["genres"]["local"] = trip_hop(t)
    t["genres"]["local"] = north(t)
    t["genres"]["local"] = medieval_focus(t)
    t["genres"]["local"] = minimal_focus(t)
    # t["genres"]["local"] = instrumental_focus(t)
    t["genres"]["local"] = focus_beats(t)
    t["genres"]["local"] = black_metal_focus(t)
    t["genres"]["local"] = amped(t)
    t["genres"]["local"] = quiet(t)
    t["genres"]["local"] = loud(t)
    if t["album_release_year"]:
        t["genres"]["local"] = release_year(t)
        t["genres"]["local"] = release_decade(t)
    t["genres"]["local"] = radio0(t)
    t["genres"]["local"] = radio1(t) #Adult
    t["genres"]["local"] = radio2(t) #Indie
    t["genres"]["local"] = radio3(t) #Electro
    t["genres"]["local"] = radio4(t) #Schwarze Nacht
    t["genres"]["local"] = radio5(t) #Folk
    t["genres"]["local"] = radio6(t) #BumHop Escape
    t["genres"]["local"] = radio7(t) #Bass Trip
    t["genres"]["local"] = radio8(t) #Metal
    t["genres"]["local"] = radio_nrj(t)
    t["genres"]["local"] = radio_chill(t)
    t["genres"]["local"] = radio_background(t)
    return t["genres"]["local"]


noOfDir = 0
noOfFiles= 0
for base, dirs, files in os.walk(MUSIC_PATH):
    for directories in dirs:
        noOfDir += 1
    for Files in files:
        if Files.endswith('mp3'):
            noOfFiles += 1
counter = 0


def main(path):
    path_in_str = str(path)
    try:
        meta = EasyID3(path)
    except mutagen.id3.ID3NoHeaderError:
        meta = mutagen.File(path, easy=True)
        meta.add_tags()
    meta
    if mutagen.File(path).tags.getall("COMM"):
        try:
            search_str="Searching for Artist: %s Title: %s " % (meta['artist'][0], meta['title'][0])
        except Exception as e:
            logger.error("THIS CAUSED AN ERROR 1" + path_in_str )
            logger.error(e)
        try:
            genre = meta["Genre"][0]
            genres_local = str(genre).split(";")
            genre_list_original =  str(genre).split(";")
        except:
            genres_local = []
            genre_list_original = []
        comment = mutagen.File(path).tags.getall("COMM")[0].text
        try:
            comment_list = str(comment[0]).split(";")
        except:
            logger.error(f"{bcolors.FAIL}Failed for file: %s {bcolors.ENDC}" % (path_in_str))
            logger.error(f"{bcolors.FAIL}Comment not found{bcolors.ENDC}")
        try:
            spotify_genres_from_comment = json.loads(comment[0])['spotify']['genres']
        except:
            spotify_genres_from_comment = []
        try:
            spotify_track_features_from_comment = json.loads(comment[0])['spotify']['features']
        except:
            spotify_track_features_from_comment = {}
        try:
            spotify_album_release_date_from_comment = json.loads(comment[0])['spotify']['release_date']
            try:
                date_time_obj = datetime.datetime.strptime(spotify_album_release_date_from_comment, '%Y-%m-%d')
            except:
                try:
                    date_time_obj = datetime.datetime.strptime(spotify_album_release_date_from_comment, '%Y-%m')
                except:
                    date_time_obj = datetime.datetime.strptime(spotify_album_release_date_from_comment, '%Y')
            album_release_year=str(date_time_obj.year)
        except:
            album_release_year = ""
        try:
            musicbrainz_tags_from_comment = json.loads(comment[0])['musicbrainz']['tags']
        except:
            musicbrainz_tags_from_comment = []
        try:
            lastfm_tags_from_comment = json.loads(comment[0])['lastfm']['tags']
        except:
            lastfm_tags_from_comment = []
        try:
            discogs_tags_from_comment = json.loads(comment[0])['discogs']['tags']
        except:
            discogs_tags_from_comment = []
        try:
            musicnn_genre_tags_from_comment = json.loads(comment[0])['musicnn']['genres']
        except:
            musicnn_genre_tags_from_comment = []
        try:
            musicnn_mood_from_comment = json.loads(comment[0])['musicnn']['mood']
        except:
            musicnn_mood_from_comment = []
        try:
            musicnn_mood_list = ["musicnn-" + s for s in musicnn_mood_from_comment]
        except:
            musicnn_mood_list = []

        try:
            musicnn_gender_from_comment = json.loads(comment[0])['musicnn']['gender']
        except:
            musicnn_gender_from_comment = []
        try:
            musicnn_instruments_from_comment = json.loads(comment[0])['musicnn']['instruments']
        except:
            musicnn_instruments_from_comment = []
        try:
            spotify_playlists_with_artist_from_comment = json.loads(comment[0])['spotify_playlists']['playlists_with_artist']
        except:
            spotify_playlists_with_artist_from_comment = []
        try:
            spotify_playlists_with_track_from_comment = json.loads(comment[0])['spotify_playlists']['playlists_with_track']
        except:
            spotify_playlists_with_track_from_comment = []
        try:
            cyanite_analysis_from_comment = json.loads(comment[0])['cyanite']['audioAnalysisV6']['result']
        except:
            cyanite_analysis_from_comment = {}
        try:
            cyanite_similar_tracks_from_comment = json.loads(comment[0])['cyanite']['similarTracks']
        except:
            cyanite_similar_tracks_from_comment = {}
        try:
            cyanite_mood_list = ["cyanite-" + s for s in cyanite_analysis_from_comment['moodTags']]
        except:
            cyanite_mood_list = []
        try:
            cyanite_mood_dict = cyanite_analysis_from_comment['mood']
        except:
            cyanite_mood_dict = {}
        try:
            cyanite_top_genre_dict = cyanite_analysis_from_comment['genre']
        except:
            cyanite_top_genre_dict = {}
        try:
            cyanite_sub_genre_dict = cyanite_analysis_from_comment['subgenre']
        except:
            cyanite_sub_genre_dict = {}
        try:
            cyanite_genre_dict = {**cyanite_top_genre_dict, **cyanite_sub_genre_dict}
        except:
            cyanite_genre_dict = {}
        try:
            cyanite_instrument_tags = cyanite_analysis_from_comment['instrumentTags']
        except:
            cyanite_instrument_tags = []
        try:
            cyanite_genre_tags = cyanite_analysis_from_comment['genreTags']
        except:
            cyanite_genre_tags = []
        try:
            cyanite_subgenre_tags = cyanite_analysis_from_comment['subgenreTags']
        except:
            cyanite_subgenre_tags = []
        try:
            cyanite_instrument_presence = cyanite_analysis_from_comment['instrumentPresence']
        except:
            cyanite_instrument_presence = {}
        try:
            cyanite_gender_from_comment = cyanite_analysis_from_comment["predominantVoiceGender"]
        except:
            cyanite_gender_from_comment = []
        try:
            cyanite_additional_info = {}
            cyanite_additional_info["energyLevel"] = cyanite_analysis_from_comment["energyLevel"]
            cyanite_additional_info["energyDynamics"] = cyanite_analysis_from_comment["energyDynamics"]
            cyanite_additional_info["emotionalDynamics"] = cyanite_analysis_from_comment["emotionalDynamics"]
            cyanite_additional_info["emotionalProfile"] = cyanite_analysis_from_comment["emotionalProfile"]
            cyanite_additional_info["voicePresenceProfile"] = cyanite_analysis_from_comment["voicePresenceProfile"]
            cyanite_additional_info["voice"] = cyanite_analysis_from_comment["voice"]
            cyanite_additional_info["predominantVoiceGender"] = cyanite_analysis_from_comment["predominantVoiceGender"]
            cyanite_additional_info["musicalEraTag"] = cyanite_analysis_from_comment["musicalEraTag"]
        except:
            cyanite_additional_info = {}

        genres_from_cyanite = cyanite_genre_tags + cyanite_subgenre_tags
        genres_from_cyanite = list(set(list(filter(lambda genre: genre.strip(), genres_from_cyanite))))


        genres_remote = spotify_genres_from_comment + musicbrainz_tags_from_comment + lastfm_tags_from_comment + discogs_tags_from_comment
        genres_remote = list(set(list(filter(lambda genre: genre.strip(), genres_remote))))
        
        mood_from_comment =  musicnn_mood_list + cyanite_mood_list
        mood_from_comment = list(set(list(filter(lambda mood: mood.strip(), mood_from_comment))))            
        
        intruments_from_comment =  musicnn_instruments_from_comment + cyanite_instrument_tags
        intruments_from_comment = list(set(list(filter(lambda mood: mood.strip(), intruments_from_comment))))

        track_info =  {
            'artist': meta['Artist'][0],
            'intruments': intruments_from_comment,
            'genres': {
                'local': genres_local,
                'remote': genres_remote,
                'cyanite': {
                    'list': genres_from_cyanite,
                    'dict':cyanite_genre_dict
                },
                'spotify': spotify_genres_from_comment,
                'musicnn': musicnn_genre_tags_from_comment,
                'discogs': discogs_tags_from_comment,
                'lastfm': lastfm_tags_from_comment,
                'musicbrainz': musicbrainz_tags_from_comment,
            },
            'playlists': {
                'with_track': spotify_playlists_with_track_from_comment,
                'with_artist': spotify_playlists_with_artist_from_comment
            },
            'mood': {
                'all': mood_from_comment,
                'musicnn': musicnn_mood_from_comment,
                'cyanite': {
                    'list': cyanite_mood_list,
                    'dict':cyanite_mood_dict
                }
            },
            'gender': {
                'musicnn': musicnn_gender_from_comment,
                'cyanite': cyanite_gender_from_comment
            },
            'instruments': {
                'all': intruments_from_comment,
                'musicnn': musicnn_instruments_from_comment,
                'cyanite': {
                    'tags': cyanite_instrument_tags,
                    'presence': cyanite_instrument_presence
                }
            },
            'album_release_year': album_release_year,
            'cyanite_additional_info': cyanite_additional_info,
            'spotify_additional_info': {
                'features': spotify_track_features_from_comment
            }
        }
        track_info_before = track_info
        track_info["genres"]["local"] = genre_converter_minimal_effort(track_info)
        track_info["genres"]["local"] = genre_converter(track_info)
        track_info["genres"]["local"] = genre_converter(track_info)
        logger.debug(genre_list_original)
        track_info["genres"]["local"] = list(set(list(filter(lambda genre: genre.strip(), track_info["genres"]["local"]))))
        genre_list_original = list(set(list(filter(lambda genre: genre.strip(), genre_list_original))))
        if track_info["genres"]["local"] and not (collections.Counter(genre_list_original) == collections.Counter(track_info["genres"]["local"])):
            try:
                logger.info("Updating "+ meta['Artist'][0]+ " " + meta['Title'][0])
                logger.debug(json.dumps(track_info_before, indent=1))
                logger.debug("")
                logger.debug("")
                logger.debug("")
                logger.debug(json.dumps(track_info, indent=1))
                logger.debug(genre_list_original)
                logger.info(track_info["genres"]["local"])
                diff = set(genre_list_original).symmetric_difference(set(track_info["genres"]["local"]))
                logger.info("Difference was:")
                logger.info(diff)
                logger.info("") 

            except Exception as e:
                logger.error("THIS CAUSED AN ERROR 2" + path_in_str )
                logger.error(e)
            genre_set(path, track_info["genres"]["local"])

with alive_bar(noOfFiles,enrich_print=False) as bar:
    with Pool(6) as p:
        for i, _ in enumerate(p.imap_unordered(main,pathlist), 1):
            bar()