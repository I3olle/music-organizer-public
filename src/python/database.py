import spotipy
import sys
import sqlite3
import json
import os
import logging
import time
from alive_progress import alive_bar




Log_Format = "%(levelname)s - %(message)s"

LOG_LEVEL = logging.INFO

logging.basicConfig(stream = sys.stdout, 
                    format = Log_Format, 
                    level = LOG_LEVEL)

logger = logging.getLogger()


HOME = os.environ.get('HOME')
SPOTIPY_CLIENT_ID = os.environ.get('SPOTIPY_CLIENT_ID')
SPOTIPY_CLIENT_SECRET = os.environ.get('SPOTIPY_CLIENT_SECRET')
SPOTIPY_CLIENT_USERNAME = os.environ.get('SPOTIPY_CLIENT_USERNAME')
MUSIC_DB_PATH = "/app/music_db/"
SOURCED_PLAYLISTS_PATH = "/app/sourced_playlists/"
SPOTIPY_REDIRECT_URI=os.environ.get('SPOTIPY_REDIRECT_URI')

scope = 'playlist-modify-private'

auth_manager = spotipy.oauth2.SpotifyOAuth(
    client_id=SPOTIPY_CLIENT_ID,
    client_secret=SPOTIPY_CLIENT_SECRET,
    redirect_uri=SPOTIPY_REDIRECT_URI,
    scope=scope,
    cache_path="/app/.cached-spotipy-credentials")

sp = spotipy.Spotify(auth_manager=auth_manager)


def refresh_token(auth_manager):
    cached_token = auth_manager.get_cached_token()
    refreshed_token = cached_token['refresh_token']
    new_token = auth_manager.refresh_access_token(refreshed_token)
    print(new_token['access_token'])  # <--
    logger.debug(f"New token: {new_token['access_token']}")
    sp = spotipy.Spotify(auth=new_token['access_token'])
    return sp

def create_connection(db_file):
    conn = None
    try:
        conn = sqlite3.connect(db_file)
        logger.debug("Created connection to "+   db_file)
    except Exception as e:
        logger.error("Could not create connection to "+   db_file)
        logger.error(e)
    return conn


def insert_track(conn, track, table):
    sql = f""" INSERT OR IGNORE INTO "{table}"(track_id, title, artist, album)
              VALUES(?,?,?,?) """
    cur = conn.cursor()
    logger.debug("insert_track: Defined cur")
    try:
        cur.execute(sql, track)
        logger.debug("insert_track: Inserted track "+   json.dumps(track, indent=4, sort_keys=True, default=str) + " into table " + table)
    except Exception as e:
        logger.error("insert_track: Could not insert track "+   json.dumps(track, indent=4, sort_keys=True, default=str) + " into table " + table)
        logger.error(e)
    try:
        conn.commit()
        logger.debug("insert_track: Committed insertion of track "+   json.dumps(track, indent=4, sort_keys=True, default=str) + " into table " + table)
    except Exception as e:
        logger.error("insert_track: Could not commit insertion of track "+   json.dumps(track, indent=4, sort_keys=True, default=str) + " into table " + table)
        logger.error(e)
    return cur.lastrowid


def spotify_search_tid(spotify_track_info):
    tid = spotify_track_info['uri']
    return tid

def spotify_search_track_artist_id(spotify_track_info):
    for key, artists in enumerate(spotify_track_info['artists']):
        aid = artists['id']
    return aid


def spotify_search_artist_info(sp, spotify_artist_id):
    try:
        ares = sp.artist(spotify_artist_id)
    except:
        print("WAITING DUE TO TIMEOUT")
        time.sleep(200)
        ares = sp.artist(spotify_artist_id)
    return ares

def get_spotify_track_info(sp, spotify_track_id,market_name='DE'):
    spotify_track_info = sp.track(spotify_track_id)
    logger.debug("get_spotify_track_info: "+   json.dumps(spotify_track_info, indent=4, sort_keys=True, default=str))
    return spotify_track_info
    

def get_playlist_name(sp, playlist_id):
    playlist_info = sp.playlist(playlist_id)
    playlist_name = playlist_info['owner']['display_name']+ " - " +playlist_info['name']
    return playlist_name

def create_table(conn, table):
    sql = f""" CREATE TABLE IF NOT EXISTS "{table}" (
                "track_id"	TEXT NOT NULL UNIQUE,
                "title"	TEXT NOT NULL,
                "artist"	TEXT NOT NULL,
                "album"	TEXT NOT NULL,
                PRIMARY KEY("track_id")
                )"""
    cur = conn.cursor()
    cur.execute(sql)
    conn.commit()
    return cur.lastrowid

def create_modifications_table(conn):
    sql = f"""CREATE TABLE IF NOT EXISTS "modifications" (
                "table_name"	TEXT NOT NULL UNIQUE,
                "changed_at"	INTEGER NOT NULL,
                PRIMARY KEY("table_name")
                )"""
    cur = conn.cursor()
    cur.execute(sql)
    conn.commit()
    logger.debug("created modifications table")
    return cur.lastrowid

def update_modifications(conn,table_name):
    sql = f""" INSERT OR REPLACE INTO "modifications"(table_name, changed_at)
              VALUES(?,?) """
    cur = conn.cursor()
    cur.execute(sql, (table_name, time.time()))
    conn.commit()
    return cur.lastrowid

def get_last_modification(conn,table_name):
    cur = conn.cursor()
    cur.execute("SELECT changed_at FROM modifications WHERE table_name=?", (table_name,))
    return cur.fetchone()[0]

def insert_tracks(sp, playlist_id,offset,conn,table_name):
    try:
        playlist_tracks = sp.playlist_tracks(playlist_id,offset=offset,fields='items.track.id',additional_types=['track'])['items']
    except Exception as e:
        logger.error(f"Error with getting tracks from playlist_id {playlist_id}")
        logger.error(e)
        logger.error(e.__class__.__name__)
        playlist_tracks = {}
        sp = refresh_token(auth_manager)
    if playlist_tracks:
        for track in json.loads(json.dumps(playlist_tracks)):
            try:
                track_id = json.loads(json.dumps(track))['track']['id']
                logger.debug("insert_tracks: track_id = " + track_id)
                track_info = get_spotify_track_info(sp, track_id)
                logger.debug("insert_tracks: track_info = " + json.dumps(track_info, indent=4, sort_keys=True, default=str))
                track_title = track_info['name']
                logger.debug("insert_tracks: track_title = " + track_title)
                album_name = track_info['album']['name']
                logger.debug("insert_tracks: album_name = " + album_name)
                try:
                    artist_id = spotify_search_track_artist_id(track_info)
                    logger.debug("insert_tracks: artist_id = " + artist_id)
                except:
                    logger.error("Could not get Spotify artist ID")
                    pass
                try:
                    artist_info  = spotify_search_artist_info(sp, artist_id)
                    logger.debug("insert_tracks: artist_info = " + json.dumps(artist_info, indent=4, sort_keys=True, default=str))
                except:
                    logger.error("Could not get Spotify artist info ")
                    pass
                try:
                    artist_name = artist_info['name']
                    logger.debug("insert_tracks: artist_name = " + artist_name)
                except:
                    logger.error("Could not get artist name")
                try:
                    track_info = ("spotify:track:"+track_id, track_title, artist_name, album_name)
                    logger.debug("insert_tracks: track_info = " + json.dumps(track_info, indent=4, sort_keys=True, default=str))
                except Exception as e:
                    logger.error("insert_tracks: Could not get track info for "+   json.dumps(track, indent=4, sort_keys=True, default=str))
                    logger.error(e)
                try:
                    insert_track(conn, track_info, table_name)
                except Exception as e:
                    logger.error("insert_tracks: Could not insert track "+   json.dumps(track, indent=4, sort_keys=True, default=str) + " into table " + table_name)
                    logger.error(e)
            except Exception as e:
                logger.error("One error on", playlist_id)
                logger.error(track)
                logger.error(e)
                time.sleep(5)

def update_database(conn):
    logger.info("Updating database")
    with conn:
        create_modifications_table(conn)

        number_of_playlists = 0

        with open(SOURCED_PLAYLISTS_PATH+'/current_playlists.json') as json_file:
            playlists = json.load(json_file)
            logger.debug("Playlists have been loaded")

        exclude_for_minimal_update = [
            'Sonemic',
            'ThePlaylistFactory',
            'BravoHits',
            'TheSoundOfEverything',
            'The Playlist Factory',
            'liquicityrecords',
            'ukfmusic',
            'alxrnbrdmusic',
        ]

        for category in playlists:
            number_of_playlists = number_of_playlists + len(playlists[category])
        with alive_bar(number_of_playlists) as bar:
            for category in playlists:
                if (category not in exclude_for_minimal_update):
                    for playlist_id, playlist_name in playlists[category].items():
                        try:
                            playlist_name = get_playlist_name(sp, playlist_id) #make double sure the name is saved in the correct format for the db
                        except Exception as e:
                            logger.error(f"main: playlist {playlist_id} has no name")
                        if playlist_name:
                            create_table(conn, playlist_name)
                            one_week_in_seconds = 10080
                            one_week_in_milliseconds = one_week_in_seconds * 60
                            try:
                                last_modification = get_last_modification(conn,playlist_name)
                            except:
                                last_modification = time.time() - (3 * one_week_in_milliseconds)
                            current_timestamp = time.time()
                            time_diff_min = (current_timestamp - last_modification)/60
                            if float(time_diff_min) > float(2 * one_week_in_seconds):
                                offset_intervals = [0,100,200,300,400,500,600,700,800,900]
                                for offset in offset_intervals:
                                    try:
                                        insert_tracks(sp, playlist_id,offset,conn,playlist_name)
                                    except spotipy.client.SpotifyException as e:
                                        logger.error(f"main: insert_tracks {playlist_name} did not work")
                                        logger.error(e)
                                        pass
                                update_modifications(conn,playlist_name)
                        bar()
                else:
                    for playlist_id, playlist_name in playlists[category].items():
                        bar()


def print_db_info(playlists):
    playlist_infos = {}
    for category, playlist_data in playlists.items():

        playlist_infos[category] = {}
        for playlist_id in playlist_data["playlist_ids"]:
            playlist_data = sp.playlist(playlist_id)
            playlist_name = playlist_data['name']
            playlist_infos[category][playlist_id] = playlist_name
    print(playlist_infos)


def get_featured_playlists(sp, blacklist_playlist_ids):
    featured_playlist_infos = {}
    countrycodes = [
        'AD',
        'AE',
        'AG',
        'AL',
        'AM',
        'AO',
        'AR',
        'AT',
        'AU',
        'AZ',
        'BA',
        'BB',
        'BD',
        'BE',
        'BF',
        'BG',
        'BH',
        'BI',
        'BJ',
        'BN',
        'BO',
        'BR',
        'BS',
        'BT',
        'BW',
        'BY',
        'BZ',
        'CA',
        'CH',
        'CI',
        'CL',
        'CM',
        'CO',
        'CR',
        'CV',
        'CW',
        'CY',
        'CZ',
        'DE',
        'DJ',
        'DK',
        'DM',
        'DO',
        'DZ',
        'EC',
        'EE',
        'EG',
        'ES',
        'FI',
        'FJ',
        'FM',
        'FR',
        'GA',
        'GD',
        'GE',
        'GH',
        'GM',
        'GN',
        'GQ',
        'GR',
        'GT',
        'GW',
        'GY',
        'HK',
        'HN',
        'HR',
        'HT',
        'HU',
        'ID',
        'IE',
        'IL',
        'IN',
        'IS',
        'IT',
        'JM',
        'JO',
        'JP',
        'KE',
        'KG',
        'KH',
        'KI',
        'KM',
        'KN',
        'KR',
        'KW',
        'KZ',
        'LA',
        'LB',
        'LC',
        'LI',
        'LK',
        'LR',
        'LS',
        'LT',
        'LU',
        'LV',
        'MA',
        'MC',
        'MD',
        'ME',
        'MG',
        'MH',
        'MK',
        'ML',
        'MN',
        'MO',
        'MR',
        'MT',
        'MU',
        'MV',
        'MW',
        'MX',
        'MY',
        'MZ',
        'NA',
        'NE',
        'NG',
        'NI',
        'NL',
        'NO',
        'NP',
        'NR',
        'NZ',
        'OM',
        'PA',
        'PE',
        'PG',
        'PH',
        'PK',
        'PL',
        'PS',
        'PT',
        'PW',
        'PY',
        'QA',
        'RO',
        'RS',
        'RU',
        'RW',
        'SA',
        'SB',
        'SC',
        'SE',
        'SG',
        'SI',
        'SK',
        'SL',
        'SM',
        'SN',
        'SR',
        'ST',
        'SV',
        'SZ',
        'TD',
        'TG',
        'TH',
        'TL',
        'TN',
        'TO',
        'TR',
        'TT',
        'TV',
        'TW',
        'TZ',
        'UA',
        'UG',
        'US',
        'UY',
        'UZ',
        'VC',
        'VN',
        'VU',
        'WS',
        'ZA',
        'ZM',
        'ZW'
    ]
    for countrycode in countrycodes:
        try:
            featured_playlists = sp.featured_playlists(limit=50,country=countrycode)
            #print(featured_playlists)
            while featured_playlists:
                for i, playlist in enumerate(featured_playlists['playlists']['items']):
                    if playlist['id'] not in blacklist_playlist_ids:
                        playlist_id = playlist['id']
                        playlist_name = get_playlist_name(sp, playlist_id) #make double sure the name is saved in the correct format for the db
                        featured_playlist_infos[playlist_id] = playlist_name
                        #print("%4d %s %s" % (i + 1 + playlists['offset'], playlist['uri'],  playlist['name']))
                if featured_playlists['playlists']['next']:
                    featured_playlists = sp.next(featured_playlists)
                else:
                    featured_playlists = None
        except:
            logger.error(f"Could not get featured playlists for contrycode {countrycode}")
            pass
    return featured_playlist_infos

def get_db_playlist_ids(sp):
    logger.info("Get db-playlists")
    db_playlist_ids = []
    with open('/app/sourced_playlists/current_playlists.json') as json_file:
        data = json.load(json_file)
        db_playlists_infos = {}
        for category,playlists in data.items():
            db_playlists_infos[category] = {}
            for playlist in data[category].items():
                playlist_id = playlist[0]
                db_playlist_ids.append(playlist_id)
    return db_playlist_ids

def get_blacklist_playlist_ids():
    logger.info("Get blacklist")
    db_playlist_ids = []
    with open('/app/sourced_playlists/playlist_blacklist.json') as json_file:
        data = json.load(json_file)
        for user in data:
            for playlist in data[user].items():
                db_playlist_ids.append(playlist[0])
    return db_playlist_ids

def save_json(json_content,json_file_name):
    json_file = open('/app/sourced_playlists/'+json_file_name, "w", encoding='utf8')
    json.dump(json_content, json_file, ensure_ascii=False)


def get_user_playlist_infos(sp, username, blacklist_playlist_ids):
    user_playlist_infos = {}
    playlists = sp.user_playlists(username,limit=50)
    while playlists:
        for i, playlist in enumerate(playlists['items']):
            if playlist['id'] not in blacklist_playlist_ids:
                playlist_id = playlist['id']
                playlist_name = get_playlist_name(sp, playlist_id) #make double sure the name is saved in the correct format for the db
                user_playlist_infos[playlist_id] = playlist_name
                #print("%4d %s %s" % (i + 1 + playlists['offset'], playlist['uri'],  playlist['name']))
        if playlists['next']:
            playlists = sp.next(playlists)
        else:
            playlists = None
    return user_playlist_infos

def update_available_playlists(sp,blacklist_playlist_ids):
    logger.info("Updating available playlists")
    available_playlists = {}
    logger.info(f"Saving json for my own playlists")
    available_playlists['mine'] = get_user_playlist_infos(sp, SPOTIPY_CLIENT_USERNAME, blacklist_playlist_ids)
    save_json(available_playlists,"available_playlists.json")

    logger.info(f"Saving json for spotify")
    available_playlists['spotify'] = get_user_playlist_infos(sp, 'spotify', blacklist_playlist_ids)
    save_json(available_playlists,"available_playlists.json")

    # logger.info(f"Saving json for sonemic")
    # available_playlists['sonemic'] = get_user_playlist_infos(sp, 'sonemic.com', blacklist_playlist_ids)
    # save_json(available_playlists,"available_playlists.json")

    # logger.info(f"Saving json for songpickr")
    # available_playlists['songpickr'] = get_user_playlist_infos(sp, 'holgerchristoph', blacklist_playlist_ids)
    # save_json(available_playlists,"available_playlists.json")

    # logger.info(f"Saving json for The Playlist Factory")
    # available_playlists['The Playlist Factory'] = get_user_playlist_infos(sp, 'yourfavouriteplaylists', blacklist_playlist_ids)
    # save_json(available_playlists,"available_playlists.json")

    # logger.info(f"Saving json for midnightstudios")
    # available_playlists['midnightstudios'] = get_user_playlist_infos(sp, 'midnightstudios', blacklist_playlist_ids)
    # save_json(available_playlists,"available_playlists.json")

    # logger.info(f"Saving json for suicidesheepify")
    # available_playlists['suicidesheepify'] = get_user_playlist_infos(sp, 'suicidesheepify', blacklist_playlist_ids)
    # save_json(available_playlists,"available_playlists.json")

    # logger.info(f"Saving json for selectedbase")
    # available_playlists['selectedbase'] = get_user_playlist_infos(sp, 'selectedbase', blacklist_playlist_ids)
    # save_json(available_playlists,"available_playlists.json")

    # logger.info(f"Saving json for liquicityrecords")
    # available_playlists['liquicityrecords'] = get_user_playlist_infos(sp, 'liquicityrecords', blacklist_playlist_ids)
    # save_json(available_playlists,"available_playlists.json")

    # logger.info(f"Saving json for ukfmusic")
    # available_playlists['ukfmusic'] = get_user_playlist_infos(sp, 'ukfmusic', blacklist_playlist_ids)
    # save_json(available_playlists,"available_playlists.json")

    # available_playlists['alxrnbrdmusic'] = get_user_playlist_infos(sp, 'alxrnbrdmusic', blacklist_playlist_ids)
    # save_json(available_playlists,"available_playlists.json")

    # logger.info(f"Saving json for majesticcasualofficial")
    # available_playlists['majesticcasualofficial'] = get_user_playlist_infos(sp, 'majesticcasualofficial', blacklist_playlist_ids)
    # save_json(available_playlists,"available_playlists.json")

    # logger.info(f"Saving json for chilledcow")
    # available_playlists['chilledcow'] = get_user_playlist_infos(sp, 'chilledcow', blacklist_playlist_ids)
    # save_json(available_playlists,"available_playlists.json")

    # logger.info(f"Saving json for chillhopmusic")
    # available_playlists['chillhopmusic'] = get_user_playlist_infos(sp, 'chillhopmusic', blacklist_playlist_ids)
    # save_json(available_playlists,"available_playlists.json")

    logger.info(f"Saving json for thesoundsofspotify")
    available_playlists['thesoundsofspotify'] = get_user_playlist_infos(sp, 'thesoundsofspotify', blacklist_playlist_ids)
    save_json(available_playlists,"available_playlists.json")

    logger.info(f"Saving json for featured playlists")
    available_playlists['featured'] = get_featured_playlists(sp, blacklist_playlist_ids)
    save_json(available_playlists,"available_playlists.json")

    logger.info(f"Saving json for ")


# tag::main[]
def main():
    blacklist_playlist_ids = get_blacklist_playlist_ids()
    db_playlist_ids = get_db_playlist_ids(sp)
    blacklist_playlist_ids = blacklist_playlist_ids + db_playlist_ids
    update_available_playlists(sp, blacklist_playlist_ids)
    database = MUSIC_DB_PATH+"playlists.sqlite3"
    update_database(create_connection(database))
# end::main[]


if __name__ == '__main__':
    main()